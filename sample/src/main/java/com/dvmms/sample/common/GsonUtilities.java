package com.dvmms.sample.common;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Platon on 01.07.2016.
 */
public class GsonUtilities {
    private static Gson gson;

    public static Gson getGson() {
        if (gson == null) {
            initGson();
        }
        return gson;
    }

    private static void initGson() {
        GsonBuilder gsb = new GsonBuilder();
        gsb.serializeNulls();
        gsb.addSerializationExclusionStrategy(new ExclusionStrategy() {
            @Override
            public boolean shouldSkipField(FieldAttributes f) {
                return  false;//f.getAnnotation(Exclude.class) != null;
            }

            @Override
            public boolean shouldSkipClass(Class<?> aClass) {
                return false;
            }
        })
                .addDeserializationExclusionStrategy(new ExclusionStrategy() {
                    @Override
                    public boolean shouldSkipField(FieldAttributes f) {
                        return false; //f.getAnnotation(Exclude.class) != null;
                    }

                    @Override
                    public boolean shouldSkipClass(Class<?> aClass) {
                        return false;
                    }
                });
        gson = gsb.create();
    }

    public static String serialize(Object obj) {
        String representation = getGson().toJson(obj);
        return representation;
    }

    public static <T> T deserialize(String repr, Class<T> clazz) {
        T obj = getGson().fromJson(repr, clazz);
        return obj;
    }

    public static <T> List<T> deserializeList(String json, Class<T> clazz) {
        List<T> items = new ArrayList<>();

        JsonParser parser = new JsonParser();
        JsonArray array = parser.parse(json).getAsJsonArray();

        for(final JsonElement jsonEl: array){
            T item = getGson().fromJson(jsonEl, clazz);
            items.add(item);
        }

        return items;
    }

    public static <T> T deserialize(String repr, Type clazz) {
        T obj = getGson().fromJson(repr, clazz);
        return obj;
    }

    public static <T> T deserialize(JsonElement repr, Type clazz) {
        T obj = getGson().fromJson(repr, clazz);
        return obj;
    }
}
