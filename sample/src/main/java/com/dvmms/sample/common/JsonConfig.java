package com.dvmms.sample.common;

import android.content.Context;
import android.content.res.AssetManager;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Platon on 01.07.2016.
 */
public class JsonConfig {
    public static <T> List<T> loadConfigList(String filenameWithExtension, Class<T> clazz, Context context) {
        String json = AssetJSONFile(filenameWithExtension, context);
        if (json != null) {
            return GsonUtilities.deserializeList(json, clazz);
        }
        return new ArrayList<>();
    }

    public static <T> T loadConfig(String filenameWithExtension, Class<T> clazz, Context context) {
        String json = AssetJSONFile(filenameWithExtension, context);
        if (json != null) {
            return GsonUtilities.deserialize(json, clazz);
        }
        return null;
    }

    public static String AssetJSONFile (String filename, Context context) {
        String json = null;
        try {
            InputStream is = context.getAssets().open(filename);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }
}
