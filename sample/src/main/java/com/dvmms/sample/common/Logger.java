package com.dvmms.sample.common;

import android.os.Looper;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.dvmms.dejapay.IDejaPayLogger;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Created by Platon on 06.01.2016.
 */
public class Logger implements IDejaPayLogger {
    private static final int MAX_LOG_LENGTH = 4000;
    private static final int CALL_STACK_INDEX = 5;
    private static final Pattern ANONYMOUS_CLASS = Pattern.compile("(\\$\\d+)+$");

    public Logger() {  }

    @Override
    public void v(String message, Object... args) {
        prepareLog(getTag(), Log.VERBOSE, null, message, args);
    }

    @Override
    public void v(Throwable t, String message, Object... args) {
        prepareLog(getTag(), Log.VERBOSE, t, message, args);
    }

    @Override
    public void d(String message, Object... args) {
        prepareLog(getTag(), Log.DEBUG, null, message, args);
    }

    @Override
    public void d(Throwable t, String message, Object... args) {
        prepareLog(getTag(), Log.DEBUG, t, message, args);
    }

    @Override
    public void w(String message, Object... args) {
        prepareLog(getTag(), Log.WARN, null, message, args);
    }

    @Override
    public void w(Throwable t, String message, Object... args) {
        prepareLog(getTag(), Log.WARN, t, message, args);
    }

    @Override
    public void e(String message, Object... args) {
        prepareLog(getTag(), Log.ERROR, null, message, args);
    }

    @Override
    public void e(Throwable t, String message, Object... args) {
        prepareLog(getTag(), Log.ERROR, t, message, args);
    }

    public static void e(String tag, Throwable t, String message, Object... args) {
        prepareLog(tag, Log.ERROR, t, message, args);
    }

    public static void d(String tag, String message, Object... args) {
        prepareLog(tag, Log.DEBUG, null, message, args);
    }


    private static void prepareLog(String tag, int priority, Throwable t, String message, Object... args) {
        if (!isLoggable(priority)) {
            return;
        }
        if (message != null && message.length() == 0) {
            message = null;
        }
        if (message == null) {
            if (t == null) {
                return; // Swallow message if it's null and there's no throwable.
            }
            message = getStackTraceString(t);
        } else {
            if (args.length > 0) {
                message = String.format(message, args);
            }
            if (t != null) {
                message += "\n" + getStackTraceString(t);
            }
        }

        log(priority, tag, message, t);
    }

    private static boolean isLoggable(int priority) {
        return true;
    }

    private static String getStackTraceString(Throwable t) {
        // Don't replace this with Log.getStackTraceString() - it hides
        // UnknownHostException, which is not what we want.
        StringWriter sw = new StringWriter(256);
        PrintWriter pw = new PrintWriter(sw, false);
        t.printStackTrace(pw);
        pw.flush();
        return sw.toString();
    }

    protected static void log(int priority, String tag, String message, Throwable t) {
        if (message.length() < MAX_LOG_LENGTH) {
            if (priority == Log.ASSERT) {
                Log.wtf(tag, message);
            } else {
                Log.println(priority, tag, message);
            }
        } else {
            // Split by line, then ensure each line can fit into Log's maximum length.
            for (int i = 0, length = message.length(); i < length; i++) {
                int newline = message.indexOf('\n', i);
                newline = newline != -1 ? newline : length;
                do {
                    int end = Math.min(newline, i + MAX_LOG_LENGTH);
                    String part = message.substring(i, end);
                    if (priority == Log.ASSERT) {
                        Log.wtf(tag, part);
                    } else {
                        Log.println(priority, tag, part);
                    }
                    i = end;
                } while (i < newline);
            }
        }
    }

    final String getTag() {
        return "DEJAPAY";
    }

    protected String createStackElementTag(StackTraceElement element) {
        String tag = element.getClassName();
        Matcher m = ANONYMOUS_CLASS.matcher(tag);
        if (m.find()) {
            tag = m.replaceAll("");
        }
        return tag.substring(tag.lastIndexOf('.') + 1);
    }
}

