package com.dvmms.sample.common;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by Platon on 04.07.2016.
 */
public class ToastUtils {
    public static void showToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }
}
