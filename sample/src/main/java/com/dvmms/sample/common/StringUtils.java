package com.dvmms.sample.common;

import java.nio.charset.Charset;

/**
 * Created by Platon on 30.06.2016.
 */
public class StringUtils {
    public static String fromBoolean(boolean state) {
        return (state ? "Yes" : "No");
    }
    public static String fromBytes(byte[] bytes) {
        final Charset UTF8_CHARSET = Charset.forName("UTF-8");
        if (bytes != null) {
            return new String(bytes, UTF8_CHARSET);
        }
        return "";
    }


    public static boolean isEmptyOrNull(String text) {
        return (text == null || text.isEmpty());
    }

    public static String escapeNull(String text) {
        if (text == null) {
            return "";
        } else {
            return text;
        }
    }

    public static String toString(Object object) {
        if (object == null) {
            return "";
        }

        try {
            return String.valueOf(object);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return "";
    }

    public static String fromFloat(Float value, int dimension) {
        if (value != null) {
            return value.toString();
        }
        return "0.00";
    }

    public static String fromDouble(Double value, int dimension) {
        if (value != null) {
            return value.toString();
        }
        return "0.00";
    }

    public static Boolean toBoolean(String text, boolean defaultValue) {
        if (text == null) {
            return defaultValue;
        }

        if (text.equalsIgnoreCase("true")) {
            return true;
        }

        return false;
    }

    public static Double toDouble(String text, double defaultValue) {
        try {
            return Double.parseDouble(text);
        } catch (NumberFormatException ex) {
            ex.printStackTrace();
        }
        return defaultValue;
    }
}
