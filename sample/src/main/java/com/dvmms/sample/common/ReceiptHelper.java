package com.dvmms.sample.common;

import android.support.annotation.Nullable;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ReceiptHelper {
    public static String htmlFromDejavooReceipt(String receipt, byte[] sign) {
        try {
            HashMap<String, byte[]> images = new HashMap<>();
            if (sign != null) {
                images.put("signature", sign);
            }

            List<LineItem> lines = getItemsFromReceipt(receipt, images);

            StringBuilder builder = new StringBuilder();
            builder.append("<div class='rc'>");

            for (int i = 0; i < lines.size(); i++) {
                LineItem line = lines.get(i);

                if (line instanceof BreakLineItem) {
                    builder.append("<div class='rc-br'></div>");
                    continue;
                }

                if (line instanceof ImageLineItem) {
                    if (images.containsKey(line.getText())) {
//                    BmpImageElementDocument imageEl = new BmpImageElementDocument(images.get(line.getText()));
//
//                    builder.append(String.format(
//                            "<div class='rc-center'><img src='data:image/bmp;base64,%s' /></div>",
//                            new Base64DataCoder().encode(imageEl.getBytes())
//                    ));
                    }
                    continue;
                }

                if (line instanceof SepartorLineItem) {
                    builder.append("<div class='rc-br'></div>");
                    builder.append("<div class='rc-center'>--------------------</div>");
                    builder.append("<div class='rc-br'></div>");
                    continue;
                }

                // Text Lines
                String lineBuilder = line.getText();

                if (line.isBold()) {
                    lineBuilder = String.format("<div class='rc-bold'>%s</div>", lineBuilder);
                }

                if (line.isLarge()) {
                    lineBuilder = String.format("<div class='rc-large'>%s</div>", lineBuilder);
                }

                if (line.isCondensed()) {
                    lineBuilder = String.format("<div class='rc-condensed'>%s</div>", lineBuilder);
                }

                if (line.isInverted()) {
                    lineBuilder = String.format("<div class='rc-inverted'>%s</div>", lineBuilder);
                }

                switch (line.getAlignment()) {
                    case Center: {
                        builder.append(String.format("<div class='rc-center'>%s</div>", lineBuilder));
                        break;
                    }
                    case Right: {
                        builder.append(String.format("<div class='rc-right'>%s</div>", lineBuilder));
                        break;
                    }
                    case Left: {
                        builder.append(String.format("<div class='rc-left'>%s</div>", lineBuilder));
                        break;
                    }
                }
            }

            builder.append("</div>");

            return wrapStyle(builder.toString());
        } catch (IOException ignored) {
            return "";
        } catch (XmlPullParserException ignored) {
            return "";
        }
    }

    private static String wrapStyle(String receipt) {
        StringBuilder htmlBuilder = new StringBuilder();

        htmlBuilder.append("<HEAD><STYLE type=\"text/css\">");
        htmlBuilder.append(".rc {font-family: 'Courier New', monospace; font-size: 15px; width: 300px;}\n" +
                " .rc-br {height: 1em; clear: both; }\n" +
                " .rc-left + .rc-br, .rc-right + .rc-br, .rc-center + .rc-br {height: 2px;}\n" +
                " .rc-left {max-width: 300px; text-align: left; float: left;}\n" +
                " .rc-right {max-width: 300px; text-align: right; float: right;}\n" +
                " .rc-center {text-align: center;}\n" +
                " .rc-condensed {font-stretch: condensed;}\n" +
                " .rc-large {font-size: 18px;}\n" +
                " .rc-inverted {background-color: #000; color: #fff;}\n" +
                " .rc-bold {font-weight: bold;}\n" +
                " .rc img {max-width: 192px;}\n"+
                " .parent {display: table; margin: 0 auto; text-align: center;}"
        );

        htmlBuilder.append(
                String.format("</STYLE></HEAD><BODY><div class=\"parent\">%s</div></BODY>", receipt)
        );

        return htmlBuilder.toString();
    }


    private enum Alignment { Center, Right, Left;

        public boolean anyOf(Alignment... alignments) {
            for (Alignment alignment : alignments) {
                if (alignment == this) {
                    return true;
                }
            }
            return false;
        }
    }

    private static List<LineItem> getItemsFromReceipt(String receipt, @Nullable Map<String, byte[]> images)  throws IOException, XmlPullParserException{
        List<LineItem> lines = new ArrayList<>();

        XmlPullParserFactory xmlFactoryObject = null;
        receipt = receipt.replaceAll(">\\s*<", "><");


//        receipt = receipt.replace("X____________________", "<C>X____________________</C>");

        String valueText = "";
        xmlFactoryObject = XmlPullParserFactory.newInstance();
        XmlPullParser parser = xmlFactoryObject.newPullParser();
        InputStream is = new ByteArrayInputStream(receipt.getBytes());
        parser.setInput(is, null);

        int event = parser.getEventType();
        HashMap<String, String> attrs = null;

        boolean large = false;
        boolean condensed = false;
        boolean inverted = false;
        boolean bold = false;

        while (event != XmlPullParser.END_DOCUMENT) {
            String name = parser.getName();
            switch (event) {
                case XmlPullParser.START_TAG: {
                    if (name.equalsIgnoreCase("B")) {
                        bold = true;
                    } else if (name.equalsIgnoreCase("LG")) {
                        large = true;
                    } else if (name.equalsIgnoreCase("CD")) {
                        condensed = true;
                    } else if (name.equalsIgnoreCase("INV")) {
                        inverted = true;
                    } else {
                        attrs = new HashMap<>();
                        for (int i = 0; i < parser.getAttributeCount(); i++) {
                            attrs.put(parser.getAttributeName(i), parser.getAttributeValue(i));
                        }
                    }
                }
                break;
                case XmlPullParser.TEXT:
                    valueText = parser.getText();
                    break;
                case XmlPullParser.END_TAG: {
                    if (name.equalsIgnoreCase("B")) {
                        bold = false;
                    } else if (name.equalsIgnoreCase("LG")) {
                        large = false;
                    } else if (name.equalsIgnoreCase("CD")) {
                        condensed = false;
                    } else if (name.equalsIgnoreCase("INV")) {
                        inverted = false;
                    } else if (name.equalsIgnoreCase("BR") || name.equalsIgnoreCase("LF")) {
                        lines.add(new BreakLineItem());
                    } else if (name.equalsIgnoreCase("R")) {
                        lines.add(new LineItem(valueText, Alignment.Right, large, inverted, condensed, bold));
                        valueText = "";
                    } else if (name.equalsIgnoreCase("L")) {
                        if (isEmptyOrNull(valueText)) {
                            lines.add(new BreakLeftLine());
                        } else {
                            lines.add(new LineItem(valueText, Alignment.Left, large, inverted, condensed, bold));
                        }

                        valueText = "";
                    } else if (name.equalsIgnoreCase("C")) {
                        lines.add(new LineItem(valueText, Alignment.Center, large, inverted, condensed, bold));
                        valueText = "";
                    } else if (name.equalsIgnoreCase("SEP")) {
                        lines.add(new SepartorLineItem());
                    } else if (name.equalsIgnoreCase("IMG")) {
                        if (attrs != null && attrs.containsKey("src")) {
                            String src = attrs.get("src");

                            if (src.contains("sig-")) {
                                src = "signature";
                            }

                            if (!isEmptyOrNull(src) && images != null && images.containsKey(src)) {
                                lines.add(new ImageLineItem(src));
                            }
                        }
                    }

                    break;
                }
            }
            event = parser.next();
        }

        return lines;
    }

    private static class LineItem {
        private final String text;
        private final Alignment alignment;
        private final boolean large;
        private final boolean inverted;
        private final boolean condensed;
        private final boolean bold;

        public LineItem(String text, Alignment alignment, boolean large, boolean inverted, boolean condensed, boolean bold) {
            this.text = text;
            this.alignment = alignment;
            this.large = large;
            this.inverted = inverted;
            this.condensed = condensed;
            this.bold = bold;
        }

        public String getText() {
            return text;
        }

        public Alignment getAlignment() {
            return alignment;
        }

        public boolean isLarge() {
            return large;
        }

        public boolean isInverted() {
            return inverted;
        }

        public boolean isCondensed() {
            return condensed;
        }

        public boolean isBold() {
            return bold;
        }
    }


    private static class BreakLineItem extends LineItem {
        public BreakLineItem() {
            super("", Alignment.Center, false, false, false, false);
        }
    }

    private static class ImageLineItem extends LineItem {
        public ImageLineItem(String name) {
            super(name, Alignment.Center, false, false, false, false);
        }
    }

    private static class SepartorLineItem extends LineItem {
        public SepartorLineItem() {
            super("", Alignment.Center, false, false, false, false);
        }
    }

    private static class BreakLeftLine extends LineItem {
        public BreakLeftLine() {
            super("", Alignment.Left, false, false, false, false);
        }
    }

    private static boolean isEmptyOrNull(String text) {
        return (text == null || text.isEmpty());
    }
}
