package com.dvmms.sample.ui.formatter;

import java.util.regex.Pattern;

/**
 * Created by Platon on 01.03.2016.
 */
public class IPValidator implements IValidator<String> {
    private final Pattern PATTERN = Pattern.compile(
            "^(([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.){3}([01]?\\d\\d?|2[0-4]\\d|25[0-5])$");

    @Override
    public boolean validate(String ip) {
        if (ip == null) {
            return false;
        }
        return PATTERN.matcher(ip).matches();
    }
}
