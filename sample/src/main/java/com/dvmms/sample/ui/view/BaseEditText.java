package com.dvmms.sample.ui.view;

import android.content.Context;
import android.support.design.widget.TextInputEditText;
import android.util.AttributeSet;

/**
 * Created by Platon on 14.01.2016.
 */
public class BaseEditText extends TextInputEditText {
    public BaseEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void clearFocusing() {
//        if (hasFocus()) {
//            InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
//            imm.hideSoftInputFromWindow(this.getWindowToken(), 0);
//            clearFocus();
//        }
    }
}
