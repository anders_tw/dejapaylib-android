package com.dvmms.sample.ui.fields;

import android.view.View;

import com.dvmms.sample.ui.fields.BaseFieldViewModel;

/**
 * Created by Platon on 03.02.2016.
 */
public interface IOnClickFieldListener <T extends BaseFieldViewModel> {
    void onClick(T field, View v);
}
