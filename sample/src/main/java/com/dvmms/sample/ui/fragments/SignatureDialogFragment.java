package com.dvmms.sample.ui.fragments;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dvmms.sample.R;
import com.github.gcacace.signaturepad.views.SignaturePad;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Platon on 05.07.2016.
 */
public class SignatureDialogFragment extends AppCompatDialogFragment {
    @BindView(R.id.signPadView)
    SignaturePad signaturePadView;
//    @BindView(R.id.btnClear)
//    BaseButton btnClear;
//    @BindView(R.id.btnSkip)
//    BaseButton btnSkip;
//    @BindView(R.id.btnContinue)
//    BaseButton btnContinue;

    private ISignatureDialogListener listener;

    static public SignatureDialogFragment newInstance(ISignatureDialogListener listener) {
        SignatureDialogFragment fragment = new SignatureDialogFragment();
        fragment.setListener(listener);
        return fragment;
    }



    public SignatureDialogFragment() {  }

    public void setListener(ISignatureDialogListener listener) {
        this.listener = listener;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, 0);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_signature, container, false);
        ButterKnife.bind(this, v);

        setupUI();
        return v;
    }

    void setupUI() {  }

    @OnClick(R.id.btnClear)
    void onClickedClear() {
        signaturePadView.clear();
    }

    @OnClick(R.id.btnSkip)
    void onClickedSkip() {
        listener.onSkipSignature(this);
    }

    @OnClick(R.id.btnContinue)
    void onClickedContinue() {
        if (!signaturePadView.isEmpty()) {
            Bitmap bitmap = signaturePadView.getTransparentSignatureBitmap(true);

            if (listener != null) {
                listener.onSendSignature(this, bitmap);
            }
        }
    }

//    void blockUI(boolean block) {
//        if (btnClear != null) {
//            btnClear.setEnabled(!block);
//        }
//        if (btnSkip != null) {
//            btnSkip.setEnabled(!block);
//        }
//
//        if (btnContinue != null) {
//            btnContinue.setEnabled(!block);
//        }
//
//        if (signaturePadView != null) {
//            signaturePadView.setEnabled(!block);
//        }
//    }

    public interface ISignatureDialogListener {
        void onSkipSignature(SignatureDialogFragment dialog);
        void onSendSignature(SignatureDialogFragment dialog, Bitmap sign);
    }
}
