package com.dvmms.sample.ui.fragments;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dvmms.sample.R;
import com.dvmms.sample.ui.fields.BaseFieldViewModel;
import com.dvmms.sample.ui.fields.FieldListAdapter;
import com.dvmms.sample.ui.fields.FieldViewModelFactory;
import com.dvmms.sample.ui.fields.FieldsManager;

import java.util.List;

/**
 * Created by Platon on 01.07.2016.
 */
public abstract class AbstractFieldsFragment extends Fragment {
    private RecyclerView rvList;
    private FieldListAdapter adapter;
    private FieldsManager fieldsManager;
    private ProgressDialog progressDialog;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_list, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        rvList = (RecyclerView)view.findViewById(R.id.rvList);
        adapter = new FieldListAdapter(getContext(), new FieldViewModelFactory());

        rvList.setLayoutManager(new LinearLayoutManager(getContext()));
        rvList.setAdapter(adapter);

        setupUI();
    }

    abstract protected void setupUI();


    protected void setForm(List<BaseFieldViewModel> fields) {
        fieldsManager = new FieldsManager(fields);
        adapter.setFields(fields);
    }

    protected boolean isValid(FieldsManager.IFilterAction action) {
        if (fieldsManager != null) {
            boolean isValid = fieldsManager.validate(action);

            fieldsManager.enableShowValidationError();
            rvList.post(new Runnable() {
                @Override
                public void run() {
                    adapter.notifyDataSetChanged();
                }
            });
            return isValid;
        }
        return true;
    }

    protected <T extends BaseFieldViewModel> T findFieldByKey(String key) {
        return fieldsManager.findFirstByKey(key);
    }

    protected <T> T getDataFieldByKey(String key, T defaultValue) {
        BaseFieldViewModel field = findFieldByKey(key);
        if (field != null) {
            T data = field.data();

            if (data != null) {
                return data;
            }
        }

        return defaultValue;
    }

    protected <T> boolean setDataFieldByKey(String key, T data) {
        BaseFieldViewModel field = findFieldByKey(key);
        if (field != null) {
            field.setData(data);
            adapter.notifyChangedField(field);
            return true;
        }

        return false;
    }


    protected void filterFields(FieldsManager.IFilterAction filter, FieldsManager.IFieldAction action) {
        fieldsManager.actionFields(filter, action);
    }

    protected void showAlertMessage(String message) {
        new AlertDialog.Builder(getContext())
                .setTitle("Alert")
                .setMessage(message)
                .setPositiveButton(android.R.string.ok, (d, __) -> {})
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    protected void onStartProgress(String message) {
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }

        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage(message);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", (dialog, __) -> dialog.dismiss());
        progressDialog.show();
    }

    protected void onStopProgress() {
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    protected void refreshFields() {
        if (fieldsManager != null) {
            adapter.setFields(fieldsManager.getFields(field -> !field.getHidden()));
        }
        adapter.notifyDataSetChanged();
    }

}
