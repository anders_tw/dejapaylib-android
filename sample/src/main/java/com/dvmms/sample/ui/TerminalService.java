package com.dvmms.sample.ui;

import android.content.Context;

import com.dvmms.dejapay.DejavooTerminal;
import com.dvmms.dejapay.transports.BluetoothDejavooTransport;
import com.dvmms.dejapay.transports.IDejavooTransport;
import com.dvmms.dejapay.transports.ProxyDejavooTransport;
import com.dvmms.dejapay.transports.WifiDejavooTransport;
import com.dvmms.dejapay.transports.BluetoothDejavooTransport;
import com.dvmms.sample.common.Logger;

/**
 * Created by Platon on 04.07.2016.
 */
public class TerminalService {
    private TerminalSettingsModel settingsModel;
    private DejavooTerminal terminal;

    private static TerminalService instance;

    public static synchronized TerminalService getInstance() {
        if (instance == null) {
            instance = new TerminalService();
        }
        return instance;
    }

    protected TerminalService() { }

    public void setSettingsModel(Context context, TerminalSettingsModel settingsModel) {
        this.settingsModel = settingsModel;

        if (settingsModel != null && settingsModel.getConnection() != null) {
            IDejavooTransport transport = null;

            if (terminal != null) {
                terminal.disconnect();
            }

            switch (settingsModel.getConnection()) {
                case WiFi: {
                    transport = new WifiDejavooTransport(settingsModel.getTpn(), "http", settingsModel.getWifiIp(), settingsModel.getWifiPort());
                    break;
                }
                case Proxy: {
                    transport = new ProxyDejavooTransport(settingsModel.getTpn(), settingsModel.getProxyUrl(), settingsModel.getProxyAuthKey(), settingsModel.getProxyRegisterId());
                    break;
                }
                case Bluetooth: {
                    transport = new BluetoothDejavooTransport(context, settingsModel.getTpn(), settingsModel.getBluetoothDevice(), settingsModel.getBluetoothSecurity(), settingsModel.getBluetoothPassword(), 30 * 60);
                    break;
                }
            }


            terminal = new DejavooTerminal(transport);
        } else {
            terminal = null;
        }

    }

    public DejavooTerminal getTerminal() {
        if (terminal != null) {
//        try {
            terminal.setLogger(new Logger());
//        } catch (IllegalArgumentException e) {
//        }
        }

        return terminal;
    }
}
