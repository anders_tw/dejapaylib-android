package com.dvmms.sample.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.dvmms.sample.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Platon on 05.07.2016.
 */
public class PaymentReceiptFragment extends Fragment {
    @BindView(R.id.webView) WebView webView;
    private static final String ARGUMENT_HTML_KEY = "ARGUMENT_HTML_KEY";
    private String contentHtml;


    public PaymentReceiptFragment() {
        // Required empty public constructor
    }

    public static PaymentReceiptFragment newInstance(String html) {
        PaymentReceiptFragment fragment = new PaymentReceiptFragment();
        Bundle args = new Bundle();
        args.putString(ARGUMENT_HTML_KEY, html);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            contentHtml = getArguments().getString(ARGUMENT_HTML_KEY);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_receipt, container, false);
        ButterKnife.bind(this, v);
        setupUI();
        return v;
    }

    void setupUI() {
        webView.loadData(contentHtml, "text/html; charset=UTF-8", null);
    }

}
