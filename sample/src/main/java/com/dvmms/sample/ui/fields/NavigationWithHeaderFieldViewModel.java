package com.dvmms.sample.ui.fields;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import butterknife.BindView;
import butterknife.ButterKnife;

import com.dvmms.sample.R;
import com.dvmms.sample.ui.view.BaseTextView;

/**
 * Created by Platon on 15.01.2016.
 */
public class NavigationWithHeaderFieldViewModel extends BaseFieldViewModel {
    private boolean hasHeader;
    private IOnClickFieldListener<NavigationWithHeaderFieldViewModel> clickListener;

    protected NavigationWithHeaderFieldViewModel(Builder builder) {
        super(builder);
        hasHeader = builder.hasHeader;
        clickListener = builder.clickListener;
    }

    public static IFieldTypeAdapter fieldTypeAdapter = new IFieldTypeAdapter() {
        @Override
        public int resourceLayoutId() {
            return R.layout.field_navigation_with_header;
        }
    };

    public static class Builder extends BaseFieldViewModel.Builder<Builder> {
        private boolean hasHeader = false;
        private IOnClickFieldListener clickListener;

        public Builder hasHeader(boolean hasHeader) {
            this.hasHeader = hasHeader;
            return this;
        }

        public Builder clickListener(IOnClickFieldListener<NavigationWithHeaderFieldViewModel> clickListener) {
            this.clickListener = clickListener;
            return this;
        }

        public NavigationWithHeaderFieldViewModel build() {
            return new NavigationWithHeaderFieldViewModel(this);
        }
    }

    public static class FieldView extends AbstractFieldView<NavigationWithHeaderFieldViewModel> {
        @BindView(R.id.tvTitle)
        BaseTextView tvTitle;
        @BindView(R.id.tvValue) BaseTextView tvValue;

        public FieldView(Context context) {
            super(context);
        }

        public FieldView(Context context, AttributeSet attrs) {
            super(context, attrs);
        }

        public FieldView(Context context, AttributeSet attrs, int defStyleAttr) {
            super(context, attrs, defStyleAttr);
        }

        @Override
        protected void initializeUI(Context context) {
            View v = inflate(context, R.layout.view_field_navigation_with_header, this);
            ButterKnife.bind(this, v);
        }

        @Override
        protected void initializeViewModel() {

            if (fieldViewModel.hasHeader) {
                tvTitle.setVisibility(View.VISIBLE);
                tvTitle.setText(fieldViewModel.title());
                tvValue.setText(fieldViewModel.value());
            } else {
                tvTitle.setVisibility(View.GONE);
                tvValue.setText(fieldViewModel.title());
            }

//            imgArrow.setVisibility(fieldViewModel.enabled ? View.VISIBLE : View.GONE);

            OnClickListener clickListener = new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (fieldViewModel.clickListener != null && fieldViewModel.enabled) {
                        fieldViewModel.clickListener.onClick(fieldViewModel, v);
                    }
                }
            };

            this.setOnClickListener(clickListener);
        }

        @Override
        public void setErrorMessage(String error) {

        }
    }

}
