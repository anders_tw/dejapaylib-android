package com.dvmms.sample.ui.formatter;

import java.util.Map;

/**
 * Created by Platon on 14.01.2016.
 */
public interface IListFieldFormatter {
    void setPossibleValues(Map<Object, String> possibleValues);
}
