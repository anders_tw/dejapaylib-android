package com.dvmms.sample.ui.formatter;

/**
 * Created by Platon on 07.02.2016.
 */
public class PhoneFieldValidator extends RegexpFieldValidator{
    public PhoneFieldValidator(boolean nullable) {
        super("\\(\\d{3}\\) \\d{3}-\\d{4}", nullable);
    }
    public PhoneFieldValidator() {
        super("\\(\\d{3}\\) \\d{3}-\\d{4}");
    }

}