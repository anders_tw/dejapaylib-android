package com.dvmms.sample.ui.fields;

import android.os.Bundle;

import java.util.LinkedHashMap;
import java.util.Map;

import com.dvmms.sample.ui.ICallback;
import com.dvmms.sample.ui.exception.MethodNotImplementation;
import com.dvmms.sample.ui.formatter.IFieldFormatter;
import com.dvmms.sample.ui.formatter.IValidator;
import com.dvmms.sample.ui.formatter.TextFieldFormatter;


/**
 * Created by Platon on 15.01.2016.
 */
public abstract class BaseFieldViewModel {
    private String title;
    private int code;
    private String promptMessage;
    protected boolean enabled;
    private String key;
    private boolean hidden;
    private int weight;

    protected IFieldFormatter formatter;
    protected Map<IValidator, IInvalidListener> validators;

    private Object data;
    private boolean isValid;
    private Bundle extra;

    private boolean shownValidationError = false;

    private ICallback updatedCallback;

    protected BaseFieldViewModel(Builder builder) {
        this.title = builder.title;
        this.formatter = builder.formatter;
        this.data = builder.data;
        this.isValid = true;
        this.validators = builder.validators;
        this.code = builder.code;
        this.extra = builder.extra;
        this.enabled = builder.enabled;
        this.promptMessage = builder.promptMessage;
        this.key = builder.key;
        this.hidden = builder.hidden;
        this.weight = builder.weight;

        for (IValidator validator : validators.keySet()) {
            this.isValid &= validator.validate(builder.data);
            if (!this.isValid) {
                break;
            }
        }

    }

    public void setUpdatedCallback(ICallback updatedCallback) {
//        logger.v("Updated field callback for update view, field='%s'", toString());
        this.updatedCallback = updatedCallback;
    }

    public Map<IValidator, IInvalidListener> validators() {
        return validators;
    }

    public boolean validate() {
        for (Map.Entry<IValidator, IInvalidListener> entry : validators().entrySet()) {
            if (entry.getKey() != null && !entry.getKey().validate(data)) {
                return false;
            }
        }
        return true;
    }

    public BaseFieldViewModel setDataFromValue(String value) {
//        logger.v("[%s] Set data from value='%s', field='%s'", getClass().getSimpleName(), value, toString());

        Object data = formatter.parse(value);

        setData(data);
        return this;
    }

    public int getWeight() {
        return weight;
    }

    public void setHidden(boolean hidden) {
        this.hidden = hidden;
    }

    public BaseFieldViewModel setTitle(String title) {
        this.title = title;
//        logger.v("Update title='%s'. For field='%s'", title, toString());
        return this;
    }

    public String key() {
        return key;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public BaseFieldViewModel setData(Object data) {
//        logger.d("[%s] Updated data='%s' ,field='%s'", getClass().getSimpleName(), (data == null) ? "null" : data.toString(), toString());
        this.data = data;

        return this;

//        if (this.isValid) {
//            this.data = data;
//        } else {
//            this.data = null;
//        }
    }

    public String getPromptMessage() {
        return promptMessage;
    }

    public int code() {
        return this.code;
    }

    public boolean shownValidationError() {
        return shownValidationError;
    }

    public void setShownValidationError(boolean shownValidationError) {
        this.shownValidationError = shownValidationError;
    }

    public <T> T data() {
        T d = (T)data;

        if (d == null) {
//            logger.w("Data is null. For field='%s'", toString());
        }

        return d;
    }

    public String title() {
        return title;
    }

    public String value() {
        if (formatter != null) {
            String value = formatter.format(data);
//            logger.v("Value='%s'. For field='%s'", value, toString());
            return value;
        }

//        logger.w("Formatter is null");
        String value = (String)data;
//        logger.v("Value='%s'. For field='%s'", value, toString());
        return value;
    }

    public void clear() {
//        logger.v("Clear field='%s'", toString());
        this.isValid = true;
        this.setData(null);
    }

    public boolean isValid() {
        return isValid;
    }

    public Boolean hasData() {
        return (data != null);
    }

    public IFieldFormatter formatter() {
        return formatter;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public void setValidator(IValidator validator, IInvalidListener listener) {
        this.validators.clear();
        this.validators.put(validator, listener);
    }

    public void setIsValid(boolean isValid) {
//        logger.v("[%s] Updated field validate='%s', field='%s'", getClass().getSimpleName(), StringUtils.fromBoolean(isValid), toString());
        this.isValid = isValid;
    }

    public synchronized void updateView() {
//        logger.v("[%s] Update view (has callback='%s'), field='%s'", getClass().getSimpleName(), StringUtils.fromBoolean(updatedCallback != null), toString());
        if (updatedCallback != null) {
            updatedCallback.call();
        }
    }

    public boolean isHidden() {
        return this.hidden;
    }

    @Override
    public String toString() {
        return  this.getClass().getSimpleName() + "{" +
                "data=" + data +
                ", title='" + title + '\'' +
                ", isValid=" + isValid +
                ", number validators=" + String.valueOf(validators.size()) +
                ", formatter=" + formatter +
                '}';
    }

    public boolean getHidden() {
        return hidden;
    }



    public static class Builder <T extends Builder> {
        private String title;
        private int code = 0;
        private String promptMessage = "";
        protected IFieldFormatter formatter = new TextFieldFormatter();
        protected Map<IValidator, IInvalidListener> validators = new LinkedHashMap<>();
        private Object data;
        private Bundle extra = new Bundle();
        private boolean enabled = true;
        private String key = "";
        private boolean hidden = false;
        private int weight = 999;

        public Builder() {}

        public T weight(int weight) {
            this.weight = weight;
            return (T)this;
        }

        public T hidden(boolean hidden) {
            this.hidden = hidden;
            return (T)this;
        }

        public T enabled(boolean enabled) {
            this.enabled = enabled;
            return (T)this;
        }

        public T putExtraString(String key, String value) {
            this.extra.putString(key, value);
            return (T)this;
        }

        public T putExtraInt(String key, Integer value) {
            this.extra.putInt(key, value);
            return (T)this;
        }


        public T extra(Bundle extra) {
            this.extra = extra;
            return (T)this;
        }

        public T key(String key) {
            this.key = key;
            return (T)this;
        }

        public T code(int code) {
            this.code = code;
            return (T)this;
        }

        public T title(String title) {
            this.title = title;
            return (T)this;
        }

        public T formatter(IFieldFormatter formatter) {
            this.formatter = formatter;
            return (T)this;
        }

        public T promptMessage(String promptMessage) {
            this.promptMessage = promptMessage;
            return (T)this;
        }

        public T addValidator(IValidator validator, IInvalidListener listener) {
            if (validator != null) {
                validators.put(validator, listener);
            }
            return (T) this;
        }


        public T data(Object data) {
            this.data = data;
            return (T) this;
        }

        public BaseFieldViewModel build() {
            throw new MethodNotImplementation();
        }

    }
}
