package com.dvmms.sample.ui;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v4.content.SharedPreferencesCompat;

import com.dvmms.sample.R;
import com.dvmms.sample.common.GsonUtilities;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Platon on 04.07.2016.
 */
public class TerminalSettingsModel {
    private static final String TERMINAL_SETTINGS_KEY = "terminal_settings_key";
    private static final String TERMINAL_REF_ID_KEY = "terminal_settings_refId_key";

    public enum Connecvity {
        @SerializedName("WiFi")
        WiFi,
        @SerializedName("Proxy")
        Proxy,
        @SerializedName("Bluetooth")
        Bluetooth
    };

    @SerializedName("Connection")
    Connecvity connection;
    @SerializedName("WiFi_IP")
    String wifiIp;
    @SerializedName("WiFi_Port")
    Integer wifiPort;
    @SerializedName("Bluetooth_Driver")
    String bluetoothDevice;
    @SerializedName("Bluetooth_Security")
    Boolean bluetoothSecurity;
    @SerializedName("Bluetooth_Password")
    String bluetoothPassword;

    @SerializedName("Proxy_Protocol")
    String proxyProtocol;

    @SerializedName("Proxy_Host")
    String proxyHost;

    @SerializedName("Proxy_Path")
    String proxyPath;


    @SerializedName("Proxy_AuthKey")
    String proxyAuthKey;
    @SerializedName("Proxy_RegisterId")
    String proxyRegisterId;

    @SerializedName("TPN")
    String tpn;


    public Connecvity getConnection() {
        return connection;
    }

    public void setConnection(Connecvity connection) {
        this.connection = connection;
    }

    public String getWifiIp() {
        return wifiIp;
    }

    public void setWifiIp(String wifiIp) {
        this.wifiIp = wifiIp;
    }

    public Integer getWifiPort() {
        return wifiPort;
    }

    public void setWifiPort(Integer wifiPort) {
        this.wifiPort = wifiPort;
    }

    public String getBluetoothDevice() {
        return bluetoothDevice;
    }

    public void setBluetoothDevice(String bluetoothDevice) {
        this.bluetoothDevice = bluetoothDevice;
    }

    public Boolean getBluetoothSecurity() {
        return bluetoothSecurity;
    }

    public void setBluetoothSecurity(Boolean bluetoothSecurity) {
        this.bluetoothSecurity = bluetoothSecurity;
    }

    public String getBluetoothPassword() {
        return bluetoothPassword;
    }

    public void setBluetoothPassword(String bluetoothPassword) {
        this.bluetoothPassword = bluetoothPassword;
    }

    public String getProxyUrl() {
        return proxyProtocol+"://"+proxyHost+proxyPath;
//        return proxyUrl;
    }

//    public void setProxyUrl(String proxyUrl) {
//        this.proxyUrl = proxyUrl;
//    }

    public String getProxyAuthKey() {
        return proxyAuthKey;
    }

    public void setProxyAuthKey(String proxyAuthKey) {
        this.proxyAuthKey = proxyAuthKey;
    }

    public String getProxyRegisterId() {
        return proxyRegisterId;
    }

    public void setProxyRegisterId(String proxyRegisterId) {
        this.proxyRegisterId = proxyRegisterId;
    }

    public String getProxyProtocol() {
        return proxyProtocol;
    }

    public TerminalSettingsModel setProxyProtocol(String proxyProtocol) {
        this.proxyProtocol = proxyProtocol;
        return this;
    }

    public String getProxyHost() {
        return proxyHost;
    }

    public TerminalSettingsModel setProxyHost(String proxyHost) {
        this.proxyHost = proxyHost;
        return this;
    }

    public String getProxyPath() {
        return proxyPath;
    }

    public TerminalSettingsModel setProxyPath(String proxyPath) {
        this.proxyPath = proxyPath;
        return this;
    }

    public String getTpn() {
        return tpn != null ? tpn : "";
    }

    public TerminalSettingsModel setTpn(String tpn) {
        this.tpn = tpn;
        return this;
    }

    public boolean save(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(context.getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(TERMINAL_SETTINGS_KEY, GsonUtilities.serialize(this));
        return editor.commit();
    }





    public static TerminalSettingsModel load(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(context.getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        String jsonModel = sharedPref.getString(TERMINAL_SETTINGS_KEY, null);
        if (jsonModel != null) {
            return GsonUtilities.deserialize(jsonModel, TerminalSettingsModel.class);
        }

        // Default Terminal Settings model
        TerminalSettingsModel model = new TerminalSettingsModel();
        model.setWifiIp("");
        model.setWifiPort(0);
        model.setBluetoothSecurity(false);
        model.setProxyProtocol("https");
        model.setProxyHost("spinpos.net");
        model.setProxyPath("/spin");
        model.setTpn("");
        return model;
    }

    public static Integer getReferenceId(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(context.getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        return sharedPref.getInt(TERMINAL_REF_ID_KEY, 1);
    }

    public static boolean saveReferenceId(Context context, int referenceId) {
        SharedPreferences sharedPref = context.getSharedPreferences(context.getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt(TERMINAL_REF_ID_KEY, referenceId);
        return editor.commit();
    }
}
