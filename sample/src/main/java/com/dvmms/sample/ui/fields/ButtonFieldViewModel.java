package com.dvmms.sample.ui.fields;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import butterknife.BindView;
import butterknife.ButterKnife;
import com.dvmms.sample.R;
import com.dvmms.sample.ui.view.BaseButton;

/**
 * Created by Platon on 15.01.2016.
 */
public class ButtonFieldViewModel extends BaseFieldViewModel {
    public enum Type { Plain, Cancelable }
    public enum State { None, Loading }

    private IOnClickFieldListener<ButtonFieldViewModel> clickListener;
    private Type type;
    private String cancelText;
    private State state = State.None;

    protected ButtonFieldViewModel(Builder builder) {
        super(builder);
        clickListener = builder.clickListener;
        type = builder.type;
        cancelText = builder.cancelText;
    }

    public State state() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
    public static IFieldTypeAdapter fieldTypeAdapter = new IFieldTypeAdapter() {
        @Override
        public int resourceLayoutId() {
            return R.layout.field_button;
        }
    };

    public static class Builder extends BaseFieldViewModel.Builder<Builder> {
        private Type type = Type.Plain;
        private IOnClickFieldListener<ButtonFieldViewModel> clickListener;
        private String cancelText = "";

        public Builder type(Type type) {
            this.type = type;
            return this;
        }

        public Builder clickListener(IOnClickFieldListener<ButtonFieldViewModel> clickListener) {
            this.clickListener = clickListener;
            return this;
        }

        public Builder cancelText(String cancelText) {
            this.cancelText = cancelText;
            return this;
        }

        public ButtonFieldViewModel build() {
            return new ButtonFieldViewModel(this);
        }
    }

    public static class FieldView extends AbstractFieldView<ButtonFieldViewModel> {
        @BindView(R.id.btnSubmit)
        BaseButton btnSubmit;

        public FieldView(Context context) {
            super(context);
        }

        public FieldView(Context context, AttributeSet attrs) {
            super(context, attrs);
        }

        public FieldView(Context context, AttributeSet attrs, int defStyleAttr) {
            super(context, attrs, defStyleAttr);
        }

        @Override
        protected void initializeUI(Context context) {
            View view = inflate(context, R.layout.view_field_button, this);
            ButterKnife.bind(this, view);

            btnSubmit.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (fieldViewModel.clickListener != null) {
                        fieldViewModel.clickListener.onClick(fieldViewModel, v);
                    }
                }
            });
        }

        @Override
        protected void initializeViewModel() {
            updateStateView();
            btnSubmit.setEnabled(fieldViewModel.enabled);
        }

        @Override
        public void setErrorMessage(String error) { }

        private void updateStateView() {
            if (fieldViewModel.state == State.None || fieldViewModel.type == Type.Plain) {
                btnSubmit.setText(fieldViewModel.title());
//                pvLoading.stop();
//                pvLoading.setVisibility(View.GONE);
            } else {
                btnSubmit.setText(fieldViewModel.cancelText);
//                pvLoading.setVisibility(View.VISIBLE);
//                pvLoading.start();
            }
        }
    }

}
