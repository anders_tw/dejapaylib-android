package com.dvmms.sample.ui.formatter;

/**
 * Created by Platon on 13.01.2016.
 */
public final class TextFieldFormatter implements IFieldFormatter {
    @Override
    public String format(Object value) {
        if (value == null) {
            return "";
        }

        if (value instanceof Integer) {
            return String.valueOf(value);
        }

        return (String)value;
    }

    @Override
    public Object parse(String value) {
        return value;
    }

    @Override
    public String transformText(String oldValue, String value) {
        return value;
    }

    @Override
    public String toString() {
        return "TextFieldFormatter{}";
    }
}
