package com.dvmms.sample.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.dvmms.dejapay.models.DejavooTransactionResponse;
import com.dvmms.sample.ui.fields.FieldsManager;
import com.dvmms.sample.ui.fields.BaseFieldViewModel;
import com.dvmms.sample.ui.fields.LabelWithHeaderFieldViewModel;
import com.dvmms.sample.ui.fields.SectionFieldViewModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Platon on 05.07.2016.
 */
public class PaymentDetailsFragment extends AbstractFieldsFragment {
    private static final String TRANSACTION_RESPONSE_KEY = "TRANSACTION_RESPONSE_KEY";
    DejavooTransactionResponse transactionResponse;

    public PaymentDetailsFragment() {
        // Required empty public constructor
    }

    public static PaymentDetailsFragment newInstance(DejavooTransactionResponse response) {
        PaymentDetailsFragment fragment = new PaymentDetailsFragment();
        Bundle args = new Bundle();
        args.putSerializable(TRANSACTION_RESPONSE_KEY, response);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            transactionResponse = (DejavooTransactionResponse) getArguments().getSerializable(TRANSACTION_RESPONSE_KEY);
        }
    }

    @Override
    protected void setupUI() {
        List<BaseFieldViewModel> fields = new ArrayList<>();

        fields.add(new LabelWithHeaderFieldViewModel.Builder()
                .title("Payment Type")
                .data(transactionResponse.getPaymentType().name())
                .build()
        );
        fields.add(new LabelWithHeaderFieldViewModel.Builder()
                .title("Transaction Type")
                .data(transactionResponse.getTransactionType().name())
                .build()
        );
        fields.add(new LabelWithHeaderFieldViewModel.Builder()
                .title("Reference ID")
                .data(transactionResponse.getReferenceId())
                .build()
        );
        fields.add(new LabelWithHeaderFieldViewModel.Builder()
                .title("Register ID")
                .data(transactionResponse.getRegisterId())
                .build()
        );
        fields.add(new LabelWithHeaderFieldViewModel.Builder()
                .title("Invoice Number")
                .data(transactionResponse.getInvoiceNumber())
                .build()
        );
        fields.add(new LabelWithHeaderFieldViewModel.Builder()
                .title("Result Code")
                .data(transactionResponse.getResultCode().name())
                .build()
        );
        fields.add(new LabelWithHeaderFieldViewModel.Builder()
                .title("Response Message")
                .data(transactionResponse.getResponseMessage())
                .build()
        );
        fields.add(new LabelWithHeaderFieldViewModel.Builder()
                .title("Message")
                .data(transactionResponse.getMessage())
                .build()
        );
        fields.add(new LabelWithHeaderFieldViewModel.Builder()
                .title("Authentication Code")
                .data(transactionResponse.getAuthenticationCode())
                .build()
        );
        fields.add(new LabelWithHeaderFieldViewModel.Builder()
                .title("PN Reference")
                .data(transactionResponse.getPnReference())
                .build()
        );
        fields.add(new LabelWithHeaderFieldViewModel.Builder()
                .title("EMV Data")
                .data(transactionResponse.getEmvData())
                .build()
        );
        fields.add(new LabelWithHeaderFieldViewModel.Builder()
                .title("Print Receipt")
                .data(transactionResponse.getReceipt(DejavooTransactionResponse.ReceiptType.Customer))
                .build()
        );
//        fields.add(new LabelWithHeaderFieldViewModel.Builder()
//                .title("Error")
//                .data(mResponse.getError())
//                .build()
//        );
//        fields.add(new LabelWithHeaderFieldViewModel.Builder()
//                .title("Error Code")
//                .data(mResponse.getErrorCode())
//                .build()
//        );


        if ( transactionResponse.getExtData("") != null) {
            fields.add(new SectionFieldViewModel.Builder()
                    .title("Extension Data")
                    .build()
            );

            for (Map.Entry<String, String> entry : transactionResponse.getExtData("").entrySet()) {
                fields.add(new LabelWithHeaderFieldViewModel.Builder()
                        .title(entry.getKey())
                        .data(entry.getValue())
                        .build()
                );
            }
        }

        setForm(fields);
    }
}

