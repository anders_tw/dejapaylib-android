package com.dvmms.sample.ui.formatter;

/**
 * Created by Platon on 12.04.2016.
 */
public class PortFieldFormatter implements IFieldFormatter<Integer> {
    private static final Integer MIN_VALUE = 1;
    private static final Integer MAX_VALUE = 65535;

    @Override
    public String format(Integer value) {
        if (value == null || value == 0) {
            return "";
        }
        return String.valueOf(value);
    }

    @Override
    public Integer parse(String value) {
        try {
            if (value.isEmpty()) {
                return null;
            }

            return Integer.valueOf(value);
        } catch (NumberFormatException ex) {
            return null;
        }
    }

    @Override
    public String transformText(String oldValue, String value) {
        if (value.startsWith("0")) {
            return oldValue;
        }

        if (!value.isEmpty()) {
            try {
                Integer port = Integer.parseInt(value);

                if (port >= MIN_VALUE && port <= MAX_VALUE) {
                    return value;
                } else {
                    return oldValue;
                }
            } catch (NumberFormatException ex) {
                return oldValue;
            }
        }
        return "";
    }
}