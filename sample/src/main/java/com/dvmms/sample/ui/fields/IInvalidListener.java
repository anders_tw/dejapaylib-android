package com.dvmms.sample.ui.fields;

import com.dvmms.sample.ui.fields.IErrorableView;

/**
 * Created by Platon on 04.02.2016.
 */
public interface IInvalidListener {
    void onInvalid(IErrorableView view);
}
