package com.dvmms.sample.ui.fragments;

import android.content.Context;

import com.dvmms.dejapay.DejavooTerminal;
import com.dvmms.dejapay.IRequestCallback;
import com.dvmms.dejapay.exception.DejavooThrowable;
import com.dvmms.dejapay.models.DejavooGetCardResponse;
import com.dvmms.sample.common.Logger;
import com.dvmms.sample.screens.navigators.GetCardSampleNavigatorContract;
import com.dvmms.sample.ui.IntegerUtils;
import com.dvmms.sample.ui.TerminalService;
import com.dvmms.sample.ui.fields.BaseFieldViewModel;
import com.dvmms.sample.ui.fields.ButtonFieldViewModel;
import com.dvmms.sample.ui.fields.SectionFieldViewModel;
import com.dvmms.sample.ui.fields.TextFieldViewModel;
import com.dvmms.sample.ui.formatter.IntegerFieldFormatter;
import com.dvmms.sample.ui.formatter.RequiredValidator;

import java.util.ArrayList;
import java.util.List;

public class GetCardFragment extends AbstractFieldsFragment {
    public static final String TAG = GetCardFragment.class.getSimpleName();
    private GetCardSampleNavigatorContract mNavigator;

    @Override
    protected void setupUI() {
        List<BaseFieldViewModel> fields = new ArrayList<>();

        fields.add(new TextFieldViewModel.Builder()
                .key(FIELD_PROMPT)
                .title("Prompt")
                .data("Swipe or Enter Card")
                .type(TextFieldViewModel.Type.Text)
                .maxLength(250)
                .addValidator(new RequiredValidator(), (e) -> e.setErrorMessage("Required"))
                .build()
        );


        fields.add(new TextFieldViewModel.Builder()
                .key(FIELD_TIMEOUT)
                .title("Timeout")
                .data(120)
                .type(TextFieldViewModel.Type.Number)
                .formatter(new IntegerFieldFormatter())
                .build()
        );


        fields.add(new ButtonFieldViewModel.Builder()
                .title("Get Card")
                .clickListener((f,v) -> getCard(
                        getDataFieldByKey(FIELD_PROMPT, ""),
                        getDataFieldByKey(FIELD_TIMEOUT, 120)
                ))
                .build()
        );


        fields.add(new SectionFieldViewModel.Builder()
                .title("Last Read Tracks")
                .build()
        );


        fields.add(new TextFieldViewModel.Builder()
                .key(FIELD_TRACK1)
                .title("Track 1")
                .type(TextFieldViewModel.Type.Text)
                .enabled(false)
                .build()
        );

        fields.add(new TextFieldViewModel.Builder()
                .key(FIELD_TRACK2)
                .title("Track 2")
                .type(TextFieldViewModel.Type.Text)
                .enabled(false)
                .build()
        );

        fields.add(new TextFieldViewModel.Builder()
                .key(FIELD_ACCOUNT)
                .title("Account")
                .type(TextFieldViewModel.Type.Text)
                .enabled(false)
                .build()
        );


        setForm(fields);
    }

    private void getCard(String prompt, Integer timeout) {
        DejavooTerminal terminal = TerminalService.getInstance().getTerminal();

        if (terminal == null) {
            showAlertMessage("DejaPay not configured");
            return;
        }

        onStartProgress("Processing GetCard request");

        terminal.getCard(prompt, IntegerUtils.valueOf(timeout, 120), new IRequestCallback<DejavooGetCardResponse>() {
            @Override
            public void onResponse(DejavooGetCardResponse response) {
                onStopProgress();
                Logger.d(TAG, "GetCard request completed");

                if (response.isSuccess()) {
                    showAlertMessage("GetCard request completed");

                    setDataFieldByKey(FIELD_TRACK1, response.getTrack1());
                    setDataFieldByKey(FIELD_TRACK2, response.getTrack2());
                    setDataFieldByKey(FIELD_ACCOUNT, response.getAccount());
                } else {
                    showAlertMessage(response.getResponseMessage());
                    setDataFieldByKey(FIELD_TRACK1, "");
                    setDataFieldByKey(FIELD_TRACK2, "");
                    setDataFieldByKey(FIELD_ACCOUNT, "");
                }
            }

            @Override
            public void onError(DejavooThrowable t) {
                onStopProgress();
                Logger.e(TAG, t, "GetCard request failed");
                showAlertMessage("GetCard Failed: " + t.toString());
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof GetCardSampleNavigatorContract) {
            mNavigator = (GetCardSampleNavigatorContract) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement GetCardSampleNavigatorContract");
        }
    }

    private final static String FIELD_PROMPT = "Prompt";
    private final static String FIELD_TIMEOUT = "Timeout";
    private final static String FIELD_TRACK1 = "Track1";
    private final static String FIELD_TRACK2 = "Track2";
    private final static String FIELD_ACCOUNT = "Account";


    public static GetCardFragment newInstance() {
        return new GetCardFragment();
    }
}
