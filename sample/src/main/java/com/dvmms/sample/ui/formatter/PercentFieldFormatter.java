package com.dvmms.sample.ui.formatter;

import java.text.DecimalFormat;

/**
 * Created by Platon on 13.01.2016.
 */
public class PercentFieldFormatter implements IFieldFormatter<Double> {
    public static final String PERCENT_SUFFIX = "%";

    @Override
    public String format(Double value) {
        if (value == null) {
            return "";
        }

        if (value == 0) {
            return "0.00" + PERCENT_SUFFIX;
        }

        DecimalFormat df = new DecimalFormat("#.##");
        df.setMaximumFractionDigits(2);
        df.setMinimumFractionDigits(2);
        return df.format(value).replace(",", ".") + PERCENT_SUFFIX;
    }

    @Override
    public Double parse(String value) {
        value = value.replace(PERCENT_SUFFIX, "");

        if (!value.isEmpty()) {
            try {
                return Double.parseDouble(value);
            } catch (NumberFormatException e) {
                // TODO: Add log
                return null;
            }
        }

        return null;
    }

    @Override
    public String transformText(String oldValue, String value) {
        if (value.isEmpty()) {
            return "";
        }

        if (value.contains(PERCENT_SUFFIX)) {
            value = value.replace(PERCENT_SUFFIX, "");
        } else {
            value = value.substring(0, value.length()-1);
        }
        Double data = parse(value);

        if (data == null) {
            return format(0.0);
        }

        if (value.contains(".")) {
            int numberCharsAfPoint = value.substring(value.indexOf('.') + 1, value.length()).length();

            if (numberCharsAfPoint == 0) {
                data /= 100.;
            } else if (numberCharsAfPoint == 1) {
                data /= 10.0;
            } else if (numberCharsAfPoint == 3) {
                data *= 10.0;
            }
        } else {
            data /= 100.;
        }

        if (data >= 100.0 || data < 0.0) {
            return oldValue;
        }

        return format(data);

    }
}
