package com.dvmms.sample.ui.fragments;

import android.content.Context;

import com.dvmms.dejapay.DejavooTerminal;
import com.dvmms.dejapay.IRequestCallback;
import com.dvmms.dejapay.exception.DejavooThrowable;
import com.dvmms.dejapay.models.DejavooPaymentType;
import com.dvmms.dejapay.models.DejavooTransactionCardData;
import com.dvmms.dejapay.models.DejavooTransactionRequest;
import com.dvmms.dejapay.models.requests.DejavooRequestSale;
import com.dvmms.dejapay.models.requests.DejavooResponseSale;
import com.dvmms.sample.common.Logger;
import com.dvmms.sample.screens.navigators.SaleSampleNavigatorContract;
import com.dvmms.sample.ui.TerminalService;
import com.dvmms.sample.ui.fields.BaseFieldViewModel;
import com.dvmms.sample.ui.fields.ButtonFieldViewModel;
import com.dvmms.sample.ui.fields.SwitchFieldViewModel;
import com.dvmms.sample.ui.fields.TextFieldViewModel;
import com.dvmms.sample.ui.formatter.IntegerFieldFormatter;
import com.dvmms.sample.ui.formatter.PriceFieldFormatter;

import java.util.ArrayList;
import java.util.List;

public class SaleOperationListFragment extends AbstractFieldsFragment {
    public static final String TAG = SaleOperationListFragment.class.getSimpleName();
    private SaleSampleNavigatorContract mNavigator;

    @Override
    protected void setupUI() {
        List<BaseFieldViewModel> fields = new ArrayList<>();

        fields.add(new TextFieldViewModel.Builder()
                .key(FIELD_AMOUNT)
                .title("Amount")
                .type(TextFieldViewModel.Type.Number)
                .formatter(new PriceFieldFormatter())
                .build()
        );


        fields.add(new TextFieldViewModel.Builder()
                .key(FIELD_TIP)
                .title("Tip")
                .type(TextFieldViewModel.Type.Number)
                .formatter(new PriceFieldFormatter())
                .build()
        );

        fields.add(new SwitchFieldViewModel.Builder()
                .key(FIELD_MULTIPLE_SALES)
                .title("Multiple Sales")
                .updatedFieldListener(f -> {
                    findFieldByKey(FIELD_NUMBER_SALES).setEnabled(true);
                    refreshFields();
                })
                .build()
        );


        fields.add(new TextFieldViewModel.Builder()
                .key(FIELD_NUMBER_SALES)
                .title("Number of Sales")
                .type(TextFieldViewModel.Type.Number)
                .formatter(new IntegerFieldFormatter())
                .enabled(false)
                .build()
        );



        fields.add(new ButtonFieldViewModel.Builder()
                .title("Send Sale")
                .clickListener((f,v) -> sendSale(
                        getDataFieldByKey(FIELD_AMOUNT, 0.f),
                        getDataFieldByKey(FIELD_TIP, 0.f),
                        getDataFieldByKey(FIELD_MULTIPLE_SALES, false),
                        getDataFieldByKey(FIELD_NUMBER_SALES, 0),
                        1
                ))
                .build()
        );

        setForm(fields);
    }

    private void sendSale(Float amount, Float tip, final boolean isMultipleSales, final int maxIteration, final int iter) {
        if (isMultipleSales) {
            Logger.d(TAG, "Send sale %d of %d iteration", iter, maxIteration);
        }

        DejavooTerminal terminal = TerminalService.getInstance().getTerminal();

        if (terminal == null) {
            showAlertMessage("DejaPay not configured");
            return;
        }

        DejavooRequestSale.Builder builder =  DejavooRequestSale.builder()
                .setPaymentType(DejavooPaymentType.Credit)
                .setAmount( amount != null ? amount.doubleValue() : 0.0 )
                .setTip( tip != null ? tip.doubleValue() : 0.0 )
                .setReceipt(DejavooTransactionRequest.ReceiptType.No);


        if (isMultipleSales) {
            DejavooTransactionCardData cardData = new DejavooTransactionCardData();

            cardData.setEntType(DejavooTransactionCardData.Type.Manual);
            cardData.setPAN("");
            cardData.setTrack2(";5454545454545454=20041015432112345601?");
            cardData.setCardPresent(true);

            builder.setCardData(cardData);
        }


        DejavooRequestSale saleRequest = builder.build();

        onStartProgress("Processing Sale request");

        terminal.processSaleRequest(saleRequest, new IRequestCallback<DejavooResponseSale>() {
            @Override
            public void onResponse(DejavooResponseSale response) {
                onStopProgress();
                Logger.d(TAG, "Sale request completed");

                if (response.isSuccess()) {
                    if (isMultipleSales) {
                        if (iter < maxIteration) {
                            sendSale(amount, tip, isMultipleSales, maxIteration, iter+1);
                        } else {
                            showAlertMessage("Sale request completed");
                        }

                    } else {
                        mNavigator.showResponse(response);
                    }
                } else {
                    showAlertMessage(response.getErrorMessage());
                }
            }

            @Override
            public void onError(DejavooThrowable t) {
                onStopProgress();
                Logger.e(TAG, t, "Sale request failed");
                showAlertMessage("Transaction Failed: " + t.toString());
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof SaleSampleNavigatorContract) {
            mNavigator = (SaleSampleNavigatorContract) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement SettleSampleNavigatorContract");
        }
    }

    private final static String FIELD_AMOUNT = "Amount";
    private final static String FIELD_TIP = "Tip";
    private static final String FIELD_MULTIPLE_SALES = "MultipleSales";
    private static final String FIELD_NUMBER_SALES = "NumberSales";


    public static SaleOperationListFragment newInstance() {
        return new SaleOperationListFragment();
    }
}
