package com.dvmms.sample.ui.fields;

import java.util.HashMap;
import java.util.Map;

import com.dvmms.sample.ui.exception.NotFoundFieldTypeAdapter;
import com.dvmms.sample.ui.fields.ButtonFieldViewModel;
import com.dvmms.sample.ui.fields.IFieldTypeAdapter;
import com.dvmms.sample.ui.fields.LabelWithHeaderFieldViewModel;
import com.dvmms.sample.ui.fields.ListPickerFieldViewModel;
import com.dvmms.sample.ui.fields.NavigationWithHeaderFieldViewModel;
import com.dvmms.sample.ui.fields.SectionFieldViewModel;
import com.dvmms.sample.ui.fields.SwitchFieldViewModel;
import com.dvmms.sample.ui.fields.TextFieldViewModel;

/**
 * Created by Platon on 15.01.2016.
 */
public class FieldViewModelFactory {
    private Map<Class<?>, IFieldTypeAdapter> adapters;

    public FieldViewModelFactory() {
        adapters = new HashMap<>();
        registerFieldTypeAdapter(
                ButtonFieldViewModel.class,
                ButtonFieldViewModel.fieldTypeAdapter
        );
        registerFieldTypeAdapter(
                LabelWithHeaderFieldViewModel.class,
                LabelWithHeaderFieldViewModel.fieldTypeAdapter
        );
        registerFieldTypeAdapter(
                SectionFieldViewModel.class,
                SectionFieldViewModel.fieldTypeAdapter
        );
        registerFieldTypeAdapter(
                SwitchFieldViewModel.class,
                SwitchFieldViewModel.fieldTypeAdapter
        );
        registerFieldTypeAdapter(
                NavigationWithHeaderFieldViewModel.class,
                NavigationWithHeaderFieldViewModel.fieldTypeAdapter
        );
        registerFieldTypeAdapter(
                TextFieldViewModel.class,
                TextFieldViewModel.fieldTypeAdapter
        );
        registerFieldTypeAdapter(
                ListPickerFieldViewModel.class,
                ListPickerFieldViewModel.fieldTypeAdapter
        );
    }

    public void registerFieldTypeAdapter(Class<?> clazz, IFieldTypeAdapter fieldTypeAdapter) {
        adapters.put(clazz, fieldTypeAdapter);
    }

    public int viewTypeFieldTypeAdapter(Class<?> clazz) {
        int index = 0;
        for (Map.Entry<Class<?>, IFieldTypeAdapter> entry : adapters.entrySet()) {
            if (entry.getKey() == clazz) {
                return index;
            }
            index++;
        }
        throw new NotFoundFieldTypeAdapter();
    }

    public IFieldTypeAdapter getFieldAdapterByViewType(int index) {
        int i = 0;
        for (Map.Entry<Class<?>, IFieldTypeAdapter> entry : adapters.entrySet()) {
            if (i == index) {
                return entry.getValue();
            }
            i++;
        }
        throw new NotFoundFieldTypeAdapter();
    }
}
