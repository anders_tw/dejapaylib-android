package com.dvmms.sample.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.dvmms.sample.R;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PaymentFragment extends Fragment {
    @BindView(R.id.tabLayout)
    TabLayout tabLayout;
    @BindView(R.id.viewPager)
    ViewPager viewPager;

    PaymentOperationPagerAdapter adapter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_payment, container, false);
        ButterKnife.bind(this, v);
        setupUI();
        return v;
    }

    void setupUI() {
        adapter = new PaymentOperationPagerAdapter(getChildFragmentManager());
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(3);
        tabLayout.setupWithViewPager(viewPager);
    }

    public enum PaymentOperation {
        Transaction, Printout
    }

    public PaymentFragment() { }

    public static PaymentFragment newInstance() {
        PaymentFragment fragment = new PaymentFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }


    class PaymentOperationPagerAdapter extends FragmentPagerAdapter {
        List<PaymentOperation> operations;
        private Map<PaymentOperation, Fragment> fragments;

        public PaymentOperationPagerAdapter(FragmentManager fm) {
            super(fm);
            this.operations = Arrays.asList(PaymentOperation.Transaction/*, PaymentOperation.Printout*/);
            this.fragments = new LinkedHashMap<>();
        }

        @Override
        public Fragment getItem(int position) {
            PaymentOperation operation = operations.get(position);

            if (!fragments.containsKey(operation)) {
                Fragment fragment = null;

                switch (operation) {
                    case Transaction: {
                        fragment = PaymentTransactionFragment.newInstance();
                        break;
                    }
                    case Printout: {
                        fragment = PaymentPrintoutFragment.newInstance();
                        break;
                    }
                }
//                return fragment;
                fragments.put(operation, fragment);
            }

            return fragments.get(operation);
        }

        @Override
        public int getCount() {
            return (operations != null) ? operations.size() : 0;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            PaymentOperation operation = operations.get(position);
            switch (operation) {
                case Transaction: return "Transaction";
                case Printout: return "Printout";
                default:
                    return "";
            }
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }
    }
}
