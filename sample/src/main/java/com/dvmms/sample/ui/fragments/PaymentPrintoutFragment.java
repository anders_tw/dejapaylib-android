package com.dvmms.sample.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.dvmms.sample.ui.fields.FieldsManager;
import com.dvmms.sample.ui.fields.BaseFieldViewModel;

import java.util.ArrayList;
import java.util.List;

public class PaymentPrintoutFragment extends AbstractFieldsFragment {

    FieldsManager fieldsManager;

    public PaymentPrintoutFragment() {
        // Required empty public constructor
    }

    public static PaymentPrintoutFragment newInstance() {
        PaymentPrintoutFragment fragment = new PaymentPrintoutFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void setupUI() {
        // Setup Fields
        List<BaseFieldViewModel> fields = new ArrayList<>();
        setForm(fields);
    }
}
