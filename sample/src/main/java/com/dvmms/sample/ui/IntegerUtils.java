package com.dvmms.sample.ui;

/**
 * Created by Platon on 01.07.2016.
 */
public class IntegerUtils {
    public static int compare(int lhs, int rhs) {
        return lhs < rhs ? -1 : (lhs == rhs ? 0 : 1);
    }

    public static int valueOf(Integer value, int defaultValue) {
        if (value != null) {
            return value;
        }

        return defaultValue;
    }
}
