package com.dvmms.sample.ui.fields;

import android.content.Context;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import com.dvmms.sample.R;
import com.dvmms.sample.ui.ICallback;

/**
 * Created by Platon on 06.01.2016.
 */
public class FieldListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<BaseFieldViewModel> fieldViewModelList;
    private final LayoutInflater layoutInflater;
    private final FieldViewModelFactory viewModelFactory;

    public FieldListAdapter(Context context, FieldViewModelFactory viewModelFactory) {
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.fieldViewModelList = new ArrayList<>();
        this.viewModelFactory = viewModelFactory;
    }

//    public BaseFieldViewModel findField(int code) {
//        for (BaseFieldViewModel viewModel : fieldViewModelList) {
//            if (viewModel.code() == code) {
//                return viewModel;
//            }
//        }
//        return null;
//    }

    @Override
    public FieldListAdapter.FieldViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        IFieldTypeAdapter fieldTypeAdapter = viewModelFactory.getFieldAdapterByViewType(viewType);

        View v = layoutInflater.inflate(fieldTypeAdapter.resourceLayoutId(), parent, false);

        if (v == null) {
            throw new RuntimeException();
        }

        return new FieldViewHolder(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final BaseFieldViewModel fieldViewModel = this.fieldViewModelList.get(position);
        FieldListAdapter.FieldViewHolder viewHolder = (FieldListAdapter.FieldViewHolder)holder;

        if (viewHolder.vwContainer instanceof IFieldView) {
            IFieldView fieldView = (IFieldView)viewHolder.vwContainer;
            fieldView.setFieldViewModel(fieldViewModel);
        }

    }

    public synchronized void setFields(List<? extends BaseFieldViewModel> fields) {
        List<BaseFieldViewModel> preparedFields = new ArrayList<>();

        for (final BaseFieldViewModel fieldViewModel : fields) {
            if (!fieldViewModel.isHidden()) {
                fieldViewModel.setUpdatedCallback(new ICallback() {
                    @Override
                    public void call() {
                        notifyChangedField(fieldViewModel);
                    }
                });
                preparedFields.add(fieldViewModel);
            }
        }

        this.fieldViewModelList = preparedFields;
        notifyDataSetChanged();
    }

    public synchronized void removeFieldsWithPosition(int position) {
        int removedCount = 0;
        int maxItems = fieldViewModelList.size();

        if (position < maxItems) {
            fieldViewModelList.subList(position, maxItems).clear();
            removedCount = maxItems - position;

            if (removedCount > 0) {
                notifyItemRangeRemoved(position, removedCount);
            }
        }



    }

    public synchronized void appendFields(List<BaseFieldViewModel> fields) {
        int pos = fieldViewModelList.size();

        this.fieldViewModelList.addAll(fields);
        notifyItemRangeInserted(pos, fields.size());

        for (final BaseFieldViewModel viewModel : fields) {
            viewModel.setUpdatedCallback(new ICallback() {
                @Override
                public void call() {
                    notifyChangedField(viewModel);
                }
            });
        }

    }

    public List<BaseFieldViewModel> fieldViewModelList() {
        return fieldViewModelList;
    }

    @Override
    public int getItemCount() {
        return (this.fieldViewModelList != null) ? this.fieldViewModelList.size() : 0;
    }



    @Override
    public int getItemViewType(int position) {
        final BaseFieldViewModel fieldViewModel = this.fieldViewModelList.get(position);
        return viewModelFactory.viewTypeFieldTypeAdapter(fieldViewModel.getClass());
    }


    public void notifyChangedField(final BaseFieldViewModel fieldViewModel) {
        Handler handler = new Handler();

        final Runnable r = new Runnable() {
            public void run() {
                int pos = 0;
                for (BaseFieldViewModel viewModel : fieldViewModelList) {
                    if (viewModel.equals(fieldViewModel)) {
                        notifyItemChanged(pos);
                        break;
                    }
                    pos++;
                }
            }
        };

        handler.post(r);
    }

    public void refresh() {
        Handler handler = new Handler();

        final Runnable r = new Runnable() {
            public void run() {
                notifyDataSetChanged();
            }
        };

        handler.post(r);
    }

    static class FieldViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.vwContainer)
        View vwContainer;
        public FieldViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
