package com.dvmms.sample.ui.fragments;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dvmms.sample.R;
import com.dvmms.dejapay.models.DejavooTransactionResponse;
import com.dvmms.sample.common.ReceiptHelper;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Platon on 05.07.2016.
 */
public class PaymentReportFragment extends Fragment {
    public static final String TAG = PaymentReportFragment.class.getSimpleName();

    private static final String TRANSACTION_RESPONSE_KEY = "TRANSACTION_RESPONSE_KEY";
    @BindView(R.id.tabLayout)
    TabLayout tabLayout;
    @BindView(R.id.viewPager)
    ViewPager viewPager;

    PaymentReportPagerAdapter adapter;
    DejavooTransactionResponse mResponse;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mResponse = (DejavooTransactionResponse) getArguments().getSerializable(TRANSACTION_RESPONSE_KEY);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_report, container, false);
        ButterKnife.bind(this, v);
        setupUI();
        return v;
    }

    void setupUI() {
        List<PaymentReport> reports = new ArrayList<>();
        if (mResponse.hasReceipt(DejavooTransactionResponse.ReceiptType.Merchant)) {
            reports.add(PaymentReport.Merchant_Receipt);
        }
        if (mResponse.hasReceipt(DejavooTransactionResponse.ReceiptType.Customer)) {
            reports.add(PaymentReport.Customer_Receipt);
        }
        reports.add(PaymentReport.Details);

        adapter = new PaymentReportPagerAdapter(getChildFragmentManager(), reports);
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(3);
        tabLayout.setupWithViewPager(viewPager);
    }

    public enum PaymentReport {
        Merchant_Receipt, Customer_Receipt, Details
    }


    public PaymentReportFragment() { }

    public static PaymentReportFragment newInstance(DejavooTransactionResponse response) {
        PaymentReportFragment fragment = new PaymentReportFragment();
        Bundle args = new Bundle();
        args.putSerializable(TRANSACTION_RESPONSE_KEY, response);
        fragment.setArguments(args);
        return fragment;
    }

    class PaymentReportPagerAdapter extends FragmentPagerAdapter {
        List<PaymentReport> reports;
        private Map<PaymentReport, Fragment> fragments;

        public PaymentReportPagerAdapter(FragmentManager fm, List<PaymentReport> reports) {
            super(fm);
            this.reports = reports;
            this.fragments = new LinkedHashMap<>();
        }

        @Override
        public Fragment getItem(int position) {
            PaymentReport report = reports.get(position);

            if (!fragments.containsKey(report)) {
                Fragment fragment = null;

                switch (report) {
                    case Merchant_Receipt: {
                        fragment = PaymentReceiptFragment.newInstance(ReceiptHelper.htmlFromDejavooReceipt(
                                mResponse.getReceipt(DejavooTransactionResponse.ReceiptType.Merchant),
                                mResponse.getSign()
                        ));
                        break;
                    }
                    case Customer_Receipt: {
                        fragment = PaymentReceiptFragment.newInstance(ReceiptHelper.htmlFromDejavooReceipt(
                                mResponse.getReceipt(DejavooTransactionResponse.ReceiptType.Customer),
                                mResponse.getSign()
                        ));
                        break;
                    }
                    case Details: {
                        fragment = PaymentDetailsFragment.newInstance(mResponse);
                        break;
                    }
                }
//                return fragment;
                fragments.put(report, fragment);
            }

            return fragments.get(report);
        }

        @Override
        public int getCount() {
            return (reports != null) ? reports.size() : 0;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            PaymentReport operation = reports.get(position);
            switch (operation) {
                case Merchant_Receipt: return "Merchant";
                case Customer_Receipt: return "Customer";
                case Details: return "Details";
                default:
                    return "";
            }
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }
    }



}
