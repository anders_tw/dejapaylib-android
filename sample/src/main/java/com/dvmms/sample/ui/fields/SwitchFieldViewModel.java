package com.dvmms.sample.ui.fields;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.CompoundButton;

import butterknife.BindView;
import butterknife.ButterKnife;

import com.dvmms.sample.R;
import com.dvmms.sample.ui.view.BaseSwitch;
import com.dvmms.sample.ui.view.BaseTextView;

/**
 * Created by Platon on 15.01.2016.
 */
public class SwitchFieldViewModel extends BaseFieldViewModel {
    private IUpdatedFieldListener<SwitchFieldViewModel> updatedFieldListener;

    protected SwitchFieldViewModel(Builder builder) {
        super(builder);
        updatedFieldListener = builder.updatedFieldListener;
    }

    public static IFieldTypeAdapter fieldTypeAdapter = new IFieldTypeAdapter() {
        @Override
        public int resourceLayoutId() {
            return R.layout.field_switch;
        }
    };

    public static class Builder extends BaseFieldViewModel.Builder<Builder> {
        private IUpdatedFieldListener<SwitchFieldViewModel> updatedFieldListener;
        public Builder updatedFieldListener(IUpdatedFieldListener<SwitchFieldViewModel> updatedFieldListener) {
            this.updatedFieldListener = updatedFieldListener;
            return this;
        }

        public SwitchFieldViewModel build() {
            return new SwitchFieldViewModel(this);
        }
    }

    public static class FieldView extends AbstractFieldView<SwitchFieldViewModel> {
        @BindView(R.id.tvTitle)
        BaseTextView tvTitle;
        @BindView(R.id.swValue)
        BaseSwitch swValue;

        @Override
        protected void initializeUI(Context context) {
            View view = inflate(context, R.layout.view_field_switch, this);
            ButterKnife.bind(view);
        }

        @Override
        protected void initializeViewModel() {
            tvTitle.setText(fieldViewModel.title());
            boolean enabled = (fieldViewModel.<Boolean>data() != null) ? fieldViewModel.<Boolean>data() : false;
            swValue.setChecked(enabled);

            swValue.setEnabled(fieldViewModel.enabled);
            swValue.setClickable(fieldViewModel.enabled);

            swValue.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (!fieldViewModel.enabled) {
                        buttonView.setChecked(true);
                    } else {
                        fieldViewModel.setData(isChecked);

                        if (fieldViewModel.updatedFieldListener != null) {
                            fieldViewModel.updatedFieldListener.onUpdatedField(fieldViewModel);
                        }
                    }
                }
            });
        }

        public FieldView(Context context) {
            super(context);
        }

        public FieldView(Context context, AttributeSet attrs) {
            super(context, attrs);
        }

        public FieldView(Context context, AttributeSet attrs, int defStyleAttr) {
            super(context, attrs, defStyleAttr);
        }

        @Override
        public void setErrorMessage(String error) {

        }
    }

}
