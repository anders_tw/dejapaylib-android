package com.dvmms.sample.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;

import com.dvmms.dejapay.DejavooTerminal;
import com.dvmms.dejapay.IRequestCallback;
import com.dvmms.dejapay.exception.DejavooThrowable;
import com.dvmms.dejapay.models.requests.DejavooResponseSale;
import com.dvmms.dejapay.models.requests.DejavooResponseVoid;
import com.dvmms.sample.common.Logger;
import com.dvmms.sample.screens.navigators.SaleSampleNavigatorContract;
import com.dvmms.sample.ui.TerminalService;
import com.dvmms.sample.ui.fields.BaseFieldViewModel;
import com.dvmms.sample.ui.fields.ButtonFieldViewModel;
import com.dvmms.sample.ui.fields.LabelWithHeaderFieldViewModel;
import com.dvmms.sample.ui.fields.NavigationWithHeaderFieldViewModel;
import com.dvmms.sample.ui.fields.SectionFieldViewModel;
import com.dvmms.sample.ui.fields.TextFieldViewModel;
import com.dvmms.sample.ui.formatter.PriceFieldFormatter;

import java.util.ArrayList;
import java.util.List;

public class SaleDetailsFragment extends AbstractFieldsFragment {
    public static final String TAG = SaleDetailsFragment.class.getSimpleName();

    private SaleSampleNavigatorContract mNavigator;
    private final PriceFieldFormatter mPriceFormatter = new PriceFieldFormatter();
    private DejavooResponseSale mSaleResponse;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mSaleResponse = getArguments().getParcelable(ARG_SALE);
    }

    @Override
    protected void setupUI() {
        updateForm();
    }

    private void updateForm() {
        List<BaseFieldViewModel> fields = new ArrayList<>();

        fields.add(new LabelWithHeaderFieldViewModel.Builder()
                .title("Amount")
                .data(mPriceFormatter.format((float)mSaleResponse.getAmount()))
                .build()
        );

        fields.add(new LabelWithHeaderFieldViewModel.Builder()
                .title("Tip")
                .data(mPriceFormatter.format((float)mSaleResponse.getTip()))
                .build()
        );

        fields.add(new NavigationWithHeaderFieldViewModel.Builder()
                .title("Show receipt")
                .clickListener((f,v) -> showReceipt())
                .build()
        );



        fields.add(new NavigationWithHeaderFieldViewModel.Builder()
                .title("Void Sale")
                .clickListener((f,v) -> voidSale())
                .build()
        );

        fields.add(new SectionFieldViewModel.Builder()
                .title("Tip Adjustment")
                .build()
        );

        fields.add(new TextFieldViewModel.Builder()
                .key("Tip")
                .title("Tip")
                .type(TextFieldViewModel.Type.Number)
                .formatter(new PriceFieldFormatter())
                .build()
        );

        fields.add(new ButtonFieldViewModel.Builder()
                .title("Update Tip")
                .clickListener((f,v) -> {
                    tipAdjust( getDataFieldByKey("Tip", 0.f) );
                })
                .build()
        );

        setForm(fields);
    }

    private void tipAdjust(Float tip) {
        DejavooTerminal terminal = TerminalService.getInstance().getTerminal();

        if (terminal == null) {
            showAlertMessage("DejaPay not configured");
            return;
        }

        onStartProgress("Processing Tip adjustment request");

        terminal.processTipAdjustRequest(mSaleResponse, tip.doubleValue(), new IRequestCallback<DejavooResponseSale>() {
            @Override
            public void onResponse(DejavooResponseSale response) {
                onStopProgress();
                Logger.d(TAG, "Tip Adjustment request completed");

                if (response.isSuccess()) {
                    showAlertMessage("Tip Adjustment request completed");
                    mSaleResponse = response;
                    updateForm();
                } else {
                    showAlertMessage(response.getErrorMessage());
                }
            }

            @Override
            public void onError(DejavooThrowable t) {
                onStopProgress();
                Logger.e(TAG, t, "Tip Adjustment request failed");
                showAlertMessage("Transaction Failed: " + t.toString());
            }
        });
    }

    private void voidSale() {
        DejavooTerminal terminal = TerminalService.getInstance().getTerminal();

        if (terminal == null) {
            showAlertMessage("DejaPay not configured");
            return;
        }

        onStartProgress("Processing Void request");

        terminal.processVoidRequest(mSaleResponse, new IRequestCallback<DejavooResponseVoid>() {
            @Override
            public void onResponse(DejavooResponseVoid response) {
                onStopProgress();
                Logger.d(TAG, "Void request completed");

                if (response.isSuccess()) {
                    showAlertMessage("Void request completed");
                    mNavigator.onSaleVoided();
                } else {
                    showAlertMessage(response.getErrorMessage());
                }
            }

            @Override
            public void onError(DejavooThrowable t) {
                onStopProgress();
                Logger.e(TAG, t, "Void request failed");
                showAlertMessage("Transaction Failed: " + t.toString());
            }
        });
    }

    private void showReceipt() {
        mNavigator.showReceipt(mSaleResponse.getResponse());
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof SaleSampleNavigatorContract) {
            mNavigator = (SaleSampleNavigatorContract) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement SettleSampleNavigatorContract");
        }
    }


    public static SaleDetailsFragment newInstance(DejavooResponseSale saleResponse) {
        SaleDetailsFragment fr = new SaleDetailsFragment();

        Bundle args = new Bundle();
        args.putParcelable(ARG_SALE, saleResponse);
        fr.setArguments(args);

        return fr;
    }

    private static final String ARG_SALE = "ARG_SALE";
}
