package com.dvmms.sample.ui.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;

import com.dvmms.dejapay.DejavooTerminal;
import com.dvmms.dejapay.IDejaPayLogger;
import com.dvmms.dejapay.IRequestCallback;
import com.dvmms.sample.common.JsonConfig;
import com.dvmms.dejapay.exception.DejavooThrowable;
import com.dvmms.dejapay.models.DejavooPaymentType;
import com.dvmms.dejapay.models.DejavooTransactionRequest;
import com.dvmms.dejapay.models.DejavooTransactionResponse;
import com.dvmms.dejapay.models.DejavooTransactionType;
import com.dvmms.sample.ui.fields.FieldsManager;
import com.dvmms.sample.ui.fields.IOnClickFieldListener;
import com.dvmms.sample.ui.fields.IUpdatedFieldListener;
import com.dvmms.sample.ui.TerminalService;
import com.dvmms.sample.ui.TerminalSettingsModel;
import com.dvmms.sample.ui.TransactionConfig;
import com.dvmms.sample.ui.fields.BaseFieldViewModel;
import com.dvmms.sample.ui.fields.ButtonFieldViewModel;
import com.dvmms.sample.ui.fields.ListPickerFieldViewModel;
import com.dvmms.sample.ui.fields.TextFieldViewModel;
import com.dvmms.sample.ui.formatter.IntegerFieldFormatter;
import com.dvmms.sample.ui.formatter.PriceFieldFormatter;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class PaymentTransactionFragment extends AbstractFieldsFragment {
    private IPaymentTransactionFragmentListener mListener;

    List<TransactionConfig> transactionConfigs;

    private static final int CODE_SKIP_FIELD = 100; //
    private static final int CODE_NOSKIP_FIELD = 101; //
    private static DejavooTerminal terminal;
    ProgressDialog progressDialog;


    public PaymentTransactionFragment() {
        // Required empty public constructor
    }

    public static PaymentTransactionFragment newInstance() {
        PaymentTransactionFragment fragment = new PaymentTransactionFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TerminalService.getInstance().setSettingsModel(getContext(), TerminalSettingsModel.load(getContext()));
    }

    @Override
    protected void setupUI() {
        // Setup Fields
        List<BaseFieldViewModel> fields = new ArrayList<>();

        transactionConfigs = JsonConfig.loadConfigList("transactions_configs.json", TransactionConfig.class , getContext());
        Map<Object, String> paymentTypes = new LinkedHashMap<>();

        for (TransactionConfig config : transactionConfigs) {
            paymentTypes.put(config.getPaymentType(), config.getPaymentType());
        }

        fields.add(new ListPickerFieldViewModel.Builder()
                .activity(getActivity())
                .possibleValues(paymentTypes)
                .code(CODE_SKIP_FIELD)
                .key("PaymentType")
                .title("Payment Type")
                .updatedFieldListener(new IUpdatedFieldListener<ListPickerFieldViewModel>() {
                    @Override
                    public void onUpdatedField(ListPickerFieldViewModel model) {
                        updatePaymentType(model.<String>data());
                    }
                })
                .build()
        );
        fields.add(new ListPickerFieldViewModel.Builder()
                .activity(getActivity())
                .possibleValues(new LinkedHashMap<Object, String>())
                .code(CODE_SKIP_FIELD)
                .key("TransType")
                .title("Transaction Type")
                .hidden(true)
                .build()
        );

        Map<Object, String> settlementTypes = new TreeMap<>();
        settlementTypes.put("Close", "Close");
        settlementTypes.put("Clear", "Clear");
        settlementTypes.put("Force", "Force");

        fields.add(new ListPickerFieldViewModel.Builder()
                .activity(getActivity())
                .possibleValues(settlementTypes)
                .key("Param")
                .title("Settlement Type")
                .hidden(true)
                .build()
        );

        fields.add(new TextFieldViewModel.Builder()
                .title("Reference ID")
                .key("RefId")
                .data(String.valueOf(TerminalSettingsModel.getReferenceId(getContext())))
                .type(TextFieldViewModel.Type.Number)
                .hidden(true)
                .build()
        );

        fields.add(new TextFieldViewModel.Builder()
                .title("Amount")
                .type(TextFieldViewModel.Type.Number)
                .key("Amount")
                .formatter(new PriceFieldFormatter())
                .hidden(true)
                .build()
        );

        fields.add(new TextFieldViewModel.Builder()
                .title("Tip")
                .key("Tip")
                .type(TextFieldViewModel.Type.Number)
                .formatter(new PriceFieldFormatter())
                .hidden(true)
                .build()
        );

        fields.add(new TextFieldViewModel.Builder()
                .title("Points")
                .key("Points")
                .type(TextFieldViewModel.Type.Number)
                .formatter(new IntegerFieldFormatter())
                .hidden(true)
                .build()
        );

//        fields.add(new TextFieldViewModel.Builder()
//                .title("Expiry date (MMYY)")
//                .key("ExpDate")
//                .type(TextFieldViewModel.Type.Number)
//                .maxLength(4)
//                .hidden(true)
//                .build()
//        );
//
//        fields.add(new TextFieldViewModel.Builder()
//                .title("CVV")
//                .key("CVV")
//                .type(TextFieldViewModel.Type.Number)
//                .maxLength(3)
//                .hidden(true)
//                .build()
//        );
//
//        fields.add(new TextFieldViewModel.Builder()
//                .title("ZIP Code")
//                .key("AWS")
//                .hidden(true)
//                .build()
//        );

        fields.add(new TextFieldViewModel.Builder()
                .title("Account last 4 digits")
                .key("AcntLast4")
                .hidden(true)
                .build()
        );

        fields.add(new TextFieldViewModel.Builder()
                .title("Auth code")
                .key("AuthCode")
                .hidden(true)
                .build()
        );


        // Frequency missed

        fields.add(new TextFieldViewModel.Builder()
                .title("Token")
                .key("Token")
                .hidden(true)
                .build()
        );



//        fields.add(new TextFieldViewModel.Builder()
//                .title("Register ID")
//                .key("RegisterId")
//                .type(TextFieldViewModel.Type.Number)
//                .hidden(true)
//                .build()
//        );

        fields.add(new TextFieldViewModel.Builder()
                .title("Invoice Number")
                .key("InvNum")
                .hidden(true)
                .build()
        );


        fields.add(new TextFieldViewModel.Builder()
                .title("Clerk ID")
                .key("ClerkId")
                .hidden(true)
                .build()
        );

        fields.add(new TextFieldViewModel.Builder()
                .title("Table Number")
                .key("TableNum")
                .hidden(true)
                .build()
        );

        fields.add(new TextFieldViewModel.Builder()
                .title("Ticket Number")
                .key("TicketNum")
                .hidden(true)
                .build()
        );

        fields.add(new ButtonFieldViewModel.Builder()
                .title("Submit")
                .key("Submit")
                .code(CODE_SKIP_FIELD)
                .clickListener(new IOnClickFieldListener<ButtonFieldViewModel>() {
                    @Override
                    public void onClick(ButtonFieldViewModel field, View v) {
                        sendTransaction(null);
                    }
                })
                .hidden(true)
                .build()
        );

        setForm(fields);
    }

    void updatePaymentType(String paymentType) {
        findFieldByKey("Submit").setHidden(false);

        Map<Object, String> transactionTypes = new LinkedHashMap<>();
        String transType = null;
        final TransactionConfig transactionConfig = getTransactionConfigForPaymentType(paymentType);
        assert (transactionConfig != null);

        for (String transactionType : transactionConfig.getTransactionTypes()) {
            if (transType == null) {
                transType = transactionType;
            }
            transactionTypes.put(transactionType, transactionType);
        }

        ListPickerFieldViewModel transactionTypeField = findFieldByKey("TransType");
        assert (transactionTypeField != null);
        assert (transType != null);

        transactionTypeField.setPossibleValues(transactionTypes);
        transactionTypeField.setData(transType);
        transactionTypeField.setHidden(false);


        // Configuration Fields
        filterFields(field -> {
            return !field.key().equalsIgnoreCase("PaymentType") &&
                    !field.key().equalsIgnoreCase("TransType") &&
                    !field.key().equalsIgnoreCase("Submit");
        }, field -> {
            if (transactionConfig.hasFieldByKey(field.key())) {
                field.setHidden(false);
                field.setCode(CODE_NOSKIP_FIELD);
            } else {
                field.setHidden(true);
                field.setCode(CODE_SKIP_FIELD);
            }
        });

        refreshFields();
    }

    TransactionConfig getTransactionConfigForPaymentType(String paymentType) {
        for (TransactionConfig config : transactionConfigs) {
            if (config.getPaymentType().equalsIgnoreCase(paymentType)) {
                return config;
            }
        }

        return null;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof IPaymentTransactionFragmentListener) {
            mListener = (IPaymentTransactionFragmentListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement IPaymentPrintoutFragmentListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface IPaymentTransactionFragmentListener {
        void onShowTransactionReport(DejavooTransactionResponse response);
    }

    void showSignatureDialog() {
        SignatureDialogFragment.newInstance(new SignatureDialogFragment.ISignatureDialogListener() {
            @Override
            public void onSkipSignature(SignatureDialogFragment dialog) {
                dialog.dismiss();
                sendSignature(null);
            }

            @Override
            public void onSendSignature(SignatureDialogFragment dialog, Bitmap sign) {
                dialog.dismiss();
                sendSignature(sign);
            }
        }).show(getFragmentManager(), "Signature");
    }

    void sendSignature(Bitmap signature) {
        int MAX_SIGNATURE_WIDTH = 384;
        byte[] signBytes = null;
        if (signature != null) {
            if (signature.getWidth() > MAX_SIGNATURE_WIDTH) {
                float aspect = signature.getWidth() / signature.getHeight();
                int scaledHeight = Math.round(MAX_SIGNATURE_WIDTH / aspect);

                signature = Bitmap.createScaledBitmap(signature, MAX_SIGNATURE_WIDTH, scaledHeight, false);
            }

            int quality = 100;
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            signature.compress(Bitmap.CompressFormat.PNG, quality, byteArrayOutputStream);
            signBytes = byteArrayOutputStream.toByteArray();
        }
        sendTransaction(signBytes);

    }

    void sendTransaction(byte[] signatureBytes) {
        final DejavooTerminal terminal = TerminalService.getInstance().getTerminal();

        if (terminal == null) {
            showAlertMessage("DejaPay not configured");
            return;
        }

        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }

        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Sending transaction");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        progressDialog.show();

        final DejavooTransactionRequest request = new DejavooTransactionRequest();

        BaseFieldViewModel paymentTypeField = findFieldByKey("PaymentType");
        String paymentTypeText = paymentTypeField.data();
        for (DejavooPaymentType paymentType : DejavooPaymentType.values()) {
            if (paymentType.name().equalsIgnoreCase(paymentTypeText)) {
                request.setPaymentType(paymentType);
                break;
            }
        }

        BaseFieldViewModel transactionTypeField = findFieldByKey("TransType");
        String transactionTypeText = transactionTypeField.data();
        for (DejavooTransactionType transactionType : DejavooTransactionType.values()) {
            if (transactionType.name().equalsIgnoreCase(transactionTypeText)) {
                request.setTransactionType(transactionType);
                break;
            }
        }

        request.setParameter(this.<String>getValueByKey("Param", null));
        request.setAmount(this.getValueByKey("Amount", 0.0f));
        request.setTip(this.getValueByKey("Tip", 0.f));
        request.setAcntLast4(this.getValueByKey("AcntLast4", ""));
        request.setAuthenticationCode(this.getValueByKey("AuthCode", ""));
        request.setToken(this.<String>getValueByKey("Token", null));
        request.setReferenceId(this.getValueByKey("RefId", ""));

//        request.setRegisterId(this.getValueByKey("RegisterId", ""));

        request.setInvoiceNumber(this.getValueByKey("InvNum", ""));
        request.setClerkId(this.<Integer>getValueByKey("ClerkId", null));
        request.setTableNumber(this.<Integer>getValueByKey("TableNum", null));
        request.setTickerNumber(this.<String>getValueByKey("TicketNum", null));



        request.setReceiptType(DejavooTransactionRequest.ReceiptType.Both);

        request.setSignature(signatureBytes);
        request.setSignatureCapable(true);


        TerminalSettingsModel settingsModel = TerminalSettingsModel.load(getContext());

        if (settingsModel.getConnection() == TerminalSettingsModel.Connecvity.Proxy) {
            request.setRegisterId(settingsModel.getProxyRegisterId());
            request.setAuthenticationKey(settingsModel.getProxyAuthKey());
        } else {
            request.setRegisterId("1");
        }

        terminal.sendTransactionRequest(request, new IRequestCallback<DejavooTransactionResponse>() {
            @Override
            public void onResponse(DejavooTransactionResponse response) {
                progressDialog.dismiss();
                if (response.isSignatureRequired()) {
                    showSignatureDialog();
                } else {
                    if (response.getResultCode() == DejavooTransactionResponse.ResultCode.Failed) {
                        increaseReferenceId(10);
                    } else {
                        increaseReferenceId(1);
                    }
                    mListener.onShowTransactionReport(response);
                }
            }

            @Override
            public void onError(DejavooThrowable throwable) {
                progressDialog.dismiss();
                showAlertMessage("Transaction Failed: " + throwable.toString());
            }
        });

    }

    void increaseReferenceId(int step) {
        BaseFieldViewModel refIdField = findFieldByKey("RefId");
        int refId = TerminalSettingsModel.getReferenceId(getContext());
        if (refIdField != null) {
            refId = Integer.parseInt(refIdField.<String>data());
        }

        TerminalSettingsModel.saveReferenceId(
                getContext(),refId +step);
    }

    <T> T getValueByKey(String key, T defaultValue) {
        BaseFieldViewModel field = findFieldByKey(key);
        if (field != null) {
            if (field.code() == CODE_SKIP_FIELD) {
                return defaultValue;
            }
            return field.<T>data();
        }

        return defaultValue;
    }
}
