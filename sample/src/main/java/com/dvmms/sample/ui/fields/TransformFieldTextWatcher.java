package com.dvmms.sample.ui.fields;

import android.text.Editable;
import android.text.TextWatcher;

import com.dvmms.sample.ui.formatter.IFieldFormatter;
import com.dvmms.sample.ui.formatter.TextFieldFormatter;
import com.dvmms.sample.ui.view.BaseEditText;


/**
 * Created by Platon on 13.01.2016.
 */
public abstract class TransformFieldTextWatcher implements TextWatcher {
    private enum State {
        None,
        Update,
        Transform
    }

    private boolean enabled;
    private final BaseEditText editText;
    private State state;
    private IFieldFormatter formatter;
    private String oldText;

    public TransformFieldTextWatcher(BaseEditText editText) {
        this.state = State.None;
        this.editText = editText;
        this.enabled = true;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        if (this.enabled) {
            oldText = editText.getText().toString();
        }
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (this.enabled) {

        }
    }

    @Override
    public void afterTextChanged(Editable s) {
        if (this.enabled) {
            String text = editText.getText().toString();
            String value = text;
            // TODO: Temporary hack
            if (this.formatter instanceof TextFieldFormatter) {
                textChanged(value);
            } else if (this.formatter != null) {
                value = formatter.transformText(oldText, text);

                if (!text.equals(value)) {
                    state = State.Transform;
                    this.enabled = false;
//                    editText.removeTextChangedListener(this);
                    editText.setText(value);
                    editText.setSelection(value.length());
                    this.enabled = true;
//                    editText.addTextCha1ngedListener(this);
                }
                textChanged(value);
            } else {
                textChanged(value);
            }


        }
    }

    public abstract void textChanged(String value);

    public synchronized void enable() {
        this.enabled = true;
    }

    public void disable() {
        this.enabled = false;
    }

    public void setFormatter(IFieldFormatter formatter) {
        this.formatter = formatter;
    }

    public IFieldFormatter getFormatter() {
        return formatter;
    }
}
