package com.dvmms.sample.ui.formatter;

/**
 * Created by Platon on 24.03.2016.
 */
public class IntegerFieldFormatter implements IFieldFormatter<Integer> {
    @Override
    public String format(Integer value) {
        if (value == null) {
            return "";
        }
        return String.valueOf(value);
    }

    @Override
    public Integer parse(String value) {
        try {
            return Integer.valueOf(value);
        } catch (NumberFormatException ex) {
            return null;
        }
    }

    @Override
    public String transformText(String oldValue, String value) {
        if (!value.isEmpty()) {
            try {
                Integer.parseInt(value);
                return value;
            } catch (NumberFormatException ex) {
                return oldValue;
            }
        }
        return "";
    }
}
