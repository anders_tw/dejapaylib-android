package com.dvmms.sample.ui.fields;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import butterknife.BindView;
import butterknife.ButterKnife;

import com.dvmms.sample.R;
import com.dvmms.sample.ui.view.BaseTextView;

/**
 * Created by Platon on 15.01.2016.
 */
public class SectionFieldViewModel extends BaseFieldViewModel {
    protected SectionFieldViewModel(Builder builder) {
        super(builder);
    }

    public static IFieldTypeAdapter fieldTypeAdapter = new IFieldTypeAdapter() {
        @Override
        public int resourceLayoutId() {
            return R.layout.field_section;
        }
    };

    public static class Builder extends BaseFieldViewModel.Builder<Builder> {
        public SectionFieldViewModel build() {
            return new SectionFieldViewModel(this);
        }
    }

    public static class FieldView extends AbstractFieldView<SectionFieldViewModel> {
        @BindView(R.id.tvTitle)
        BaseTextView tvTitle;

        public FieldView(Context context) {
            super(context);
        }

        public FieldView(Context context, AttributeSet attrs) {
            super(context, attrs);
        }

        public FieldView(Context context, AttributeSet attrs, int defStyleAttr) {
            super(context, attrs, defStyleAttr);
        }

        @Override
        protected void initializeUI(Context context) {
            View v = inflate(context, R.layout.view_field_section, this);
            ButterKnife.bind(this, v);
        }

        @Override
        protected void initializeViewModel() {
            tvTitle.setText(fieldViewModel.title());
        }

        @Override
        public void setErrorMessage(String error) {

        }
    }
}
