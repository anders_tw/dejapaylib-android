package com.dvmms.sample.ui.fields;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.dvmms.sample.ui.IntegerUtils;
import com.dvmms.sample.ui.exception.NotFoundFieldException;
import com.dvmms.sample.ui.fields.BaseFieldViewModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Platon on 01.07.2016.
 */
public class FieldsManager {
    public interface IFilterAction {
        boolean onCheck(BaseFieldViewModel field);
    }

    public interface IFieldAction {
        void action(BaseFieldViewModel field);
    }

    private List<BaseFieldViewModel> fields;

    public FieldsManager(@NonNull List<BaseFieldViewModel> fields) {
        this.fields = new ArrayList<>();
        addFields(fields);
    }
    private boolean isEnabled = false;
    public List<BaseFieldViewModel> fields() {
        return fields;
    }

    public <T extends BaseFieldViewModel> T findFirstByCode(int code) {
        for (BaseFieldViewModel field : fields) {
            if (field.code() == code) {
                T castedClass = (T)field;

                if (castedClass == null) {
                    throw new ClassCastException();
                }

                return castedClass;
            }
        }
        throw new NotFoundFieldException();
    }

    public <T extends BaseFieldViewModel> T findFirstByKey(String key) {
        for (BaseFieldViewModel field : fields) {
            if (field.key().equalsIgnoreCase(key)) {
                T castedClass = (T)field;

                if (castedClass == null) {
                    throw new ClassCastException();
                }

                return castedClass;
            }
        }
        return null;
    }

    public void enableShowValidationError() {
        this.isEnabled = true;
        for (BaseFieldViewModel field : fields) {
            field.setShownValidationError(true);
        }
    }



    public void disableShowValidationError() {
        this.isEnabled = true;
        for (BaseFieldViewModel field : fields) {
            field.setShownValidationError(false);
        }
    }

    public boolean isEnabled() {
        return this.isEnabled;
    }

    public void removeFields(IFilterAction filter) {
        if (filter == null) {
            fields = new ArrayList<>();
            return;
        }

        List<BaseFieldViewModel> res = new ArrayList<>();

        for (BaseFieldViewModel field : fields) {
            if (!filter.onCheck(field)) {
                res.add(field);
            }
        }

        fields = res;
    }

    public boolean validate(@NonNull IFilterAction filter) {
        boolean isValid = true;
        for (BaseFieldViewModel field : fields) {
            if (filter.onCheck(field)) {
                isValid &= field.validate();
            }
        }
        return isValid;
    }

    public List<BaseFieldViewModel> getFields(@Nullable IFilterAction filter) {
        if (filter == null) {
            return fields;
        }

        List<BaseFieldViewModel> res = new ArrayList<>();

        for (BaseFieldViewModel field : fields) {
            if (filter.onCheck(field)) {
                res.add(field);
            }
        }

        return res;
    }


    public void addFields(List<BaseFieldViewModel> fields) {
        this.fields.addAll(fields);
        Collections.sort(this.fields, new Comparator<BaseFieldViewModel>() {
            @Override
            public int compare(BaseFieldViewModel lhs, BaseFieldViewModel rhs) {
                return IntegerUtils.compare(lhs.getWeight(), rhs.getWeight());
            }
        });
    }

    public void actionFields(IFilterAction filter, IFieldAction action) {
        assert(action != null);
        for (BaseFieldViewModel field : getFields(filter)) {
            action.action(field);
        }
    }


}
