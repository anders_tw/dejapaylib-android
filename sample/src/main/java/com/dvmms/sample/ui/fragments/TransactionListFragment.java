package com.dvmms.sample.ui.fragments;

import com.dvmms.dejapay.DejavooTerminal;
import com.dvmms.dejapay.IRequestCallback;
import com.dvmms.dejapay.exception.DejavooThrowable;
import com.dvmms.dejapay.models.DejavooPaymentType;
import com.dvmms.dejapay.models.DejavooTransactionRecord;
import com.dvmms.dejapay.models.requests.DejavooResponseTransactionRecords;
import com.dvmms.sample.common.Logger;
import com.dvmms.sample.ui.TerminalService;
import com.dvmms.sample.ui.fields.BaseFieldViewModel;
import com.dvmms.sample.ui.fields.ButtonFieldViewModel;
import com.dvmms.sample.ui.fields.ListPickerFieldViewModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TransactionListFragment extends AbstractFieldsFragment {
    public static final String TAG = TransactionListFragment.class.getSimpleName();

    @Override
    protected void setupUI() {
        List<BaseFieldViewModel> fields = new ArrayList<>();


        fields.add(new ListPickerFieldViewModel.Builder()
                .activity(getActivity())
                .possibleValues(getPaymentTypes())
                .key(FIELD_PAYMENT_TYPE)
                .title("Payment Type")
                .build()
        );

        fields.add(new ButtonFieldViewModel.Builder()
                .title("Get 10 last transactions")
                .clickListener((f,v) -> getTransactions(
                        getDataFieldByKey(FIELD_PAYMENT_TYPE, DejavooPaymentType.Credit)
                ))
                .build()
        );

        setForm(fields);
    }

    private void getTransactions(DejavooPaymentType paymentType) {
        DejavooTerminal terminal = TerminalService.getInstance().getTerminal();

        if (terminal == null) {
            showAlertMessage("DejaPay not configured");
            return;
        }

        onStartProgress("Processing Get Transactions request");

        terminal.getTransactions(paymentType, 1, 10, new IRequestCallback<DejavooResponseTransactionRecords>() {
            @Override
            public void onResponse(DejavooResponseTransactionRecords response) {
                onStopProgress();
                Logger.d(TAG, "Get Transactions request completed");

                if (response.isSuccess()) {
                    showAlertMessage("Get Transactions request completed");
                    showRecords(response.getRecords());
                } else {
                    showAlertMessage(response.getMessage()+"\n"+response.getResponseMessage());
                }


            }

            @Override
            public void onError(DejavooThrowable t) {
                onStopProgress();
                Logger.e(TAG, t, "Get Transactions request failed");
                showAlertMessage("Get Transactions Failed: " + t.toString());
            }
        });
    }

    private void showRecords(List<DejavooTransactionRecord> records) {
        showAlertMessage("Found " + records.size() + " records");
    }

    private Map<Object, String> getPaymentTypes() {
        return new HashMap<Object, String>(){{
            put(DejavooPaymentType.Credit, "Credit");
            put(DejavooPaymentType.Cash, "Cash");
        }};
    }


    private final static String FIELD_PAYMENT_TYPE = "PaymentType";

    public static TransactionListFragment newInstance() {
        return new TransactionListFragment();
    }
}
