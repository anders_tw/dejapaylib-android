package com.dvmms.sample.ui.formatter;

import java.util.Map;

/**
 * Created by Platon on 14.01.2016.
 */
public class ListFieldFormatter implements IFieldFormatter, IListFieldFormatter {
    private Map<Object, String> possibleValues;

    public ListFieldFormatter(Map<Object, String> possibleValues) {
        this.possibleValues = possibleValues;
    }

    @Override
    public void setPossibleValues(Map<Object, String> possibleValues) {
        this.possibleValues = possibleValues;
    }

    @Override
    public String format(Object value) {
        if (value == null) {
            return "";
        }

        if (!possibleValues.containsKey(value)) {
            return "";
        }

        return possibleValues.get(value);
    }

    @Override
    public Object parse(String value) {
        for (Object key : possibleValues.keySet()) {
            if (possibleValues.get(key).equals(value)) {
                return key;
            }
        }

        return null;
    }

    @Override
    public String transformText(String oldValue, String value) {
        return value;
    }
}
