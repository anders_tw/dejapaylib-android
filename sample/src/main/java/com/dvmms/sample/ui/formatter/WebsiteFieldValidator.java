package com.dvmms.sample.ui.formatter;

/**
 * Created by Platon on 24.03.2016.
 */
public class WebsiteFieldValidator extends RegexpFieldValidator {
    private static final String format = "^(https?:\\/\\/)?([\\da-z\\.-]+)\\.([a-z\\.]{2,6})([\\/\\w \\.-]*)*\\/?$";

    public WebsiteFieldValidator() {
        super(format);
    }

    public WebsiteFieldValidator(boolean nullable) {
        super(format, nullable);
    }
}
