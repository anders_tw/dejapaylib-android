package com.dvmms.sample.ui.fields;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;


import butterknife.BindView;
import butterknife.ButterKnife;
import com.dvmms.sample.R;
import com.dvmms.sample.common.StringUtils;
import com.dvmms.sample.ui.view.BaseTextView;

/**
 * Created by Platon on 15.01.2016.
 */
public class LabelWithHeaderFieldViewModel extends BaseFieldViewModel {
    private int resInfoIcon;

    public LabelWithHeaderFieldViewModel(Builder builder) {
        super(builder);
        resInfoIcon = builder.resInfoIcon;
    }

    public static IFieldTypeAdapter fieldTypeAdapter = new IFieldTypeAdapter() {
        @Override
        public int resourceLayoutId() {
            return R.layout.field_label_with_header;
        }
    };

    public static class Builder extends BaseFieldViewModel.Builder<Builder> {

        private int resInfoIcon = -1;

        public LabelWithHeaderFieldViewModel build() {
            return new LabelWithHeaderFieldViewModel(this);
        }
    }

    public static class FieldView extends AbstractFieldView<LabelWithHeaderFieldViewModel> {
        @BindView(R.id.tvTitle)
        BaseTextView lblTitle;
        @BindView(R.id.tvValue) BaseTextView tvValue;
        @BindView(R.id.tvError) BaseTextView tvError;


        public FieldView(Context context) {
            super(context);
        }

        public FieldView(Context context, AttributeSet attrs) {
            super(context, attrs);
        }

        public FieldView(Context context, AttributeSet attrs, int defStyleAttr) {
            super(context, attrs, defStyleAttr);
        }

        @Override
        protected void initializeUI(Context context) {
            View view = inflate(context, R.layout.view_field_label_with_header, this);
            ButterKnife.bind(this, view);
        }

        @Override
        protected void initializeViewModel() {
            lblTitle.setVisibility(
                    (StringUtils.isEmptyOrNull(fieldViewModel.title()))
                            ? View.GONE
                            : View.VISIBLE
            );
            tvValue.setVisibility(
                    (StringUtils.isEmptyOrNull(fieldViewModel.<String>data()))
                            ? View.GONE
                            : View.VISIBLE
            );

            lblTitle.setText(fieldViewModel.title());
            tvValue.setText(fieldViewModel.value());

            validate();
        }

        @Override
        public void setErrorMessage(String error) {
            if (StringUtils.isEmptyOrNull(error)) {
                tvError.setVisibility(View.GONE);
            } else {
                tvError.setText(error);
                tvError.setVisibility(View.VISIBLE);
            }
        }
    }
}
