package com.dvmms.sample.ui.fields;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckedTextView;
import android.widget.ListAdapter;
import android.widget.ListView;


import com.dvmms.sample.R;
import com.dvmms.sample.ui.view.BaseEditText;
import com.dvmms.sample.ui.view.BaseTextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Platon on 15.01.2016.
 */
public class ListPickerFieldViewModel extends BaseFieldViewModel {
    private IOnClickFieldListener clickFieldListener;
    private IUpdatedFieldListener<ListPickerFieldViewModel> updatedFieldListener;
    private boolean hasHeader;
    private boolean hasClear;
    private Activity activity;
    private Map<Object, String> possibleValues;

    protected ListPickerFieldViewModel(Builder builder) {
        super(builder);
        possibleValues = builder.possibleValues;
        clickFieldListener = builder.clickFieldListener;
        hasHeader = builder.hasHeader;
        hasClear = builder.hasClear;
        updatedFieldListener = builder.updatedFieldListener;
        activity = builder.activity;
    }

    public static IFieldTypeAdapter fieldTypeAdapter = new IFieldTypeAdapter() {
        @Override
        public int resourceLayoutId() {
            return R.layout.field_picker;
        }
    };

    public void setPossibleValues(Map<Object, String> possibleValues) {
        this.possibleValues = possibleValues;
    }

    public static class Builder extends BaseFieldViewModel.Builder<Builder> {
        private IOnClickFieldListener clickFieldListener;
        private boolean hasHeader;
        private boolean hasClear;
        private IUpdatedFieldListener<ListPickerFieldViewModel> updatedFieldListener;
        private Map<Object, String> possibleValues = new TreeMap<>();
        private Activity activity;

        public Builder activity(Activity activity) {
            this.activity = activity;
            return this;
        }

        public Builder updatedFieldListener(IUpdatedFieldListener<ListPickerFieldViewModel> updatedFieldListener) {
            this.updatedFieldListener = updatedFieldListener;
            return this;
        }

        public Builder clickFieldListener(IOnClickFieldListener clickFieldListener) {
            this.clickFieldListener = clickFieldListener;
            return this;
        }

        public Builder hasHeader(boolean hasHeader) {
            this.hasHeader = hasHeader;
            return this;
        }

        public Builder possibleValues(Map<Object, String> possibleValues) {
            this.possibleValues = possibleValues;
            return this;
        }

        public Builder hasClear(boolean hasClear) {
            this.hasClear = hasClear;
            return this;
        }

        public ListPickerFieldViewModel build() {
            return new ListPickerFieldViewModel(this);
        }
    }


    public static class FieldView extends AbstractFieldView<ListPickerFieldViewModel> {
        @BindView(R.id.tvTitle) BaseTextView tvTitle;
        @BindView(R.id.tvValue) BaseEditText tvValue;
        @BindView(R.id.layTextInput) TextInputLayout textInputLayout;

        private FieldAdapter adapter;

        public FieldView(Context context) {
            super(context);
        }

        public FieldView(Context context, AttributeSet attrs) {
            super(context, attrs);
        }

        public FieldView(Context context, AttributeSet attrs, int defStyleAttr) {
            super(context, attrs, defStyleAttr);
        }

        public void initializeUI(final Context context) {
            View view = inflate(context, R.layout.view_field_picker, this);
            ButterKnife.bind(view);

            tvTitle.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    tvValue.requestFocus();
                }
            });
            view.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    tvValue.requestFocus();
                }
            });

        }

        @Override
        protected void initializeViewModel() {
            if (fieldViewModel.hasHeader) {
                tvTitle.setText(fieldViewModel.title());
                tvTitle.setVisibility(View.VISIBLE);
            } else {
                tvTitle.setVisibility(View.GONE);
            }

//            tvValue.setHint(fieldViewModel.title());
            textInputLayout.setHint(fieldViewModel.title());

            Map<Object, String> values = fieldViewModel.possibleValues;
            ArrayList<ListFilterItem> items = new ArrayList<>();

            int pos = 0;
            int foundPos = -1;
            for (Map.Entry<Object, String> entry : values.entrySet()) {
                ListFilterItem item = new ListFilterItem(entry.getKey(), entry.getValue());

                if (fieldViewModel.hasData()) {
                    if (item.key().equals(fieldViewModel.data())) {
                        foundPos = pos;
                    }
                }

                items.add(item);
                pos++;
            }

            if (fieldViewModel.hasData() && foundPos != -1) {
                tvValue.setText(items.get(foundPos).value);
            }


            adapter = new FieldAdapter(getContext(), items);

            tvValue.setOnFocusChangeListener(new OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (hasFocus) {
                        clearFocus();

                        AlertDialog.Builder builder = new AlertDialog.Builder(fieldViewModel.activity);

                        builder.setTitle(fieldViewModel.title());

                        Map<Object, String> values = fieldViewModel.possibleValues;
                        int selectedPos = -1;
                        if (fieldViewModel.hasData()) {
                            int pos = 0;
                            for (Map.Entry<Object, String> entry : values.entrySet()) {
                                ListFilterItem item = new ListFilterItem(entry.getKey(), entry.getValue());

                                if (item.key().equals(fieldViewModel.data())) {
                                    selectedPos = pos;
                                    break;
                                }

                                pos++;
                            }

                        }

                        DialogInterface.OnClickListener clickListener = new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ListView lv = ((AlertDialog) dialog).getListView();

                                if (lv.getCheckedItemPosition() != -1) {
                                    ListFilterItem item = adapter.getDataItem(lv.getCheckedItemPosition());
                                    fieldViewModel.setData(item.key());

                                    tvValue.setText(item.value());
                                    validate();

                                    if (fieldViewModel.updatedFieldListener != null) {
                                        fieldViewModel.updatedFieldListener.onUpdatedField(fieldViewModel);
                                    }
                                }

                                dialog.dismiss();
                            }
                        };


                        builder.setSingleChoiceItems(adapter, selectedPos, clickListener);

//                        builder.setPositiveButton(getContext().getString(R.string.common_alerts_ok), clickListener);

                        builder.setNegativeButton("CANCEL", null);

                        builder.create().show();

                    }
                }
            });
            validate();
        }

        @Override
        public void setErrorMessage(String error) {
            tvValue.setError(error);
        }

        private void clear() {
            tvValue.setText("");
            fieldViewModel.setData(null);
             validate();
        }


        public class ListFilterItem {
            private Object key;
            private String value;

            public ListFilterItem(Object key, String value) {
                this.key = key;
                this.value = value;
            }

            public Object key() {
                return key;
            }

            public String value() {
                return value;
            }

            @Override
            public String toString() {
                return "ListFilterItem{" +
                        "key=" + key +
                        ", value='" + value + '\'' +
                        '}';
            }
        }

        private class FieldAdapter extends BaseAdapter implements ListAdapter {
            private List<ListFilterItem> items;
            private final Context context;
            private final LayoutInflater layoutInflater;

            public FieldAdapter(Context context) {
                this(context, new ArrayList<ListFilterItem>());
            }

            public FieldAdapter(Context context, List<ListFilterItem> items) {
                this.context = context;
                this.items = items;
                this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            }


            @Override
            public int getCount() {
                return items.size();
            }

            @Override
            public Object getItem(int position) {
                return items.get(position);
            }

            public ListFilterItem getDataItem(int position) {
                return items.get(position);
            }

            @Override
            public long getItemId(int position) {
                return 0;
            }

            @Override
            public View getView(int position, View convertView, ViewGroup viewGroup) {
                if (convertView == null) {
                    convertView = layoutInflater.inflate(R.layout.item_list_picker, null);
                }

                CheckedTextView textView = (CheckedTextView)convertView;
                ListFilterItem item = items.get(position);

                textView.setText(item.value());

                return convertView;
            }


            public void setItems(List<ListFilterItem> items) {
                this.items = items;
                notifyDataSetChanged();
            }

        }
    }


}
