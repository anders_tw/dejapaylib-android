package com.dvmms.sample.ui.fields;

import android.content.Context;
import android.support.design.widget.TextInputLayout;
import android.text.InputFilter;
import android.text.InputType;
import android.text.method.PasswordTransformationMethod;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import com.dvmms.sample.R;
import com.dvmms.sample.common.StringUtils;
import com.dvmms.sample.ui.view.BaseEditText;
import com.dvmms.sample.ui.view.BaseTextView;

/**
 * Created by Platon on 15.01.2016.
 */
public class TextFieldViewModel extends BaseFieldViewModel {
    public enum Type { Text, Number, PasswordText, NumberDecimal, Phone, Ip, Email}

    private int maxLength;
    private IUpdatedFieldListener<TextFieldViewModel> updatedFieldListener;
    private Type type;
    private boolean hasHeader;
    private boolean hasClear;
    private boolean hasInfo;
    private TextView.OnEditorActionListener onEditorActionListener;
    private boolean firstUpperCase;

    protected TextFieldViewModel(Builder builder) {
        super(builder);
        this.maxLength = builder.maxLength;
        this.updatedFieldListener = builder.updatedFieldListener;
        this.type = builder.type;
        this.hasHeader = builder.hasHeader;
        this.hasClear = builder.hasClear;
        this.hasInfo = builder.hasInfo;
        this.onEditorActionListener = builder.onEditorActionListener;
        this.firstUpperCase = builder.firstUpperCase;
    }

    public static IFieldTypeAdapter fieldTypeAdapter = new IFieldTypeAdapter() {
        @Override
        public int resourceLayoutId() {
            return R.layout.field_text_with_header;
        }
    };

    public void setMaxLength(int maxLength) {
        this.maxLength = maxLength;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public void setOnEditorActionListener(TextView.OnEditorActionListener onEditorActionListener) {
        this.onEditorActionListener = onEditorActionListener;
    }

    public static class Builder extends BaseFieldViewModel.Builder<Builder> {
        private int maxLength = Integer.MAX_VALUE;
        private IUpdatedFieldListener<TextFieldViewModel> updatedFieldListener;
        private Type type = Type.Text;
        private boolean hasHeader = false;
        private boolean hasClear = false;
        private boolean firstUpperCase = true;

        private boolean hasInfo = false;
        private TextView.OnEditorActionListener onEditorActionListener = null;

        public Builder onEditorActionListener(TextView.OnEditorActionListener onEditorActionListener) {
            this.onEditorActionListener = onEditorActionListener;
            return this;
        }

        public Builder firstUpperCase(boolean firstUpperCase) {
            this.firstUpperCase = firstUpperCase;
            return this;
        }

        public Builder maxLength(int maxLength) {
            this.maxLength = maxLength;
            return this;
        }

        public Builder updatedFieldListener(IUpdatedFieldListener<TextFieldViewModel> updatedFieldListener) {
            this.updatedFieldListener = updatedFieldListener;
            return this;
        }

        public Builder type(Type type) {
            this.type = type;
            return this;
        }

        public Builder hasHeader(boolean hasHeader) {
            this.hasHeader = hasHeader;
            return this;
        }

        public Builder hasClear(boolean hasClear) {
            this.hasClear = hasClear;
            return this;
        }

        public Builder hasInfo(boolean hasInfo) {
            this.hasInfo = hasInfo;
            return this;
        }
        public TextFieldViewModel build() {
            return new TextFieldViewModel(this);
        }
    }



    public static class FieldView extends AbstractFieldView<TextFieldViewModel> {
        @BindView(R.id.tvTitle)
        BaseTextView tvTitle;
        @BindView(R.id.tvValue)
        BaseEditText etValue;
        @BindView(R.id.layTextInput) TextInputLayout layTextInput;

        TransformFieldTextWatcher textWatcher;

        public FieldView(Context context) {
            super(context);
        }

        public FieldView(Context context, AttributeSet attrs) {
            super(context, attrs);
        }

        public FieldView(Context context, AttributeSet attrs, int defStyleAttr) {
            super(context, attrs, defStyleAttr);
        }

        @Override
        protected void initializeUI(Context context) {
            View view = inflate(context, R.layout.view_field_text_with_header, this);
            ButterKnife.bind(view);
            tvTitle.setVisibility(View.GONE);
            textWatcher = new TransformFieldTextWatcher(etValue) {
                @Override
                public void textChanged(String value) {
                    if (fieldViewModel.hasClear) {
                        if (value.isEmpty()) {
                            clearField();
                        } else {
                            fieldViewModel.setDataFromValue(value);
                        }
                        validate();
                    } else {
                        fieldViewModel.setDataFromValue(value);
                        validate();
                    }

                    if (fieldViewModel.updatedFieldListener != null) {
                        fieldViewModel.updatedFieldListener.onUpdatedField(fieldViewModel);
                    }

                }
            };
            tvTitle.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    etValue.requestFocus();
                }
            });
            view.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    etValue.requestFocus();
                }
            });

            etValue.addTextChangedListener(textWatcher);
        }

        @Override
        protected void initializeViewModel() {
            tvTitle.setVisibility(fieldViewModel.hasHeader ? View.VISIBLE : View.GONE);
            textWatcher.setFormatter(fieldViewModel.formatter);

            if (fieldViewModel.hasHeader && !tvTitle.getText().equals(fieldViewModel.title())) {
                tvTitle.setText(fieldViewModel.title());
            }

            etValue.setOnEditorActionListener(fieldViewModel.onEditorActionListener);

            textWatcher.disable();
            if (fieldViewModel.type == Type.Text) {
                if (fieldViewModel.firstUpperCase) {
                    etValue.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
                } else {
                    etValue.setInputType(InputType.TYPE_CLASS_TEXT);
                }
            } else if (fieldViewModel.type == Type.Number) {
                etValue.setInputType(InputType.TYPE_CLASS_NUMBER);
            } else if (fieldViewModel.type == Type.PasswordText) {
                etValue.setInputType(InputType.TYPE_CLASS_TEXT |
                        InputType.TYPE_TEXT_VARIATION_PASSWORD);
                etValue.setTransformationMethod(PasswordTransformationMethod.getInstance());
            } else if (fieldViewModel.type == Type.NumberDecimal) {
                etValue.setInputType(InputType.TYPE_NUMBER_FLAG_DECIMAL | InputType.TYPE_CLASS_NUMBER);
            } else if (fieldViewModel.type == Type.Phone || fieldViewModel.type == Type.Ip) {
                etValue.setInputType(InputType.TYPE_CLASS_PHONE);
            } else if (fieldViewModel.type == Type.Email) {
                etValue.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
            }


            layTextInput.setHint(fieldViewModel.title());
//            if (etValue.getHint() == null || !etValue.getHint().equals(fieldViewModel.title())) {
////                etValue.setHint(fieldViewModel.title());
//                tiValue.setHint(fieldViewModel.title());
//            }

            List<InputFilter> filters = new ArrayList<>();

            if (fieldViewModel.type == Type.Ip) {
                filters.add(new InputFilter() {
                    @Override
                    public CharSequence filter(CharSequence source, int start, int end,
                                               android.text.Spanned dest, int dstart, int dend) {
                        if (end > start) {
                            String destTxt = dest.toString();
                            String resultingTxt = destTxt.substring(0, dstart) + source.subSequence(start, end) + destTxt.substring(dend);
                            if (!resultingTxt.matches ("^\\d{1,3}(\\.(\\d{1,3}(\\.(\\d{1,3}(\\.(\\d{1,3})?)?)?)?)?)?")) {
                                return "";
                            } else {
                                String[] splits = resultingTxt.split("\\.");
                                for (int i=0; i<splits.length; i++) {
                                    if (Integer.valueOf(splits[i]) > 255) {
                                        return "";
                                    }
                                }
                            }
                        }
                        return null;
                    }

                });
            }

            if (fieldViewModel.maxLength != Integer.MAX_VALUE) {
                filters.add(new InputFilter.LengthFilter(fieldViewModel.maxLength));
            }

            etValue.setFilters(filters.toArray(new InputFilter[]{}));
            textWatcher.enable();

            if (!etValue.getText().toString().equals(fieldViewModel.value())) {
                textWatcher.disable();
                etValue.setText(fieldViewModel.value());
                textWatcher.enable();
            }

            validate();

//            if (transformFieldTextWatcher != null) {
//                validate();
//            } else {
//                etValue.addTextChangedListener(transformFieldTextWatcher);
//            }dt

//            etValue.setHelper(fieldViewModel.getPromptMessage());
            etValue.setEnabled(fieldViewModel.enabled);
        }

        private void clearField() {
            fieldViewModel.clear();

            if (fieldViewModel.hasClear) {
                textWatcher.disable();

                etValue.setText("");

                textWatcher.enable();
            }
            validate();
        }


        @Override
        public void setErrorMessage(String error) {
            if (StringUtils.isEmptyOrNull(error)) {
                layTextInput.setError(null);
                layTextInput.setErrorEnabled(false);
            } else {
//                tiValue.setErrorEnabled(true);
                layTextInput.setError(error);
            }
        }
    }

}
