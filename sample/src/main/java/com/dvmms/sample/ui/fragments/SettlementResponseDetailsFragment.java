package com.dvmms.sample.ui.fragments;

import android.os.Bundle;

import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;
import com.dvmms.dejapay.models.DejavooPaymentType;
import com.dvmms.dejapay.models.requests.DejavooResponseSettlement;
import com.dvmms.sample.ui.fields.BaseFieldViewModel;
import com.dvmms.sample.ui.fields.LabelWithHeaderFieldViewModel;
import com.dvmms.sample.ui.fields.SectionFieldViewModel;

import java.util.ArrayList;
import java.util.List;

public class SettlementResponseDetailsFragment extends AbstractFieldsFragment {
    public static final String TAG = SettlementResponseDetailsFragment.class.getSimpleName();

    private static final String ARG_SETTLE_RESPONSE = "ARG_SETTLE_RESPONSE";

    @Override
    protected void setupUI() {
        assert getArguments() != null;
        DejavooResponseSettlement settle = getArguments().getParcelable(ARG_SETTLE_RESPONSE);
        assert settle != null;

        List<BaseFieldViewModel> fields = new ArrayList<>();

        fields.add(new SectionFieldViewModel.Builder()
                .title("Transaction")
                .build()
        );

        fields.add(new LabelWithHeaderFieldViewModel.Builder()
                .title("Result")
                .data(settle.isSuccess() ? "Succeeded" : "Failed")
                .build()
        );
        fields.add(new LabelWithHeaderFieldViewModel.Builder()
                .title("Error Message (if Result failed)")
                .data(settle.getErrorMessage())
                .build()
        );

        if (settle.getPaymentTypes().size() > 0) {
            fields.add(new LabelWithHeaderFieldViewModel.Builder()
                    .title("Payment Types")
                    .data(Stream.of(settle.getPaymentTypes())
                            .map(Enum::name)
                            .collect(Collectors.joining(", "))
                    )
                    .build()
            );

            for (DejavooPaymentType paymentType : settle.getPaymentTypes()) {
                fields.add(new SectionFieldViewModel.Builder()
                        .title(paymentType.name())
                        .build()
                );

                fields.add(new LabelWithHeaderFieldViewModel.Builder()
                        .title("Result Code")
                        .data(settle.isSuccessPaymentType(paymentType) ? "Succeeded" : "Failed")
                        .build()
                );
                fields.add(new LabelWithHeaderFieldViewModel.Builder()
                        .title("Response Message")
                        .data(settle.getResponseMessage(paymentType))
                        .build()
                );
                fields.add(new LabelWithHeaderFieldViewModel.Builder()
                        .title("Batch Number")
                        .data(String.valueOf( settle.getBatchNumber(paymentType) ))
                        .build()
                );
            }
        } else {
            fields.add(new LabelWithHeaderFieldViewModel.Builder()
                    .title("Payment Types")
                    .data("Not Found")
                    .build()
            );
        }

        setForm(fields);
    }


    public static SettlementResponseDetailsFragment newInstance(DejavooResponseSettlement responseSettle) {
        SettlementResponseDetailsFragment fr = new SettlementResponseDetailsFragment();

        Bundle args = new Bundle();
        args.putParcelable(ARG_SETTLE_RESPONSE, responseSettle);
        fr.setArguments(args);

        return fr;
    }


}
