package com.dvmms.sample.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.dvmms.sample.R;
import com.dvmms.sample.screens.navigators.MainNavigatorContract;
import com.dvmms.sample.ui.fields.BaseFieldViewModel;
import com.dvmms.sample.ui.fields.NavigationWithHeaderFieldViewModel;

import java.util.ArrayList;
import java.util.List;

public class MainFragment extends AbstractFieldsFragment {
    public static final String TAG = MainFragment.class.getSimpleName();

    private MainNavigatorContract mNavigator;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    protected void setupUI() {
        List<BaseFieldViewModel> fields = new ArrayList<>();

        fields.add(new NavigationWithHeaderFieldViewModel.Builder()
                .title("Sale Sample")
                .clickListener((f, v) -> mNavigator.navigateToSaleSample())
                .build()
        );

        fields.add(new NavigationWithHeaderFieldViewModel.Builder()
                .title("Settlement Sample")
                .clickListener((f, v) -> mNavigator.navigateToSettlementSample())
                .build()
        );

        fields.add(new NavigationWithHeaderFieldViewModel.Builder()
                .title("Plain Request Sample")
                .clickListener((f, v) -> mNavigator.navigateToPlainRequestSample())
                .build()
        );

        fields.add(new NavigationWithHeaderFieldViewModel.Builder()
                .title("Get Transactions Sample")
                .clickListener((f, v) -> mNavigator.navigateToGetTransactionsSample())
                .build()
        );

        fields.add(new NavigationWithHeaderFieldViewModel.Builder()
                .title("Get Card Sample")
                .clickListener((f, v) -> mNavigator.navigateToGetCardSample())
                .build()
        );

        setForm(fields);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof MainNavigatorContract) {
            mNavigator = (MainNavigatorContract) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement MainNavigatorContract");
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_main, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.mnSettings) {
            mNavigator.navigateToSettings();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public static MainFragment newInstance() {
        return new MainFragment();
    }
}
