package com.dvmms.sample.ui.fragments;

import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;

import com.dvmms.sample.R;
import com.dvmms.sample.common.StringUtils;
import com.dvmms.sample.common.ToastUtils;
import com.dvmms.sample.ui.fields.FieldsManager;
import com.dvmms.sample.ui.fields.IErrorableView;
import com.dvmms.sample.ui.fields.IInvalidListener;
import com.dvmms.sample.ui.fields.IOnClickFieldListener;
import com.dvmms.sample.ui.fields.IUpdatedFieldListener;
import com.dvmms.sample.ui.TerminalService;
import com.dvmms.sample.ui.TerminalSettingsModel;
import com.dvmms.sample.ui.fields.BaseFieldViewModel;
import com.dvmms.sample.ui.fields.ButtonFieldViewModel;
import com.dvmms.sample.ui.fields.ListPickerFieldViewModel;
import com.dvmms.sample.ui.fields.NavigationWithHeaderFieldViewModel;
import com.dvmms.sample.ui.fields.SwitchFieldViewModel;
import com.dvmms.sample.ui.fields.TextFieldViewModel;
import com.dvmms.sample.ui.formatter.IPFieldFormatter;
import com.dvmms.sample.ui.formatter.IPValidator;
import com.dvmms.sample.ui.formatter.PortFieldFormatter;
import com.dvmms.sample.ui.formatter.RequiredValidator;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class SettingsFragment extends AbstractFieldsFragment {
    public static final String TAG = SettingsFragment.class.getSimpleName();

    private ISettingsFragmentListener mListener;
    private ProgressDialog progressDialog;

    private static final int CODE_SKIP_FIELD = 100;
    private static final int CODE_NOSKIP_FIELD = 101;

    public SettingsFragment() {
    }

    public static SettingsFragment newInstance() {
        SettingsFragment fragment = new SettingsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    protected void setupUI() {
        // Setup Fields
        List<BaseFieldViewModel> fields = new ArrayList<>();

        TerminalSettingsModel model = TerminalSettingsModel.load(getContext());

        Map<Object, String> mapConnections = new LinkedHashMap<>();
        mapConnections.put(TerminalSettingsModel.Connecvity.WiFi, "WiFi");
        mapConnections.put(TerminalSettingsModel.Connecvity.Bluetooth, "Bluetooth");
        mapConnections.put(TerminalSettingsModel.Connecvity.Proxy, "Proxy");

        fields.add(new ListPickerFieldViewModel.Builder()
                .activity(getActivity())
                .key("connection")
                .title("Connection")
                .data(model.getConnection())
                .possibleValues(mapConnections)
                .updatedFieldListener(new IUpdatedFieldListener<ListPickerFieldViewModel>() {
                    @Override
                    public void onUpdatedField(ListPickerFieldViewModel field) {
                        setConnectivity(field.<TerminalSettingsModel.Connecvity>data());
                    }
                })
                .build()
        );
        fields.add(new TextFieldViewModel.Builder()
                .key("wifi_ip")
                .title("IP")
                .data(model.getWifiIp())
                .type(TextFieldViewModel.Type.NumberDecimal)
                .formatter(new IPFieldFormatter())
                .addValidator(new IPValidator(), new IInvalidListener() {
                    @Override
                    public void onInvalid(IErrorableView view) {
                        view.setErrorMessage("Invalid WiFi IP");
                    }
                })
                .hidden(true)
                .build()
        );
        fields.add(new TextFieldViewModel.Builder()
                .key("wifi_port")
                .title("Port")
                .data(model.getWifiPort())
                .type(TextFieldViewModel.Type.Number)
                .formatter(new PortFieldFormatter())
                .addValidator(new RequiredValidator(), new IInvalidListener() {
                    @Override
                    public void onInvalid(IErrorableView view) {
                        view.setErrorMessage("Required");
                    }
                })
                .hidden(true)
                .build()
        );
        fields.add(new NavigationWithHeaderFieldViewModel.Builder()
                .key("bluetooth_device")
                .title("MAC Address terminal")
                .hasHeader(true)
                .data(model.getBluetoothDevice())
                .clickListener(new IOnClickFieldListener<NavigationWithHeaderFieldViewModel>() {
                    @Override
                    public void onClick(NavigationWithHeaderFieldViewModel field, View v) {
                        lookingForBluetoothDevices();
                    }
                })
                .hidden(true)
                .build()
        );
        fields.add(new SwitchFieldViewModel.Builder()
                .key("bluetooth_security")
                .title("Security")
                .data(model.getBluetoothSecurity())
                .hidden(true)
                .build()
        );
        fields.add(new TextFieldViewModel.Builder()
                .key("bluetooth_password")
                .title("Password")
                .data(model.getBluetoothPassword())
                .hidden(true)
                .build()
        );

        Map<Object, String> protocols = new LinkedHashMap<>();
        protocols.put("http", "HTTP");
        protocols.put("https", "HTTPS");

        fields.add(new ListPickerFieldViewModel.Builder()
                .key("proxy_protocol")
                .title("Protocol")
                .activity(getActivity())
                .data(model.getProxyProtocol())
                .possibleValues(protocols)
                .addValidator(new RequiredValidator(), new IInvalidListener() {
                    @Override
                    public void onInvalid(IErrorableView v) {
                        v.setErrorMessage("Required");
                    }
                })
                .hidden(true)
                .build()
        );


        fields.add(new TextFieldViewModel.Builder()
                .key("proxy_host")
                .title("Host")
                .data(model.getProxyHost())
                .firstUpperCase(false)
                .hidden(true)
                .addValidator(new RequiredValidator(), new IInvalidListener() {
                    @Override
                    public void onInvalid(IErrorableView v) {
                        v.setErrorMessage("Required");
                    }
                })
                .build()
        );


        fields.add(new TextFieldViewModel.Builder()
                .key("proxy_path")
                .title("Path")
                .data(model.getProxyPath())
                .firstUpperCase(false)
                .hidden(true)
                .addValidator(new RequiredValidator(), new IInvalidListener() {
                    @Override
                    public void onInvalid(IErrorableView v) {
                        v.setErrorMessage("Required");
                    }
                })
                .build()
        );


        fields.add(new TextFieldViewModel.Builder()
                .key("proxy_authKey")
                .title("Authentication Key")
                .data(model.getProxyAuthKey())
                .addValidator(new RequiredValidator(), new IInvalidListener() {
                    @Override
                    public void onInvalid(IErrorableView view) {
                        view.setErrorMessage("Required");
                    }
                })
                .hidden(true)
                .build()
        );
        fields.add(new TextFieldViewModel.Builder()
                .key("proxy_registerId")
                .data(model.getProxyRegisterId())
                .title("Register ID")
                .type(TextFieldViewModel.Type.Number)
                .addValidator(new RequiredValidator(), new IInvalidListener() {
                    @Override
                    public void onInvalid(IErrorableView view) {
                        view.setErrorMessage("Required");
                    }
                })
                .hidden(true)
                .build()
        );

        fields.add(new TextFieldViewModel.Builder()
                .key("tpn")
                .title("TPN")
                .data(model.getTpn())
                .addValidator(new RequiredValidator(), new IInvalidListener() {
                    @Override
                    public void onInvalid(IErrorableView view) {
                        view.setErrorMessage("Required");
                    }
                })
                .build()
        );

        fields.add(new ButtonFieldViewModel.Builder()
                .key("Submit")
                .title("Save")
                .code(CODE_SKIP_FIELD)
                .clickListener(new IOnClickFieldListener<ButtonFieldViewModel>() {
                    @Override
                    public void onClick(ButtonFieldViewModel field, View v) {
                        saveSettings();
                    }
                })
                .hidden(true)
                .build()
        );

        setForm(fields);

        if (model.getConnection() != null) {
            setConnectivity(model.getConnection());
        } else {
            refreshFields();
        }

    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ISettingsFragmentListener) {
            mListener = (ISettingsFragmentListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement IPaymentPrintoutFragmentListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface ISettingsFragmentListener {
        boolean onRequestBluetoothPermissions();
    }

    private List<BluetoothDevice> foundDevices = new ArrayList<>();

    void registerBluetoothBroadcastReceivers() {
        // Register for broadcasts when a device is discovered
        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        this.getActivity().registerReceiver(mReceiver, filter);

        // Register for broadcasts when startDiscovery has finished
        filter = new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        this.getActivity().registerReceiver(mReceiver, filter);
    }

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                if (!StringUtils.isEmptyOrNull(device.getName())) {
                    foundDevices.add(device);
                }
            } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                showBluetoothList(foundDevices);
            }
        }
    };

    void lookingForBluetoothDevices() {
        if (mListener.onRequestBluetoothPermissions()) {
            progressDialog = new ProgressDialog(getContext());
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setMessage("Looking for bluetooth devices");
            progressDialog.show();
            foundDevices = new ArrayList<>();
            registerBluetoothBroadcastReceivers();
            BluetoothAdapter.getDefaultAdapter().startDiscovery();
        }
    }

    void setConnectivity(final TerminalSettingsModel.Connecvity connectivity) {
        findFieldByKey("Submit").setHidden(false);

        filterFields(field -> {
            return (!field.key().equalsIgnoreCase("connection") &&
                    !field.key().equalsIgnoreCase("tpn") &&
                    !field.key().equalsIgnoreCase("Submit"));
        }, field -> {
            field.setHidden(true);
            field.setCode(CODE_SKIP_FIELD);
        });


        filterFields(field -> {
            String actionKey = connectivity.name().toLowerCase();

            if (!actionKey.isEmpty()) {
                return field.key().startsWith(actionKey);
            }
            return false;
        }, field -> {
            field.setHidden(false);
            field.setCode(CODE_NOSKIP_FIELD);
        });
        refreshFields();
    }

    void showBluetoothList(List<BluetoothDevice> devices) {
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
        BluetoothListDialog.create(getContext(), "Bluetooth List", devices, new BluetoothListDialog.IBluetoothListListener() {
            @Override
            public void onSelected(BluetoothDevice bluetoothDevice) {
                findFieldByKey("bluetooth_device").setData(bluetoothDevice.getAddress());
                findFieldByKey("bluetooth_device").updateView();
            }
        }).show();
    }

    void saveSettings() {
        if (isValid(f -> !f.isHidden() && f.code() != CODE_SKIP_FIELD)) {
            TerminalSettingsModel model = new TerminalSettingsModel();

            BaseFieldViewModel connectionField = findFieldByKey("Connection");
            assert (connectionField != null);
            model.setConnection(connectionField.<TerminalSettingsModel.Connecvity>data());

            model.setWifiIp(this.getValueByKey("wifi_ip", ""));
            model.setWifiPort(this.<Integer>getValueByKey("wifi_port", null));
            model.setBluetoothDevice(this.<String>getValueByKey("bluetooth_device", null));
            model.setBluetoothSecurity(this.<Boolean>getValueByKey("bluetooth_security", false));
            model.setBluetoothPassword(this.<String>getValueByKey("bluetooth_password", null));
            model.setProxyProtocol(this.<String>getValueByKey("proxy_protocol", null));
            model.setProxyHost(this.<String>getValueByKey("proxy_host", null));
            model.setProxyPath(this.<String>getValueByKey("proxy_path", null));

            model.setProxyAuthKey(this.<String>getValueByKey("proxy_authKey", null));
            model.setProxyRegisterId(this.<String>getValueByKey("proxy_registerId", null));

            model.setTpn(this.getValueByKey("tpn", ""));

            if (model.save(getContext())) {
                ToastUtils.showToast(getContext(), "Terminal Settings saved");
                TerminalService.getInstance().setSettingsModel(getContext(), model);
            } else {
                ToastUtils.showToast(getContext(), "Terminal Settings did not save");
            }
        } else {
            ToastUtils.showToast(getContext(),"Not all fields are filled");
        }
    }

    <T> T getValueByKey(String key, T defaultValue) {
        BaseFieldViewModel field = findFieldByKey(key);
        if (field != null) {
            if (field.code() == CODE_SKIP_FIELD) {
                return defaultValue;
            }
            return field.<T>data();
        }

        return defaultValue;
    }

}
