package com.dvmms.sample.ui.fragments;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckedTextView;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.dvmms.sample.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Platon on 05.07.2016.
 */
public class BluetoothListDialog {
    public interface IBluetoothListListener {
        void onSelected(BluetoothDevice bluetoothDevice);
    }
    public static AlertDialog create(Context context, String title, List<BluetoothDevice> devices, final IBluetoothListListener listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);

        final FieldAdapter adapter = new FieldAdapter(context);

        DialogInterface.OnClickListener clickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ListView lv = ((AlertDialog) dialog).getListView();

                if (lv.getCheckedItemPosition() != -1) {

                    ListFilterItem item = adapter.getDataItem(lv.getCheckedItemPosition());

                    if (listener != null) {
                        listener.onSelected((BluetoothDevice) item.key());
                    }
                }

                dialog.dismiss();
            }
        };


        List<ListFilterItem> items = new ArrayList<>();
        for (BluetoothDevice device : devices) {
            items.add(new ListFilterItem(device, device.getName()));
        }
        adapter.setItems(items);
        builder.setSingleChoiceItems(adapter, -1, clickListener);

//                        builder.setPositiveButton(getContext().getString(R.string.common_alerts_ok), clickListener);

        builder.setNegativeButton("CANCEL", null);

        return builder.create();
    }


    public static class ListFilterItem {
        private Object key;
        private String value;

        public ListFilterItem(Object key, String value) {
            this.key = key;
            this.value = value;
        }

        public Object key() {
            return key;
        }

        public String value() {
            return value;
        }

        @Override
        public String toString() {
            return "ListFilterItem{" +
                    "key=" + key +
                    ", value='" + value + '\'' +
                    '}';
        }
    }

    private static class FieldAdapter extends BaseAdapter implements ListAdapter {
        private List<ListFilterItem> items;
        private final Context context;
        private final LayoutInflater layoutInflater;

        public FieldAdapter(Context context) {
            this(context, new ArrayList<ListFilterItem>());
        }

        public FieldAdapter(Context context, List<ListFilterItem> items) {
            this.context = context;
            this.items = items;
            this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }


        @Override
        public int getCount() {
            return items.size();
        }

        @Override
        public Object getItem(int position) {
            return items.get(position);
        }

        public ListFilterItem getDataItem(int position) {
            return items.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup viewGroup) {
            if (convertView == null) {
                convertView = layoutInflater.inflate(R.layout.item_list_picker, null);
            }

            CheckedTextView textView = (CheckedTextView)convertView;
            ListFilterItem item = items.get(position);

            textView.setText(item.value());

            return convertView;
        }


        public void setItems(List<ListFilterItem> items) {
            this.items = items;
            notifyDataSetChanged();
        }

    }
}
