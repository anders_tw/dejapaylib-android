package com.dvmms.sample.screens;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.dvmms.sample.R;
import com.dvmms.sample.ui.TerminalService;
import com.dvmms.sample.ui.TerminalSettingsModel;
import com.dvmms.sample.ui.fragments.SettlementOperationListFragment;
import com.dvmms.sample.ui.fragments.TransactionListFragment;

public class TransactionListSampleActivity
        extends AppCompatActivity
{
    private static final String ROOT_BACK_TAG = "ROOT_BACK_TAG";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TerminalService.getInstance().setSettingsModel(this, TerminalSettingsModel.load(this));

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.flContainer, TransactionListFragment.newInstance(), TransactionListFragment.TAG)
                .addToBackStack(ROOT_BACK_TAG)
                .commitAllowingStateLoss();
    }

    public static Intent getCallingIntent(Context context) {
        return new Intent(context, TransactionListSampleActivity.class);
    }
}
