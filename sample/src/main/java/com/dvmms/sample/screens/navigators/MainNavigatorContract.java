package com.dvmms.sample.screens.navigators;

public interface MainNavigatorContract {
    void navigateToSaleSample();
    void navigateToSettlementSample();
    void navigateToPlainRequestSample();
    void navigateToSettings();
    void navigateToGetTransactionsSample();
    void navigateToGetCardSample();
}
