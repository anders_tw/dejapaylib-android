package com.dvmms.sample.screens;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;

import com.dvmms.sample.R;
import com.dvmms.sample.screens.navigators.MainNavigatorContract;
import com.dvmms.sample.ui.fragments.MainFragment;
import com.dvmms.sample.ui.fragments.SettingsFragment;

public class MainActivity
    extends AppCompatActivity
    implements MainNavigatorContract,
        SettingsFragment.ISettingsFragmentListener
{

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.flContainer, MainFragment.newInstance(), MainFragment.TAG)
                .commitAllowingStateLoss();


        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.READ_EXTERNAL_STORAGE)) {
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        0);
            }
        }

    }

    //region MainNavigatorContract
    @Override
    public void navigateToSaleSample() {
        startActivity(SaleSampleActivity.getCallingIntent(this));
    }

    @Override
    public void navigateToSettlementSample() {
        startActivity(SettlementSampleActivity.getCallingIntent(this));
    }

    @Override
    public void navigateToPlainRequestSample() {
        startActivity(PlainRequestActivity.getCallingIntent(this));
    }

    @Override
    public void navigateToSettings() {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.flContainer, SettingsFragment.newInstance(), SettingsFragment.TAG)
                .addToBackStack(SettingsFragment.TAG)
                .commitAllowingStateLoss();
    }

    @Override
    public void navigateToGetTransactionsSample() {
        startActivity(TransactionListSampleActivity.getCallingIntent(this));
    }

    @Override
    public void navigateToGetCardSample() {
        startActivity(GetCardSampleActivity.getCallingIntent(this));
    }
    //endregion

    //region ISettingsFragmentListener
    @Override
    public boolean onRequestBluetoothPermissions() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{
                            android.Manifest.permission.ACCESS_COARSE_LOCATION},
                    1);
            return false;
        }
    }

    //endregion
}
