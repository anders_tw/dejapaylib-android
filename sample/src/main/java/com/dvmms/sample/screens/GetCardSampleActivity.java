package com.dvmms.sample.screens;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.dvmms.sample.R;
import com.dvmms.sample.screens.navigators.GetCardSampleNavigatorContract;
import com.dvmms.sample.ui.TerminalService;
import com.dvmms.sample.ui.TerminalSettingsModel;
import com.dvmms.sample.ui.fragments.GetCardFragment;

public class GetCardSampleActivity
        extends AppCompatActivity
    implements GetCardSampleNavigatorContract
{
    private static final String ROOT_BACK_TAG = "ROOT_BACK_TAG";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TerminalService.getInstance().setSettingsModel(this, TerminalSettingsModel.load(this));

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.flContainer, GetCardFragment.newInstance(), GetCardFragment.TAG)
                .addToBackStack(ROOT_BACK_TAG)
                .commitAllowingStateLoss();
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
            super.onBackPressed();
        } else {
            finish();
        }
    }

    public static Intent getCallingIntent(Context context) {
        return new Intent(context, GetCardSampleActivity.class);
    }
}
