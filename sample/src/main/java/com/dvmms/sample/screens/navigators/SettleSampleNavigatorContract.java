package com.dvmms.sample.screens.navigators;

import com.dvmms.dejapay.models.requests.DejavooResponseSettlement;

public interface SettleSampleNavigatorContract {
    void showResponse(DejavooResponseSettlement response);
}
