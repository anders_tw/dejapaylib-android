package com.dvmms.sample.screens;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.FrameLayout;

import com.crashlytics.android.Crashlytics;
import com.dvmms.dejapay.models.DejavooTransactionResponse;
import com.dvmms.sample.BuildConfig;
import com.dvmms.sample.R;
import com.dvmms.sample.screens.SaleSampleActivity;
import com.dvmms.sample.screens.SettlementSampleActivity;
import com.dvmms.sample.ui.fragments.PaymentDetailsFragment;
import com.dvmms.sample.ui.fragments.PaymentFragment;
import com.dvmms.sample.ui.fragments.PaymentPrintoutFragment;
import com.dvmms.sample.ui.fragments.PaymentReceiptFragment;
import com.dvmms.sample.ui.fragments.PaymentReportFragment;
import com.dvmms.sample.ui.fragments.PaymentTransactionFragment;
import com.dvmms.sample.ui.fragments.SettingsFragment;

import java.util.List;

import io.fabric.sdk.android.Fabric;

public class PlainRequestActivity
        extends AppCompatActivity
        implements PaymentTransactionFragment.IPaymentTransactionFragmentListener
{
    FrameLayout frmLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!BuildConfig.DEBUG) {
            Fabric.with(this, new Crashlytics());
        }
        setContentView(R.layout.activity_main);

        frmLayout = (FrameLayout)findViewById(R.id.flContainer);

        if (savedInstanceState != null) {
            return;
        }

        getSupportFragmentManager().beginTransaction()
                .add(R.id.flContainer, PaymentFragment.newInstance())
                .commitAllowingStateLoss();
    }

    @Override
    public void onBackPressed() {
        if(getFragmentManager().getBackStackEntryCount() == 0) {
            super.onBackPressed();
        }
        else {
            getFragmentManager().popBackStack();
        }
    }

    @Override
    public void onShowTransactionReport(DejavooTransactionResponse response) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.flContainer, PaymentReportFragment.newInstance(response), PaymentReportFragment.TAG)
                .addToBackStack(PaymentReportFragment.TAG)
                .commitAllowingStateLoss();
    }


    public static Intent getCallingIntent(Context context) {
        return new Intent(context, PlainRequestActivity.class);
    }
}
