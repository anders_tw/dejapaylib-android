package com.dvmms.sample.screens;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import com.dvmms.dejapay.models.DejavooTransactionResponse;
import com.dvmms.dejapay.models.requests.DejavooResponseSale;
import com.dvmms.sample.R;
import com.dvmms.sample.common.ToastUtils;
import com.dvmms.sample.screens.navigators.SaleSampleNavigatorContract;
import com.dvmms.sample.ui.TerminalService;
import com.dvmms.sample.ui.TerminalSettingsModel;
import com.dvmms.sample.ui.fragments.PaymentReportFragment;
import com.dvmms.sample.ui.fragments.SaleDetailsFragment;
import com.dvmms.sample.ui.fragments.SaleOperationListFragment;

public class SaleSampleActivity
        extends AppCompatActivity
        implements SaleSampleNavigatorContract {
    private static final String ROOT_BACK_TAG = "ROOT_BACK_TAG";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TerminalService.getInstance().setSettingsModel(this, TerminalSettingsModel.load(this));

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.flContainer, SaleOperationListFragment.newInstance(), SaleOperationListFragment.TAG)
                .addToBackStack(ROOT_BACK_TAG)
                .commitAllowingStateLoss();
    }

    //region SaleSampleNavigatorContract
    @Override
    public void showResponse(DejavooResponseSale saleResponse) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.flContainer, SaleDetailsFragment.newInstance(saleResponse), SaleDetailsFragment.TAG)
                .addToBackStack(SaleDetailsFragment.TAG)
                .commitAllowingStateLoss();
    }

    @Override
    public void showReceipt(DejavooTransactionResponse response) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.flContainer, PaymentReportFragment.newInstance(response), PaymentReportFragment.TAG)
                .addToBackStack(PaymentReportFragment.TAG)
                .commitAllowingStateLoss();
    }

    @Override
    public void onSaleVoided() {
        getSupportFragmentManager().popBackStack(ROOT_BACK_TAG, 0);
        ToastUtils.showToast(this, "Sale voided");
    }
    //endregion


    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
            super.onBackPressed();
        } else {
            finish();
        }
    }

    public static Intent getCallingIntent(Context context) {
        return new Intent(context, SaleSampleActivity.class);
    }
}
