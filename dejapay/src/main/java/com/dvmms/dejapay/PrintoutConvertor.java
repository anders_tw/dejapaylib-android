package com.dvmms.dejapay;

import com.dvmms.dejapay.models.DejavooPrintoutRequest;
import com.dvmms.dejapay.models.DejavooPrintoutResponse;
import com.dvmms.dejapay.models.IDejavooPrintoutItem;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Platon on 09.07.2016.
 */
class PrintoutConvertor
        implements IDejavooSerializer<DejavooPrintoutRequest>,
        IDejavooDeserializer<DejavooPrintoutResponse>
{
    @Override
    public String serialize(DejavooPrintoutRequest request) {
        String data = "";

        data += xmlItem(request.getRegisterId(), "RegisterId");
        data += xmlItem(request.getAuthenticationKey(), "AuthKey");
        data += xmlItem(request.getTpn(), "TPN");

        data += String.format("<printer width=\"%d\">\n", request.getWidth());

        for (IDejavooPrintoutItem item : request.getItems()) {
            data += item.data() + "\n";
        }

        data += "</printer>";
        data = "<request>" + data + "</request>";
        return data;
    }

    @Override
    public DejavooPrintoutResponse deserialize(String data) {
        DejavooPrintoutResponse response = new DejavooPrintoutResponse();

        XmlPullParserFactory xmlFactoryObject = null;
        Map<String, String> tagsText = new HashMap<>();
        data = data.replaceAll(">\\s*<", "><");

        try {
            String valueText = "";
            xmlFactoryObject = XmlPullParserFactory.newInstance();
            XmlPullParser parser = xmlFactoryObject.newPullParser();
            InputStream is = new ByteArrayInputStream(data.getBytes());
            parser.setInput(is, null);

            int event = parser.getEventType();
            String startTag = "";
            while (event != XmlPullParser.END_DOCUMENT) {
                String name = parser.getName();
                switch (event) {
                    case XmlPullParser.START_TAG:
                        startTag = name;
                        break;
                    case XmlPullParser.TEXT:
                        valueText = parser.getText();
                        tagsText.put(startTag, valueText);
                        break;
                    case XmlPullParser.END_TAG: {
                        if (name.equals("RespMSG")) {
                            response.setResponseMessage(valueText);
                        } else if (name.equals("Message")) {
                            response.setMessage(valueText);

                            if (valueText.equalsIgnoreCase("Success")) {
                                response.setResultCode(DejavooPrintoutResponse.ResultCode.Succeded);
                            } else {
                                response.setResultCode(DejavooPrintoutResponse.ResultCode.Failed);
                            }

                        } else if (name.equals("RegisterId")) {
                            response.setRegisterId(valueText);
                        }
                        break;
                    }
                }
                event = parser.next();
            }
            return response;
        } catch (XmlPullParserException ex) {
            return null;
        } catch (IOException ex) {
            return null;
        }
    }

    <T> String xmlItem(T value, String tag) {
        if (value == null) {
            return "";
        }

        if (value instanceof String) {
            String text = (String)value;
            if (!isEmptyOrNull(text)) {
                return "<"+tag+">" + text + "</"+tag+">\n";
            } else {
                return "";
            }
        }

        return "<"+tag+">" + String.valueOf(value) + "</"+tag+">\n";
    }

    static boolean isEmptyOrNull(String text) {
        return (text == null || text.isEmpty());
    }

}
