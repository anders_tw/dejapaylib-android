package com.dvmms.dejapay;

/**
 * Created by Platon on 06.07.2016.
 */
class FloatValidator
        extends DefaultValidator<Float>
        implements IValidator<Float> {

    private final String fieldName;
    private final Float maxFloat = Float.MAX_VALUE;

    public FloatValidator(String fieldName) {
        this.fieldName = fieldName;
    }

    @Override
    public boolean validate(ValidatorContext context, Float field) {
        boolean isValid = (field != null && field <= maxFloat);
        if (!isValid) {
            context.addError("'%s' invalid", fieldName);
        }
        return isValid;
    }
}

