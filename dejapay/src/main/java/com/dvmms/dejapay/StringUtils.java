package com.dvmms.dejapay;

import android.text.TextUtils;

import java.util.UUID;

/**
 * Created by Platon on 26.07.2016.
 */
class StringUtils {
    static boolean isEmptyOrNull(String text) {
        return (text == null || text.isEmpty());
    }
    static String escapeNull(String text) { return (text == null) ? "" : text; }
    public static String fromBoolean(boolean state) {
        return (state ? "Yes" : "No");
    }

    public static String fromArray(String[] array, String joinSymbol) {
        return TextUtils.join(joinSymbol, array);
    }

    public static boolean containsIgnoreCase(String text, String pattern) {
        return text.toUpperCase().contains(pattern.toUpperCase());
    }
}
