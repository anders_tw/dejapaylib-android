package com.dvmms.dejapay;

import com.dvmms.dejapay.models.DejavooGetCardRequest;
import com.dvmms.dejapay.models.DejavooGetCardResponse;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;

public class GetCardConvertor
        implements IDejavooSerializer<DejavooGetCardRequest>,
            IDejavooDeserializer<DejavooGetCardResponse>
{

    @Override
    public DejavooGetCardResponse deserialize(String data) {
        DejavooGetCardResponse.Builder builder =  DejavooGetCardResponse.builder();

        XmlPullParserFactory xmlFactoryObject = null;
        Map<String, String> tagsText = new HashMap<>();
        data = data.replaceAll(">\\s*<", "><");

        try {
            String valueText = "";
            xmlFactoryObject = XmlPullParserFactory.newInstance();
            XmlPullParser parser = xmlFactoryObject.newPullParser();
            InputStream is = new ByteArrayInputStream(data.getBytes());
            parser.setInput(is, null);

            int event = parser.getEventType();
            String startTag = "";
            while (event != XmlPullParser.END_DOCUMENT) {
                String name = parser.getName();
                switch (event) {
                    case XmlPullParser.START_TAG:
                        startTag = name;
                        break;
                    case XmlPullParser.TEXT:
                        valueText = parser.getText();
                        tagsText.put(startTag, valueText);
                        break;
                    case XmlPullParser.END_TAG: {
                        if (name.equalsIgnoreCase("RegisterId")) {
                            builder.setRegisterId(valueText);
                        } else if (name.equalsIgnoreCase("Track1")) {
                            String attrData = parser.getAttributeValue(null, "data");
                            builder.setTrack1(attrData);
                        } else if (name.equalsIgnoreCase("Track2")) {
                            String attrData = parser.getAttributeValue(null, "data");
                            builder.setTrack2(attrData);
                        } else if (name.equalsIgnoreCase("Account")) {
                            String attrData = parser.getAttributeValue(null, "data");
                            builder.setAccount(attrData);
                        } else if (name.equalsIgnoreCase("Message")) {
                            builder.setMessage(valueText);
                        } else if (name.equals("RespMSG")) {
                            builder.setResponseMessage(URLDecoder.decode(valueText, "UTF-8"));
                        }
                        break;
                    }
                }
                event = parser.next();
            }

            return builder.build();
        } catch (XmlPullParserException ex) {
            return null;
        } catch (IOException ex) {
            return null;
        }
    }

    @Override
    public String serialize(DejavooGetCardRequest request) {
        XmlElement root = new XmlElement("request");

        if (!StringUtils.isEmptyOrNull(request.getTpn())) {
            root.addChild(new XmlElement("TPN", StringUtils.escapeNull(request.getTpn())));
        }

        if (!StringUtils.isEmptyOrNull(request.getAuthenticationKey())) {
            root.addChild(new XmlElement("AuthKey", request.getAuthenticationKey()));
        }

        if (!StringUtils.isEmptyOrNull(request.getRegisterId())) {
            root.addChild(new XmlElement("RegisterId", request.getRegisterId()));
        }

        XmlElement elGetCard = new XmlElement("GetCard");
        if (!StringUtils.isEmptyOrNull(request.getPrompt())) {
            elGetCard.addAttribute("prompt", request.getPrompt());
        }

        if (request.getTimeout() > 0) {
            elGetCard.addAttribute("timeout", String.valueOf(request.getTimeout()));
        }

        root.addChild(elGetCard);

        return root.toXml();
    }
}
