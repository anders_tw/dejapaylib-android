package com.dvmms.dejapay.transports;

import com.dvmms.dejapay.exception.DejavooThrowable;

/**
 * A callback interface that implement to receive response when the request is complete. This callback
 * will execute in the background I/O thread, but the result will execute in the main thread.
 */
public interface ITransportCallback {
    /**
     * Called when an asynchronous call completes successfully.
     * @param data the packet in XML form.
     */
    void onResponse(String data);

    /**
     * Called when an asynchronous call fails to complete normally.
     * @param throwable The exception of processing
     */
    void onError(DejavooThrowable throwable);
}
