package com.dvmms.dejapay.transports;

import com.dvmms.dejapay.ICallback;
import com.dvmms.dejapay.IDejaPayLogger;
import com.dvmms.dejapay.models.DejavooTransactionRequest;

/**
 * The interface transport for sending/receiving requests to/from DejaPay terminal
 */
public interface IDejavooTransport {
    /**
     * SPin request type
     */
    enum SPinType {Transaction, Printout}

    /**
     * Connection state
     */
    enum State {None, Connected, Disconnected, Connecting}

    /**
     * Send request to DejaPay terminal
     *
     * @param sPinType request type
     * @param packet request in XML form.
     * @param callback callback
     */
    void sendPacket(SPinType sPinType, String packet, ITransportCallback callback);

    /**
     * Cancel last request to DejaPay terminal
     */
    void cancel();

    /**
     * Get current connection state
     * @param tpn TPN
     * @param callback callback
     */
    void getState(String tpn, ICallback<State> callback);

    /**
     * Stop connection with DejaPay terminal
     */
    void stop();

    /**
     * Connect with DejaPay terminal
     * @return the result of connection
     */
    boolean connect();


    /**
     * Set logger
     * @param logger logger
     */
    void setLogger(IDejaPayLogger logger);


    String getAuthKey();
    String getRegisterId();
    String getTpn();
}
