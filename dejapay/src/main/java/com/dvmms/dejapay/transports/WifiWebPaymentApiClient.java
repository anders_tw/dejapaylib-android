package com.dvmms.dejapay.transports;

import android.net.Uri;
import android.os.AsyncTask;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

public class WifiWebPaymentApiClient implements IPaymentApi {

    private final String path;
    private final int connectionTimeout;
    private final int readTimeout;

    public WifiWebPaymentApiClient(String path, int connectionTimeout, int readTimeout) {
        this.path = path;
        this.connectionTimeout = connectionTimeout;
        this.readTimeout = readTimeout;
    }

    @Override
    public WebRequest<String> getIndex() {
        return new WebRequest<String>(new GetHttpRequestTask(path+"/index.html", connectionTimeout, readTimeout));
    }

    @Override
    public WebRequest<String> sendTransaction(String request) {
        String uri = Uri.parse(path+"/cgi.html")
                .buildUpon()
                .appendQueryParameter("TerminalTransaction", request)
                .build().toString();


        return new WebRequest<>(new GetHttpRequestTask(uri, connectionTimeout, readTimeout));
    }

    @Override
    public WebRequest<String> printout(String request) {
        String uri = Uri.parse(path+"/cgi.html")
                .buildUpon()
                .appendQueryParameter("Printer", request)
                .build().toString();

        return new WebRequest<>(new GetHttpRequestTask(uri, connectionTimeout, readTimeout));
    }

    @Override
    public WebRequest<String> status(String tpn) {
        String uri = Uri.parse(path+"/GetTerminalStatus")
                .buildUpon()
                .appendQueryParameter("tpn", tpn)
                .build().toString();

        return new WebRequest<>(new GetHttpRequestTask(uri, connectionTimeout, readTimeout));
    }

    private static class Response {
        private final int status;
        private final String data;
        private final Exception exception;

        public Response(int status, String data, Exception e) {
            this.status = status;
            this.data = data;
            this.exception = e;
        }

        public Response(Exception e) {
            this(0, "", e);
        }

        public Response(int status, String data) {
            this(status, data, null);
        }


        public boolean isFail() {
            return exception != null;
        }
    }

    private class GetHttpRequestTask extends AsyncTask<Void,Integer, Response> implements IWebClientRequest<String> {

        private final String queryUrl;
        private final int connectTimeout;
        private final int readTimeout;
        private IWebCallback<WebResponse<String>> onCompletion;

        public GetHttpRequestTask(String queryUrl, int connectTimeout, int readTimeout) {
            this.queryUrl = queryUrl;
            this.connectTimeout = connectTimeout;
            this.readTimeout = readTimeout;
        }

        @Override
        protected Response doInBackground(Void... params) {
            try {
                URL url = new URL(queryUrl);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("GET");
                httpURLConnection.setConnectTimeout(connectTimeout*1000);
                httpURLConnection.setReadTimeout(readTimeout*1000);

                try {
                    httpURLConnection.connect();

                    String data = readFullyAsString(httpURLConnection.getInputStream(), "UTF-8");
                    int status = httpURLConnection.getResponseCode();

                    return new Response(status, data);

                } catch (Exception e) {
                    return new Response(e);
                } finally {
                    // this is done so that there are no open connections left when this task is going to complete
                    httpURLConnection.disconnect();
                }


            } catch (Exception e){
                return new Response(e);
            }
        }

        @Override
        protected void onPostExecute(Response response) {
            if (onCompletion != null) {
                if (response.isFail()) {
                    onCompletion.failure(response.exception);
                } else {
                    onCompletion.success(new WebResponse<>(response.status, response.data));
                }
            }
        }

        @Override
        public void cancel() {
            cancel(true);
        }

        @Override
        public void execute(IWebCallback<WebResponse<String>> completion) {
            this.onCompletion = completion;
            super.execute();
        }
    }

    public static String readFullyAsString(InputStream inputStream, String encoding)
            throws IOException {
        return readFully(inputStream).toString(encoding);
    }

    public static byte[] readFullyAsBytes(InputStream inputStream)
            throws IOException {
        return readFully(inputStream).toByteArray();
    }

    private static ByteArrayOutputStream readFully(InputStream inputStream)
            throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int length = 0;
        while ((length = inputStream.read(buffer)) != -1) {
            baos.write(buffer, 0, length);
        }
        return baos;
    }

}
