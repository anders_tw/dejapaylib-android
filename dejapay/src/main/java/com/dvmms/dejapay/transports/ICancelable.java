package com.dvmms.dejapay.transports;

public interface ICancelable {
    void cancel();
}
