package com.dvmms.dejapay.transports;

public interface IRunnable<T> {
    void execute(T completion);
}
