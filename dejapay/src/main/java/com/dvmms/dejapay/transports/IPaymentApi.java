package com.dvmms.dejapay.transports;


import com.dvmms.dejapay.ICallback;

/**
 * Created by Platon on 29.06.2016.
 */
interface IPaymentApi {
    WebRequest<String> getIndex();
    WebRequest<String> sendTransaction(String request);
    WebRequest<String> printout(String request);
    WebRequest<String> status(String tpn);
}

