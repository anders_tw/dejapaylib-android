package com.dvmms.dejapay.transports;

import android.content.Context;
import android.os.AsyncTask;

import com.dvmms.dejapay.FakeLogger;
import com.dvmms.dejapay.ICallback;
import com.dvmms.dejapay.IDejaPayLogger;

/**
 * Bluetooth transport to DejaPay terminal
 */
public class BluetoothDejavooTransport implements IDejavooTransport {
    private final String tpn;
    private final String macAddress;
    private final boolean security;
    private final String password;
    private final String registerId;
    private final String authKey;
    private final int timeoutSeconds;

    BluetoothSendPacketTask sendPacketTask;

    private IDejaPayLogger logger;
    private IBluetoothService bluetoothService;
    private ITransportCallback transportCallback;

    public BluetoothDejavooTransport(Context context, String tpn, String macAddress, boolean security, String password, String registerId, String authKey, int timeoutSeconds) {
        this.tpn = tpn;
        this.macAddress = macAddress;
        this.security = security;
        this.password = password;
        this.bluetoothService = BluetoothService.getInstance(context);
        this.registerId = registerId;
        this.authKey = authKey;
        this.logger = new FakeLogger();
        this.timeoutSeconds = timeoutSeconds;
    }

    public BluetoothDejavooTransport(Context context, String tpn, String macAddress, boolean security, String password) {
        this(context, tpn, macAddress, security, password, "1", "", 120);
    }

    public BluetoothDejavooTransport(Context context, String tpn, String macAddress, boolean security, String password, int timeoutSeconds) {
        this(context, tpn, macAddress, security, password, "1", "", timeoutSeconds);
    }

    @Override
    public void sendPacket(SPinType sPinType, String packet, ITransportCallback callback) {
        String param;

        if (sPinType == SPinType.Printout) {
            param = "Printer";
        } else {
            param = "TerminalTransaction";
        }
        param += "=" + packet;

        if (sendPacketTask != null) {
            cancel();
        }

        if (sendPacketTask != null) {
            sendPacketTask.cancel(true);
        }

        sendPacketTask = new BluetoothSendPacketTask(logger, macAddress, bluetoothService, callback, timeoutSeconds);
        sendPacketTask.execute(param);
    }

    @Override
    public void setLogger(IDejaPayLogger logger) {
        this.logger = logger;

        bluetoothService.setLogger(logger);
    }

    @Override
    public void cancel() {
        if (sendPacketTask != null && sendPacketTask.getStatus() == AsyncTask.Status.RUNNING) {
            sendPacketTask.cancel();
        }
    }

    @Override
    public void stop() {
        bluetoothService.stop();
        if (sendPacketTask != null) {
            sendPacketTask.cancel(true);
        }
    }

    @Override
    public void getState(String tpn, ICallback<State> callback) {
        callback.call(state());
    }

    private State state() {
        switch (bluetoothService.getStatus(macAddress)) {
            case None: return State.Disconnected;
            case Connected: return State.Connected;
            case Connecting: return State.Connecting;
            default: return State.None;
        }
    }

    @Override
    public boolean connect() {
        if (bluetoothService.getStatus(macAddress) == IBluetoothService.BluetoothStatus.Connected) {
            return true;
        }
        return bluetoothService.connectToDevice(macAddress, mBluetoothListener);
    }

    private final IBluetoothService.IBluetoothListener mBluetoothListener = new IBluetoothService.IBluetoothListener() {
        @Override
        public void onReadData(String macAddress, byte[] bytes) {

        }

        @Override
        public void onUpdatedStatus(String macAddress, IBluetoothService.BluetoothStatus status) {

        }
    };

    @Override
    public String getAuthKey() {
        return authKey;
    }

    @Override
    public String getRegisterId() {
        return registerId;
    }

    @Override
    public String getTpn() {
        return (tpn != null) ? tpn : "";
    }
}
