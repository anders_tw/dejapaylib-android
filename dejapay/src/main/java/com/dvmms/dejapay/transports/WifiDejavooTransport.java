package com.dvmms.dejapay.transports;

import com.dvmms.dejapay.ICallback;
import com.dvmms.dejapay.IDejaPayLogger;
import com.dvmms.dejapay.exception.DejavooInvalidResponseException;
import com.dvmms.dejapay.exception.DejavooRequestCancelledException;
import com.dvmms.dejapay.exception.DejavooRequestFailedException;
import com.dvmms.dejapay.exception.DejavooRequestTimeoutExpiredException;
import com.dvmms.dejapay.exception.DejavooThrowable;
import com.dvmms.dejapay.exception.DejavooTransportNotConnectedException;

import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Locale;
import java.util.concurrent.CancellationException;

/**
 * WiFi transport to DejaPay terminal
 */
public class WifiDejavooTransport extends AbstractHttpTransport implements IDejavooTransport {
    private final String tpn;
    private WebRequest<String> activeRequest;

    public WifiDejavooTransport(String tpn, String scheme, String host, Integer port) {
        this(tpn, scheme, host, port, 800);
    }

    public WifiDejavooTransport(String tpn, String scheme, String host, Integer port, int timeoutSeconds) {
        super(
                getPath(scheme, host, port),
                new WifiWebPaymentApiClient(getPath(scheme, host, port), timeoutSeconds, timeoutSeconds)
        );
        this.tpn = tpn;
    }

    @Override
    public void sendPacket(SPinType sPinType, String packet, final ITransportCallback callback) {
        if (sPinType == SPinType.Transaction) {
            activeRequest = api.sendTransaction(packet.replace("\n", ""));
        } else {
            activeRequest = api.printout(packet.replace("\n", ""));
        }

        activeRequest.enqueue(new IWebCallback<WebResponse<String>>() {
            @Override
            public void success(WebResponse<String> response) {
                activeRequest = null;
                if (response.getStatus() == 200) {
                    if (response.getData() != null && !response.getData().isEmpty()) {
                        callback.onResponse(response.getData());
                    } else {
                        callback.onError(new DejavooInvalidResponseException());
                    }
                } else {
                    callback.onError(new DejavooRequestFailedException());
                }
            }

            @Override
            public void failure(Throwable t) {
                activeRequest = null;

                if (t instanceof SocketException) {
                    callback.onError(new DejavooRequestTimeoutExpiredException());
                } else if (t instanceof UnknownHostException) {
                    callback.onError(new DejavooTransportNotConnectedException());
                } else if (t instanceof CancellationException) {
                    callback.onError(new DejavooRequestCancelledException());
                } else {
                    callback.onError(new DejavooThrowable(t));
                }
            }
        });
    }

    @Override
    public void cancel() {
        if (activeRequest != null) {
            activeRequest.cancel();
        }
    }

    @Override
    public void stop() {
        if (activeRequest != null) {
            activeRequest.cancel();
        }
    }

    @Override
    public boolean connect() {
        return true;
    }

    @Override
    public void getState(String tpn, final ICallback<State> callback) {
        apiCheckState.getIndex().enqueue(new IWebCallback<WebResponse<String>>() {
            @Override
            public void success(WebResponse<String> response) {
                if (response.getStatus() == 200) {
                    callback.call(State.Connected);
                } else {
                    callback.call(State.Disconnected);
                }
            }

            @Override
            public void failure(Throwable t) {
                callback.call(State.Disconnected);
            }
        });
    }

    @Override
    public void setLogger(IDejaPayLogger logger) {

    }

    @Override
    public String getAuthKey() {
        return "";
    }

    @Override
    public String getRegisterId() {
        return "1";
    }

    @Override
    public String getTpn() {
        return (tpn != null) ? tpn : "";
    }

    private static String getPath(String scheme, String host, Integer port) {
        return String.format(Locale.getDefault(), "%s://%s:%d", scheme, host, port);
    }
}
