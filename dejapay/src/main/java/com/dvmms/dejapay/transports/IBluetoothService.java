package com.dvmms.dejapay.transports;

import com.dvmms.dejapay.IDejaPayLogger;

/**
 * Created by Platon on 29.06.2016.
 */
interface IBluetoothService {
    enum BluetoothStatus {
        None, Connecting, Connected
    }

    interface IBluetoothListener {
        void onReadData(String macAddress, byte[] bytes);
        void onUpdatedStatus(String macAddress, IBluetoothService.BluetoothStatus status);
    }

    boolean connectToDevice(String macAddressDevice, IBluetoothListener listener);
    BluetoothStatus getStatus(String address);
    void stop();
    void write(String macAddressDevice, byte[] packet) throws InterruptedException;
    void removeDevice(String macAddressDevice);
    boolean isEnabled();
    boolean isSupported();

    void setLogger(IDejaPayLogger logger);
}
