package com.dvmms.dejapay.exception;

/**
 * DejaPay request was canceled
 */
public class DejavooRequestCancelledException extends DejavooThrowable {
}
