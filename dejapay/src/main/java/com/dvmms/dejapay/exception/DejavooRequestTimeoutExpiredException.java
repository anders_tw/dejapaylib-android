package com.dvmms.dejapay.exception;

/**
 * Sending request timeout was expired
 */
public class DejavooRequestTimeoutExpiredException extends DejavooThrowable {
}
