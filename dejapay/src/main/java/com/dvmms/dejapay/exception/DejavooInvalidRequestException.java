package com.dvmms.dejapay.exception;

import java.util.List;

/**
 * Invalid request to DejaPay terminal
 */
public class DejavooInvalidRequestException extends DejavooThrowable {
    private final List<String> errors;

    public DejavooInvalidRequestException(List<String> errors) {
        this.errors = errors;
    }

    public List<String> getErrors() {
        return errors;
    }

    @Override
    public String toString() {
        return "DejavooInvalidRequestException{" +
                "errors=" + errors +
                '}';
    }
}
