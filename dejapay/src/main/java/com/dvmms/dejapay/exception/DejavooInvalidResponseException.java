package com.dvmms.dejapay.exception;

/**
 * Invalid response for DejaPay terminal
 */
public class DejavooInvalidResponseException extends DejavooThrowable {
}
