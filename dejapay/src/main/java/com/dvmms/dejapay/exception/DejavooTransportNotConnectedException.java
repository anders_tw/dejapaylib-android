package com.dvmms.dejapay.exception;

/**
 * Not connected with DejaPay terminal
 */
public class DejavooTransportNotConnectedException extends DejavooThrowable {
}
