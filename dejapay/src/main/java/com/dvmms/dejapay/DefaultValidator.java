package com.dvmms.dejapay;

/**
 * Created by Platon on 30.06.2016.
 */
class DefaultValidator<T> implements IValidator<T> {
    @Override
    public boolean accept(ValidatorContext context, T field) {
        return true;
    }

    @Override
    public boolean validate(ValidatorContext context, T field) {
        return true;
    }
}
