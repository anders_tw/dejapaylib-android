package com.dvmms.dejapay;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Platon on 26.07.2016.
 */
public class XmlElement {
    String name;
    String text;
    List<XmlElement> childs;
    Map<String,String> attributes;

    public XmlElement(String name) { this(name, ""); }
    public XmlElement(String name, Integer value) {
        this(name, (value != null) ? String.valueOf(value) : "");
    }

    public XmlElement(String name, String text) {
        this.name = name;
        this.text = text;
        this.childs = new ArrayList<>();
        this.attributes = new HashMap<>();
    }



    public void setText(String text) {
        this.text = text;
    }

    public void addChild(XmlElement element) {
        this.childs.add(element);
    }

    public void addAttribute(String name, String value) {
        attributes.put(name, value);
    }

    public String toXml() {
        String xml = "<"+name;

        for (Map.Entry<String, String> entry : attributes.entrySet()) {
            xml += " " + entry.getKey() + "=\""+entry.getValue()+"\"";
        }

        if (!StringUtils.isEmptyOrNull(text)) {
            xml += ">";
            xml += text;
            xml += "</" + name + ">";
        } else {
            if (childs.size() > 0) {
                xml += ">";
                for (XmlElement child : childs) {
                    xml += child.toXml();
                }
                xml += "</" + name + ">";
            } else {
                xml += "/>";
            }
        }

        return xml;
    }
}
