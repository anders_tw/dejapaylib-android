package com.dvmms.dejapay;

import com.dvmms.dejapay.models.DejavooPaymentType;
import com.dvmms.dejapay.models.DejavooTransactionCardData;
import com.dvmms.dejapay.models.DejavooTransactionRequest;
import com.dvmms.dejapay.models.DejavooTransactionType;

/**
 * Created by Platon on 30.06.2016.
 */
class TransactionRequestValidator
        extends DefaultValidator<DejavooTransactionRequest>
        implements IValidator<DejavooTransactionRequest>
{
    @Override
    public boolean validate(ValidatorContext context, final DejavooTransactionRequest request) {
        if (request == null) {
            context.addError("Request can't be null");
            return false;
        }

        return DejavooValidator.validator(context)
                .on(request.getPaymentType(), new RequiredValidator("paymentType"))
                .on(request.getTransactionType(), new TransactionTypeValidator(request.getPaymentType()))
                .on(request.getAmount(), new FloatValidator("Amount"){
                    @Override
                    public boolean accept(ValidatorContext context, Float field) {
                        return (request.getPaymentType() != DejavooPaymentType.Batch
                                && request.getTransactionType() != DejavooTransactionType.Void
                                && request.getTransactionType() != DejavooTransactionType.Status
                        );
                    }
                })
                .on(request.getReferenceId(), new RequiredValidator("RefId"))
                .on(request.getParameter(), new RequiredValidator("Param"){
                    @Override
                    public boolean accept(ValidatorContext context, Object field) {
                        return (request.getPaymentType() == DejavooPaymentType.Batch);
                    }
                })
//                .on(request.getCardData(), new TransactionCardDataValidator(){
//                    @Override
//                    public boolean accept(ValidatorContext context, DejavooTransactionCardData field) {
//                        return field != null;
//                    }
//                })
                .doValidate()
                .isValid();
    }
}
