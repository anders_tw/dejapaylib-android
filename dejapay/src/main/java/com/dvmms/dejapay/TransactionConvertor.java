package com.dvmms.dejapay;

import com.dvmms.dejapay.models.DejavooTransactionCardData;
import com.dvmms.dejapay.models.DejavooPaymentType;
import com.dvmms.dejapay.models.DejavooTransactionRequest;
import com.dvmms.dejapay.models.DejavooTransactionResponse;
import com.dvmms.dejapay.models.DejavooTransactionType;
import com.dvmms.dejapay.models.IDejavooPrintoutItem;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by Platon on 29.06.2016.
 */
public class TransactionConvertor
        implements IDejavooSerializer<DejavooTransactionRequest>,
                   IDejavooDeserializer<DejavooTransactionResponse>
{
    private final Base64Coder base64Coder;
    private String mApp = "";

    public TransactionConvertor() {
        base64Coder = new Base64Coder();
    }

    @Override
    public DejavooTransactionResponse deserialize(String data) {
        DejavooTransactionResponse response = new DejavooTransactionResponse();

        XmlPullParserFactory xmlFactoryObject = null;
        Map<String, String> tagsText = new HashMap<>();

        String endTag = "</xmp>";
        int endIndex = data.lastIndexOf(endTag);
        if (endIndex != -1)
        {
            data = data.substring(0, endIndex+endTag.length());
        }

        data = data.replaceAll(">\\s*<", "><");

        try {
            String valueText = "";
            xmlFactoryObject = XmlPullParserFactory.newInstance();
            XmlPullParser parser = xmlFactoryObject.newPullParser();
            InputStream is = new ByteArrayInputStream(data.getBytes());
            parser.setInput(is, null);

            response.setPacketRaw(data);

            int event = parser.getEventType();
            String startTag = "";
            while (event != XmlPullParser.END_DOCUMENT) {
                String name = parser.getName();
                switch (event) {
                    case XmlPullParser.START_TAG:
                        startTag = name;
                        break;
                    case XmlPullParser.TEXT:
                        valueText = parser.getText();
                        tagsText.put(startTag, valueText);
                        break;
                    case XmlPullParser.END_TAG: {
                        if (name.equals("ResultCode")) {
                            if (valueText.equalsIgnoreCase("0")) {
                                response.setResultCode(DejavooTransactionResponse.ResultCode.Succeded);
                            } else {
                                response.setResultCode(DejavooTransactionResponse.ResultCode.Failed);
                            }
                        } else if (name.equals("Message")) {
                            response.setMessage(valueText);
                        } else if (name.equals("InvNum")) {
                            response.setInvoiceNumber(valueText);
                        } else if (name.equals("ExtData")) {
                            mApp = setResponseExtData(response, valueText);
                        } else if (name.equals("TransReport")) {
                            setTransReport(mApp, response, valueText);
                        } else if (name.equals("Receipt")) {
                            setResponseReceipts(response, valueText);
                        } else if (name.equals("PNRef")) {
                            response.setPnReference(valueText);
                        } else if (name.equals("AuthCode")) {
                            response.setAuthenticationCode(valueText);
                        } else if (name.equals("RefId")) {
                            response.setReferenceId(valueText);
                        } else if (name.equals("RegisterId")) {
                            response.setRegisterId(valueText);
                        } else if (name.equals("RespMSG")) {
                            response.setResponseMessage(URLDecoder.decode(valueText, "UTF-8"));
                        } else if (name.equals("hostname")) {
                            response.setTetheringHost(valueText);
                        } else if (name.equals("port")) {
                            response.setTetheringPort(Integer.parseInt(valueText));
                        } else if (name.equals("security")) {
                            response.setTetheringSecurity(valueText);
                        } else if (name.equals("protocol")) {
                            response.setTetheringProtocol(valueText);
                        } else if (name.equals("SignatureTimeout")) {
                            response.setSignatureTimeout(toDouble(valueText, 120.0));
                        } else if (name.equals("SignatureRequired")) {
                            response.setSignatureRequired(toBoolean(valueText, false));
                        } else if (name.equals("EMVData")) {
                            response.setEmvData(valueText);
                        } else if (name.equals("Sign")) {
                            response.setSign( base64Coder.decode(valueText) );
                        } else if (name.equals("msg")) {
                            response.setTetheringMessage(base64Coder.decode(valueText));
                        } else if (name.equals("error")) {
                            if (tagsText.containsKey(name)) {
                                response.setError(URLDecoder.decode(tagsText.get(name), "UTF-8"));
                            }
                        } else if (name.equals("code")) {
                            if (tagsText.containsKey(name)) {
                                response.setErrorCode(tagsText.get(name));
                            }
                        } else if (name.equals("Voided")) {
                            if (valueText.equalsIgnoreCase("true")) {
                                response.setVoided(true);
                            } else {
                                response.setVoided(false);
                            }
                        } else if (name.equals("PaymentType")) {
                            for (DejavooPaymentType paymentType : DejavooPaymentType.values()) {
                                if (paymentType.name().equalsIgnoreCase(valueText)) {
                                    response.setPaymentType(paymentType);
                                    break;
                                }
                            }
                        } else if (name.equals("TransType")) {
                            if (StringUtils.containsIgnoreCase(valueText, "Voucher")) {
                                response.setVoucher(true);
                            }

                            if (valueText.equalsIgnoreCase("Purchase Correction") || valueText.startsWith("Void")) {
                                response.setTransactionType(DejavooTransactionType.Void);
                            } else {
                                for (DejavooTransactionType transactionType : DejavooTransactionType.values()) {
                                    if (transactionType.name().equalsIgnoreCase(valueText)) {
                                        response.setTransactionType(transactionType);
                                        break;
                                    }
                                }
                            }


                        }
                        break;
                    }
                }
                event = parser.next();
            }

            return response;
        } catch (XmlPullParserException ex) {
            return null;
        } catch (IOException ex) {
            return null;
        }
    }

    private void setTransReport(String app, DejavooTransactionResponse response, String valueText) {
        try {
            Map<String,String> data = new LinkedHashMap<>();

            List<String> values = new ArrayList<>(Arrays.asList(valueText.split(",")));
            for (String value : values) {
                String[] items = value.split("=");

                if (items.length > 0 && items[0] != null) {
                    String title = items[0].trim();
                    if (items.length == 2) {
                        if (items[1] != null) {
                            data.put(title, items[1].trim());
                        }
                    } else {
                        data.put(title, "");
                    }
                }
            }

            response.setTransReport(app, data);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public String serialize(DejavooTransactionRequest field) {
        String packet = "";

        if (field.getPaymentType() != null) {
            packet += "<PaymentType>" + field.getPaymentType().name() + "</PaymentType>\n";
        }
        if (field.getTransactionType() != null) {
            switch (field.getTransactionType()) {
                case EBTVoucher: {
                    packet += "<TransType>Sale</TransType>\n";
                    packet += "<EBT_Voucher>Yes</EBT_Voucher>\n";
                    break;
                }
                case EBTVoucherReturn: {
                    packet += "<TransType>Return</TransType>\n";
                    packet += "<EBT_Voucher>Yes</EBT_Voucher>\n";
                    break;
                }
                case EBTVoucherVoid: {
                    packet += "<TransType>Void</TransType>\n";
                    packet += "<EBT_Voucher>Yes</EBT_Voucher>\n";
                    break;
                }

                default: {
                    packet += "<TransType>" + field.getTransactionType().name() + "</TransType>\n";
                    break;
                }
            }
        }

        packet += xmlItem(field.getAuthenticationKey(), "AuthKey");
        packet += xmlItem(field.getAmount(), "Amount");
        packet += xmlItem(field.getTip(), "Tip");
        packet += xmlItem(field.getPoints(), "Points");
        packet += xmlItem(field.getInvoiceNumber(), "InvNum");
        packet += xmlItem(field.getReferenceId(), "RefId");
        packet += xmlItem(field.getAuthenticationCode(), "AuthCode");
        packet += xmlItem(field.getClerkId(), "ClerkId");
        packet += xmlItem(field.getTableNumber(), "TableNum");
        packet += xmlItem(field.getTickerNumber(), "TicketNum");
        packet += xmlItem(field.getAcntLast4(), "AcntLast4");
        packet += xmlItem(field.getRegisterId(), "RegisterId");
        packet += xmlItem(field.getParameter(), "Param");
        packet += xmlItem(field.getToken(), "Token");
        packet += xmlItem(field.getTpn(), "TPN");
        packet += xmlItem(field.getTransNum(), "TransNum");

        if (field.getExtReceiptData() != null && field.getExtReceiptData().size() > 0) {
            String printerXml = ""; // "\n<ItemList>\n";
            for (IDejavooPrintoutItem item : field.getExtReceiptData()) {
                printerXml += item.data() + "\n";
            }
            printerXml = xmlItem(printerXml, "ItemList");

            packet += xmlItem(printerXml, "ExtReceiptData");
        }

        if (field.getPrintout() != null && field.getPrintout().size() > 0) {
            String printerXml = "";
            for (IDejavooPrintoutItem item : field.getPrintout()) {
                printerXml += item.data() + "\n";
            }

            packet += xmlItem(printerXml, "Printer");
        }

        if (field.getFrequency() != null) {
            packet += xmlItem(field.getFrequency().name(), "Frequency");
        }

        if (field.getReceiptType() != null) {
            packet += "<GetReceipt>" + field.getReceiptType().name() + "</GetReceipt>\n";
        }

        if (field.getPrintReceipt() != null) {
            packet += "<PrintReceipt>" + field.getPrintReceipt().name() + "</PrintReceipt>\n";
        }

        packet += "<SignCapable>" + (field.isSignatureCapable() ? "true" : "false")  + "</SignCapable>\n";

        if (field.isBypassSignature()) {
            packet += "<SigCapture>No</SigCapture>\n";
        }



        if (field.getCardData() != null) {
            DejavooTransactionCardData cardData = field.getCardData();
            Map<String,String> cardDataMap = new HashMap<>();

            cardDataMap.put("EntType", cardData.getEntType().getValue());
            if (cardData.getEntType() == DejavooTransactionCardData.Type.Manual) {
                if (cardData.getPAN() != null) {
                    cardDataMap.put("PAN", cardData.getPAN());
                }
                if (cardData.getExpiryDate() != null) {
                    cardDataMap.put("ExpDate", String.valueOf(cardData.getExpiryDate()));
                }
                if (cardData.getAVS() != null) {
                    cardDataMap.put("AVS", escapeNull(cardData.getAVS()));
                }

                if (cardData.getZIP() != null) {
                    cardDataMap.put("ZIP", escapeNull(cardData.getZIP()));
                }

                if (cardData.getCVV() != null) {
                    cardDataMap.put("CVV", escapeNull(cardData.getCVV()));
                }

                if (cardData.getCardPresent() != null) {
                    if (cardData.getCardPresent()) {
                        cardDataMap.put("CardPresent", "1");
                    } else {
                        cardDataMap.put("CardPresent", "0");
                    }
                }

                if (!StringUtils.isEmptyOrNull(cardData.getTrack1())) {
                    cardDataMap.put("Track1", escapeNull(cardData.getTrack1()));
                }

                if (!StringUtils.isEmptyOrNull(cardData.getTrack2())) {
                    cardDataMap.put("Track2", escapeNull(cardData.getTrack2()));
                }

            } else {
                cardDataMap.put("Track1", escapeNull(cardData.getTrack1()));
                cardDataMap.put("Track2", escapeNull(cardData.getTrack2()));
            }

            String cardDataText = "";
            for (Map.Entry<String, String> item : cardDataMap.entrySet()) {
                cardDataText += item.getKey() + "=" + item.getValue() + ",";
            }
            cardDataText = cardDataText.substring(0, cardDataText.length() - 1);

            packet += "<CardData>" + cardDataText + "</CardData>\n";
        }

        if (field.getSignature() != null) {
            String sign = base64Coder.encode(field.getSignature());
            if (!isEmptyOrNull(sign)) {
                packet += "<Sign>" + sign + "</Sign>\n";
            }
        }

        if (field.getTetheringMessage() != null) {
            String tetheringMessage = base64Coder.encode(field.getTetheringMessage()).replace("\n","");
            if (!isEmptyOrNull(tetheringMessage)) {
                packet += "<msg>" + tetheringMessage + "</msg>\n";
            }
        }


        if (field.getCustomFields() != null) {
            int i = 1;
            for (Map.Entry<String, String> customField : field.getCustomFields().entrySet()) {
                packet += xmlItem(customField.getKey(), String.format("Cust%d", i));
                packet += xmlItem(customField.getValue(), String.format("Cust%dValue", i));
                i++;
            }
        }

        if (field.getCustomOptions() != null) {
            for (Map.Entry<String, String> option : field.getCustomOptions().entrySet()) {
                packet += xmlItem(option.getValue(), option.getKey());
            }
        }

        packet = "<request>" + packet + "</request>";

        return packet;
    }

    <T> String xmlItem(T value, String tag) {
        if (value == null) {
            return "";
        }

        if (value instanceof String) {
            String text = (String)value;
            if (!isEmptyOrNull(text)) {
                return "<"+tag+">" + text + "</"+tag+">\n";
            } else {
                return "";
            }
        }

        if (value instanceof Float) {
            DecimalFormat decimalFormat = new DecimalFormat("#.##");
            decimalFormat.setMaximumFractionDigits(2);
            decimalFormat.setMinimumFractionDigits(2);
            String decimal = decimalFormat.format(value).replace(",", ".");
            return "<"+tag+">" + decimal + "</"+tag+">\n";
        }

        return "<"+tag+">" + String.valueOf(value) + "</"+tag+">\n";
    }

    private String setResponseExtData(DejavooTransactionResponse response, String extDataText) {
        try {
            Map<String,String> data = new LinkedHashMap<>();

            List<String> values = new ArrayList<>(Arrays.asList(extDataText.split(",")));
            for (String value : values) {
                String[] items = value.split("=");
                if (items.length > 0 && items[0] != null) {
                    String title = items[0].trim();
                    if (items.length == 2) {
                        if (items[1] != null) {
                            data.put(title, items[1].trim());
                        }
                    } else {
                        data.put(title, "");
                    }
                }
            }


            if (data.containsKey("App")) {
                response.setExtData(data.get("App"), data);
                return data.get("App");
            } else {
                response.setExtData("", data);
                return "";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "";
    }

    private void setResponseReceipts(DejavooTransactionResponse response, String receiptsText) {
        Map<Integer, String> mapPositions = new TreeMap<>(new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return o2.compareTo(o1);
            }
        });

        Map<DejavooTransactionResponse.ReceiptType, String> receipts = new HashMap<>();

        int pos = -1;
        pos = receiptsText.indexOf("Customer=");
        if (pos != -1) {
            mapPositions.put(pos, "Customer=");
        }

        pos = receiptsText.indexOf("Merchant=");
        if (pos != -1) {
            mapPositions.put(pos, "Merchant=");
        }

        for (Integer posReceipt : mapPositions.keySet()) {
            String receipt = receiptsText.substring(posReceipt);
            String name = mapPositions.get(posReceipt);
            receiptsText = receiptsText.replace(receipt, "");

            if (name.equals("Merchant=")) {
                try {
                    receipts.put(DejavooTransactionResponse.ReceiptType.Merchant, URLDecoder.decode(receipt.replace(name, ""), "UTF-8"));
                } catch (UnsupportedEncodingException ex) {
//                    this.merchantReceipt = "";
                }
            } else if (name.equals("Customer=")) {
                try {
                    receipts.put(DejavooTransactionResponse.ReceiptType.Customer, URLDecoder.decode(receipt.replace(name, ""), "UTF-8"));
                } catch (UnsupportedEncodingException ignored) {
                }
            }
        }

        response.setRawReceipts(new HashMap<>(receipts));
    }


    static boolean isEmptyOrNull(String text) {
        return (text == null || text.isEmpty());
    }

    static String escapeNull(String text) {
        if (text == null) {
            return "";
        } else {
            return text;
        }
    }

    static Boolean toBoolean(String text, boolean defaultValue) {
        if (text == null) {
            return defaultValue;
        }

        return text.equalsIgnoreCase("true");

    }

    static Double toDouble(String text, double defaultValue) {
        try {
            return Double.parseDouble(text);
        } catch (NumberFormatException ex) {
            ex.printStackTrace();
        }
        return defaultValue;
    }

    private static class TagElement {
        private String name;
        private Map<String,String> attributes;
        private String value;

        public TagElement(String name, Map<String, String> attributes) {
            this.name = name;
            this.attributes = attributes;
        }

        public String getName() {
            return name;
        }

        public Map<String, String> getAttributes() {
            return attributes;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public String getAttribute(String name, String defaultValue) {
            if (attributes.containsKey(name)) {
                return attributes.get(name);
            }
            return defaultValue;
        }
    }

}
