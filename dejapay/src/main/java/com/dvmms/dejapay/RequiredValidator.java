package com.dvmms.dejapay;

/**
 * Created by Platon on 30.06.2016.
 */
class RequiredValidator
    extends DefaultValidator<Object>
    implements IValidator<Object>
{
    private final String fieldName;

    public RequiredValidator(String fieldName) {
        this.fieldName = fieldName;
    }

    @Override
    public boolean validate(ValidatorContext context, Object field) {
        if (field == null) {
            context.addError("'%s' required", fieldName);
            return false;
        }

        return true;
    }
}
