package com.dvmms.dejapay;

/**
 * Created by Platon on 26.07.2016.
 */
public interface ICallback<T> {
    void call(T model);
}
