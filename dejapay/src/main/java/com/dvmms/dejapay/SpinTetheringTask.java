package com.dvmms.dejapay;

import android.os.AsyncTask;

import com.dvmms.dejapay.exception.DejavooThrowable;
import com.dvmms.dejapay.exception.DejavooTetheringFailedException;
import com.dvmms.dejapay.models.DejavooTransactionResponse;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.ByteBuffer;

import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

/**
 * Created by Platon on 05.07.2016.
 */
// @deprecated Changed to Tethering Thread
class SpinTetheringTask extends AsyncTask<byte[], Void, byte[]> {
    private final Socket socket;
    private final ISpinTetheringListener listener;
    private final DejavooTransactionResponse.TetheringProtocol tetheringProtocol;

    private static final int CONNECT_TIMEOUT_MS = 30000;
    private static final int ATTEMPT_TIMEOUT_MS = 2000;
    private static final int COUNT_ATTEMPTS = 3;
    private final IDejaPayLogger logger;

    private static final byte VISAI_ENQ = 0x05;
    private static final byte VISAI_EOT = 0x04;
    private static final byte VISAI_ACK = 0x06;



    public SpinTetheringTask(IDejaPayLogger logger, Socket socket, DejavooTransactionResponse.TetheringProtocol tetheringProtocol,  ISpinTetheringListener listener) {
        this.logger = logger;
        this.socket = socket;
        this.tetheringProtocol = tetheringProtocol;
        this.listener = listener;
    }



    @Override
    protected byte[] doInBackground(byte[]... params) {
        byte[] message = params[0];

        try {
            for (int attempt=1; attempt <= COUNT_ATTEMPTS; attempt++) {
                logger.d("SPin tethering attempt: %d. Message: %s",attempt, dbgBytes(message));
                byte[] res;

                if (tetheringProtocol == DejavooTransactionResponse.TetheringProtocol.VisaI) {
                    res = getBytesVisaI(message);
                } else  {
                    res = getBytesDefault(message);
                }

                if (res != null) {
                    logger.d("SPin response message: %s", dbgBytes(res));
                    return res;
                } else {
                    logger.d("SPin response failed. Next attempt");
                }
                Thread.sleep(ATTEMPT_TIMEOUT_MS);
            }
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }

        return null;
    }


    String dbgBytes(byte[] bytes) {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for (int i=0; i < bytes.length; i++) {
            sb.append(String.format("0x%02x,", bytes[i]));
        }
        sb.append("]");

        return sb.toString();
    }

    private byte awaitCommandByte(InputStream inputStream) {
        try {
            byte[] buffer;
            int bytesRead;

            while (true) {
                buffer = new byte[1];
                bytesRead = inputStream.read(buffer);

                logger.d("Await command, bytes read: %d; Buffer Length: %d; Buffer: %s", bytesRead, buffer.length, dbgBytes(buffer));

                if (buffer.length == 1 && (buffer[0] == VISAI_EOT || buffer[0] == VISAI_ENQ || buffer[0] == VISAI_ACK)) {
                    break;
                }

                Thread.sleep(500);
            }

            logger.d("Received command: 0x%02x", buffer[0]);

            return buffer[0];
        } catch (IOException e) {
            logger.e(e, "await failed io tethering command");
        } catch (InterruptedException e) {
            logger.e(e, "await failed interrupted tethering command");
        }

        return 0;
    }

    private byte[] getBytesVisaI(byte[] message) {
        try {
            logger.d("Using VISA I protocol");
            ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream(1024);

            OutputStream outputStream = socket.getOutputStream();
            InputStream inputStream = socket.getInputStream();

            byte[] buffer = new byte[1024];


            byte cmdByte = awaitCommandByte(inputStream);
            if (cmdByte == VISAI_ENQ) {
                logger.d("Received VISAI ENQ");
            } else if (cmdByte == VISAI_EOT) {
                logger.d("Received VISAI EOT");
            } else if (cmdByte == VISAI_ACK) {
                logger.d("Received VISAI ACK");
            } else {
                logger.d("Received unknown command byte");
            }


            outputStream.write(message);

            cmdByte = awaitCommandByte(inputStream);

            if (cmdByte == VISAI_ENQ) {
                logger.d("Received VISAI ENQ");
            } else if (cmdByte == VISAI_EOT) {
                logger.d("Received VISAI EOT");
            } else if (cmdByte == VISAI_ACK) {
                logger.d("Received VISAI ACK");
            } else {
                logger.d("Received unknown command byte");
            }

            int bytesRead;
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                byteBuffer.write(buffer, 0, bytesRead);
                Thread.sleep(500);
            }


            ByteBuffer bbuf = ByteBuffer.allocate(/*1 +*/ byteBuffer.size());
//            bbuf.put(VISAI_ACK);
            bbuf.put(byteBuffer.toByteArray());

            logger.d("Received %d bytes; BBuf: %d", byteBuffer.size(), bbuf.array().length);


//            outputStream.write(new byte[]{ VISAI_ACK });
//            cmdByte = awaitCommandByte(inputStream);
//
//            if (cmdByte == VISAI_ENQ) {
//                logger.d("Received VISAI ENQ");
//            } else if (cmdByte == VISAI_EOT) {
//                logger.d("Received VISAI EOT");
//            } else if (cmdByte == VISAI_ACK) {
//                logger.d("Received VISAI ACK");
//            } else {
//                logger.d("Received unknown command byte");
//            }

            if (byteBuffer.size() == 0) {
                return new byte[] { cmdByte };
            }

            return bbuf.array();
        }  catch (IOException e) {
            logger.e(e, "IO failed with tethering host");
        }  catch (InterruptedException e) {
            logger.e(e, "await failed interrupted tethering command");
        } finally {

        }
        return null;
    }


    private byte[] getBytesDefault(byte[] message) {
        try {
            ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream(1024);

            OutputStream outputStream = socket.getOutputStream();
            InputStream inputStream = socket.getInputStream();
            outputStream.write(message);

            byte[] buffer = new byte[1024];
            int bytesRead;

            while ((bytesRead = inputStream.read(buffer)) != -1) {
                byteBuffer.write(buffer, 0, bytesRead);
            }

            return byteBuffer.toByteArray();
        }  catch (IOException e) {
            logger.e(e, "IO failed with tethering host");
        } finally {

        }
        return null;
    }

    @Override
    protected void onPostExecute(byte[] bytes) {
        if (bytes != null) {
            listener.onResponse(bytes);
        } else {
            listener.onError(new DejavooTetheringFailedException());
        }
    }


    public static Socket getTetheringSocket(String host, int port, String security) throws IOException {
        SSLSocket socket  = (SSLSocket) SSLSocketFactory.getDefault().createSocket();
        socket.connect(new InetSocketAddress(host, port), CONNECT_TIMEOUT_MS);
        socket.startHandshake();
        return socket;
    }

    interface ISpinTetheringListener {
        void onResponse(byte[] bytes);
        void onError(DejavooThrowable throwable);
    }
}