package com.dvmms.dejapay;

/**
 * Logger interface
 */
public interface IDejaPayLogger {
    void v(String message, Object... args);
    void v(Throwable t, String message, Object... args);
    void d(String message, Object... args);
    void d(Throwable t, String message, Object... args);
    void w(String message, Object... args);
    void w(Throwable t, String message, Object... args);
    void e(String message, Object... args);
    void e(Throwable t, String message, Object... args);
}
