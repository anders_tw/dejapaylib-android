package com.dvmms.dejapay;

/**
 * Created by Platon on 29.06.2016.
 */
interface IValidator<T> {
    boolean accept(ValidatorContext context, T field);
    boolean validate(ValidatorContext context, T field);
}
