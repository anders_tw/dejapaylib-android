package com.dvmms.dejapay;

import com.dvmms.dejapay.models.DejavooUserInputRequest;
import com.dvmms.dejapay.models.DejavooUserInputResponse;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Platon on 26.07.2016.
 */
public class UserInputConvertor
    implements IDejavooSerializer<DejavooUserInputRequest>,
        IDejavooDeserializer<DejavooUserInputResponse>
{
    @Override
    public DejavooUserInputResponse deserialize(String data) {
        DejavooUserInputResponse response = new DejavooUserInputResponse();

        XmlPullParserFactory xmlFactoryObject = null;
        Map<String, String> tagsText = new HashMap<>();
        data = data.replaceAll(">\\s*<", "><");

        try {
            String valueText = "";
            xmlFactoryObject = XmlPullParserFactory.newInstance();
            XmlPullParser parser = xmlFactoryObject.newPullParser();
            InputStream is = new ByteArrayInputStream(data.getBytes());
            parser.setInput(is, null);

            int event = parser.getEventType();
            String startTag = "";
            while (event != XmlPullParser.END_DOCUMENT) {
                String name = parser.getName();
                switch (event) {
                    case XmlPullParser.START_TAG:
                        startTag = name;
                        break;
                    case XmlPullParser.TEXT:
                        valueText = parser.getText();
                        tagsText.put(startTag, valueText);
                        break;
                    case XmlPullParser.END_TAG: {
                        if (name.equalsIgnoreCase("RegisterId")) {
                            response.setRegisterId(valueText);
                        } else if (name.equalsIgnoreCase("UserInput")) {
                            response.setUserInput(valueText);
                        } else if (name.equalsIgnoreCase("Message")) {
                            response.setMessage(valueText);
                        }
                        break;
                    }
                }
                event = parser.next();
            }

            return response;
        } catch (XmlPullParserException ex) {
            return null;
        } catch (IOException ex) {
            return null;
        }
    }

    @Override
    public String serialize(DejavooUserInputRequest request) {
        XmlElement elRequest = new XmlElement("request");

        elRequest.addChild(new XmlElement("RegisterId", StringUtils.escapeNull(request.getRegisterId())));
        if (!StringUtils.isEmptyOrNull(request.getAuthKey())) {
            elRequest.addChild(new XmlElement("AuthKey", request.getAuthKey()));
        }

        XmlElement elUserInput = new XmlElement("UserInput");
        if (!StringUtils.isEmptyOrNull(request.getTitle())) {
            elUserInput.addAttribute("title", request.getTitle());
        }
        if (request.getTimeout() != null) {
            elUserInput.addAttribute("timeout", String.valueOf(request.getTimeout()));
        }

        if (request.getInputLength() != null) {
            elUserInput.addAttribute("maxlen", String.valueOf(request.getInputLength()));
        }

        if (request.getType() != null) {
            elUserInput.addAttribute("type", request.getType().getValue());
        }

        if (!StringUtils.isEmptyOrNull(request.getMask())) {
            elUserInput.addAttribute("mask", request.getMask());
        }

        if (request.getHide() != null) {
            elUserInput.addAttribute("hide", (request.getHide()) ? "yes" : "no");
        }

        if (!StringUtils.isEmptyOrNull(request.getDefaultValue())) {
            elUserInput.addAttribute("default", request.getDefaultValue());
        }

        elRequest.addChild(elUserInput);

        return elRequest.toXml();
    }
}
