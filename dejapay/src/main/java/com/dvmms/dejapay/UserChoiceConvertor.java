package com.dvmms.dejapay;

import com.dvmms.dejapay.models.DejavooUserChoiceRequest;
import com.dvmms.dejapay.models.DejavooUserChoiceResponse;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Platon on 25.07.2016.
 */
public class UserChoiceConvertor
    implements IDejavooSerializer<DejavooUserChoiceRequest>,
        IDejavooDeserializer<DejavooUserChoiceResponse>
{

    @Override
    public DejavooUserChoiceResponse deserialize(String data) {
        DejavooUserChoiceResponse response = new DejavooUserChoiceResponse();

        XmlPullParserFactory xmlFactoryObject = null;
        Map<String, String> tagsText = new HashMap<>();
        data = data.replaceAll(">\\s*<", "><");

        try {
            String valueText = "";
            xmlFactoryObject = XmlPullParserFactory.newInstance();
            XmlPullParser parser = xmlFactoryObject.newPullParser();
            InputStream is = new ByteArrayInputStream(data.getBytes());
            parser.setInput(is, null);

            int event = parser.getEventType();
            String startTag = "";
            while (event != XmlPullParser.END_DOCUMENT) {
                String name = parser.getName();
                switch (event) {
                    case XmlPullParser.START_TAG:
                        startTag = name;
                        break;
                    case XmlPullParser.TEXT:
                        valueText = parser.getText();
                        tagsText.put(startTag, valueText);
                        break;
                    case XmlPullParser.END_TAG: {
                        if (name.equalsIgnoreCase("RegisterId")) {
                            response.setRegisterId(valueText);
                        } else if (name.equalsIgnoreCase("UserChoice")) {
                            response.setUserChoice(valueText);
                        } else if (name.equalsIgnoreCase("Message")) {
                            response.setMessage(valueText);
                        }
                        break;
                    }
                }
                event = parser.next();
            }

            return response;
        } catch (XmlPullParserException ex) {
            return null;
        } catch (IOException ex) {
            return null;
        }
    }

    @Override
    public String serialize(DejavooUserChoiceRequest request) {
        XmlElement elRequest = new XmlElement("request");

        if (!StringUtils.isEmptyOrNull(request.getTpn())) {
            elRequest.addChild(new XmlElement("TPN", StringUtils.escapeNull(request.getTpn())));
        }

        if (!StringUtils.isEmptyOrNull(request.getAuthKey())) {
            elRequest.addChild(new XmlElement("AuthKey", request.getAuthKey()));
        }

        XmlElement elUserChoice = new XmlElement("UserChoice");
        if (!StringUtils.isEmptyOrNull(request.getTitle())) {
            elUserChoice.addAttribute("title", request.getTitle());
        }
        if (request.getTimeout() != null) {
            elUserChoice.addAttribute("timeout", String.valueOf(request.getTimeout()));
        }
        elUserChoice.addAttribute("count", String.valueOf(request.getChoices().size()));

        for (String choice : request.getChoices()) {
            elUserChoice.addChild(new XmlElement("Choice", choice));
        }

        elRequest.addChild(elUserChoice);

        return elRequest.toXml();
    }
}
