package com.dvmms.dejapay;

/**
 * Created by Platon on 29.06.2016.
 */
interface IDejavooDeserializer<T> {
    T deserialize(String data);
}
