package com.dvmms.dejapay;

import java.util.Date;
import java.util.UUID;

public class DejavooUtils {
    public static String generateRandomRefId() {
        String guid = UUID.randomUUID().toString();
        return guid.substring(0, Math.min(10, guid.length()));
    }

    public static int generateRandomInvoiceId() {
        return (int)(new Date().getTime() % 10000000);
    }
}
