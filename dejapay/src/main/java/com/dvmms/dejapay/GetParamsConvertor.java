package com.dvmms.dejapay;

import com.dvmms.dejapay.models.DejavooGetParamsRequest;
import com.dvmms.dejapay.models.DejavooGetParamsResponse;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GetParamsConvertor
    implements IDejavooSerializer<DejavooGetParamsRequest>,
        IDejavooDeserializer<DejavooGetParamsResponse>
{
    @Override
    public DejavooGetParamsResponse deserialize(String data) {
        DejavooGetParamsResponse response = new DejavooGetParamsResponse();

        XmlPullParserFactory xmlFactoryObject = null;
        Map<String, String> tagsText = new HashMap<>();
        data = data.replaceAll(">\\s*<", "><");

        try {
            String valueText = "";
            xmlFactoryObject = XmlPullParserFactory.newInstance();
            XmlPullParser parser = xmlFactoryObject.newPullParser();
            InputStream is = new ByteArrayInputStream(data.getBytes());
            parser.setInput(is, null);

            List<DejavooGetParamsResponse.Param> parameters = new ArrayList<>();

            boolean getParamStarted = false;
            String paramFile = "";

            int event = parser.getEventType();
            String startTag = "";
            while (event != XmlPullParser.END_DOCUMENT) {
                String name = parser.getName();
                switch (event) {
                    case XmlPullParser.START_TAG:
                        startTag = name;

                        if (startTag.equalsIgnoreCase("GetParam")) {
                            getParamStarted = true;
                            paramFile = parser.getAttributeValue(null, "Param");
                        } else if (getParamStarted) {
                            DejavooGetParamsResponse.Param param = new DejavooGetParamsResponse.Param();
                            param.setFile(paramFile);
                            param.setName(name);
                            param.setPerm(parser.getAttributeValue(null, "perm"));
                            param.setValue(parser.getAttributeValue(null, "value"));

                            parameters.add(param);
                        }

                        break;

                    case XmlPullParser.TEXT:
                        valueText = parser.getText();
                        tagsText.put(startTag, valueText);
                        break;
                    case XmlPullParser.END_TAG: {
                        if (name.equalsIgnoreCase("RegisterId")) {
                            response.setRegisterId(valueText);
                        } else if (name.equalsIgnoreCase("GetParam")) {
                            getParamStarted = false;
                        }
                        break;
                    }
                }
                event = parser.next();
            }

            response.setParameters(parameters);

            return response;
        } catch (XmlPullParserException ex) {
            return null;
        } catch (IOException ex) {
            return null;
        }
    }

    @Override
    public String serialize(DejavooGetParamsRequest request) {
        XmlElement elRequest = new XmlElement("request");
        elRequest.addChild(new XmlElement("RegisterId", StringUtils.escapeNull(request.getRegisterId())));

        if (!StringUtils.isEmptyOrNull(request.getAuthKey())) {
            elRequest.addChild(new XmlElement("AuthKey", request.getAuthKey()));
        }

        XmlElement elUserChoice = new XmlElement("GetParam");
        if (!StringUtils.isEmptyOrNull(request.getFile())) {
            elUserChoice.addAttribute("File", request.getFile());
        }
        if (!StringUtils.isEmptyOrNull(request.getParams())) {
            elUserChoice.addAttribute("params", request.getParams());
        }

        elRequest.addChild(elUserChoice);

        return elRequest.toXml();
    }
}
