package com.dvmms.dejapay;

import com.dvmms.dejapay.models.DejavooTransactionCardData;

/**
 * Created by Platon on 01.08.2016.
 */
public class TransactionCardDataValidator
    extends DefaultValidator<DejavooTransactionCardData>
    implements IValidator<DejavooTransactionCardData>
{
    private static final int MIN_LENGTH_PAN = 16;
    private static final int MIN_EXPIRY_DATE = 1000;
    private static final int MAX_EXPIRY_DATE = 9999;

    @Override
    public boolean validate(ValidatorContext context, DejavooTransactionCardData field) {
        if (field.getEntType() == null) {
            context.addError("CardData: EntType must be not null");
            return false;
        }

        boolean isValid = true;
        if (field.getEntType() == DejavooTransactionCardData.Type.Manual) {
            if (field.getPAN() != null) {
                if (field.getPAN().length() < MIN_LENGTH_PAN) {
                    context.addError("CardData: PAN must be greater or equal than %d digits", MIN_LENGTH_PAN);
                    isValid = false;
                }
            }

            if (field.getExpiryDate() != null) {
                if (field.getExpiryDate() < MIN_EXPIRY_DATE && field.getExpiryDate() > MAX_EXPIRY_DATE) {
                    context.addError("CardData: Expiry Date must be in range [%d, %d]", MIN_EXPIRY_DATE, MAX_EXPIRY_DATE);
                    isValid = false;
                }
            }
        } else {
            if (StringUtils.isEmptyOrNull(field.getTrack1()) && StringUtils.isEmptyOrNull(field.getTrack2())) {
                context.addError("CardData: Both Track1 and Track2 must be not empty or null in not manual mode");
                isValid = false;
            }
        }

        return isValid;
    }
}
