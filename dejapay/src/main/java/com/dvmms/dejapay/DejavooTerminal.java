package com.dvmms.dejapay;

import com.dvmms.dejapay.exception.DejavooInternalTerminalException;
import com.dvmms.dejapay.exception.DejavooInvalidRequestException;
import com.dvmms.dejapay.exception.DejavooInvalidResponseException;
import com.dvmms.dejapay.exception.DejavooThrowable;
import com.dvmms.dejapay.models.DejavooGetCardRequest;
import com.dvmms.dejapay.models.DejavooGetCardResponse;
import com.dvmms.dejapay.models.DejavooGetParamsRequest;
import com.dvmms.dejapay.models.DejavooGetParamsResponse;
import com.dvmms.dejapay.models.DejavooPaymentType;
import com.dvmms.dejapay.models.DejavooPrintoutRequest;
import com.dvmms.dejapay.models.DejavooPrintoutResponse;
import com.dvmms.dejapay.models.DejavooTransactionRecord;
import com.dvmms.dejapay.models.requests.DejavooRequestTransactionRecord;
import com.dvmms.dejapay.models.requests.DejavooResponseTransactionRecords;
import com.dvmms.dejapay.models.requests.DejavooRequestPreAuth;
import com.dvmms.dejapay.models.requests.DejavooRequestSale;
import com.dvmms.dejapay.models.requests.DejavooRequestSettlement;
import com.dvmms.dejapay.models.requests.DejavooResponsePreAuth;
import com.dvmms.dejapay.models.requests.DejavooResponseSale;
import com.dvmms.dejapay.models.DejavooTransactionRequest;
import com.dvmms.dejapay.models.DejavooTransactionResponse;
import com.dvmms.dejapay.models.DejavooTransactionType;
import com.dvmms.dejapay.models.DejavooUserChoiceRequest;
import com.dvmms.dejapay.models.DejavooUserChoiceResponse;
import com.dvmms.dejapay.models.DejavooUserInputRequest;
import com.dvmms.dejapay.models.DejavooUserInputResponse;
import com.dvmms.dejapay.models.requests.DejavooResponseSettlement;
import com.dvmms.dejapay.models.requests.DejavooResponseVoid;
import com.dvmms.dejapay.transports.IDejavooTransport;
import com.dvmms.dejapay.transports.ITransportCallback;

/**
 * Implemented general logic for DejPay terminal
 */
public class DejavooTerminal {
    private final static String TAG = "[TERMINAL] ";
    private final IDejavooTransport transport;
    private final TransactionConvertor transactionConvertor;
    private final PrintoutConvertor printoutConvertor;
    private final UserChoiceConvertor userChoiceConvertor;
    private final UserInputConvertor userInputConvertor;
    private final GetParamsConvertor getParamsConvertor;
    private final GetTransactionRecordsConvertor getTransactionRecordsConvertor;
    private final GetCardConvertor getCardConvertor;

    private TetheringThread tetheringThread;
    private IDejaPayLogger logger;

    /**
     * Initialize DejaPay terminal
     * @param transport implementation of IDejavooTransport interface
     * @see IDejavooTransport
     */
    public DejavooTerminal(IDejavooTransport transport) {
        this.transport = transport;

        this.transactionConvertor = new TransactionConvertor();
        this.printoutConvertor = new PrintoutConvertor();
        this.userChoiceConvertor = new UserChoiceConvertor();
        this.userInputConvertor = new UserInputConvertor();
        this.getParamsConvertor = new GetParamsConvertor();
        this.getTransactionRecordsConvertor = new GetTransactionRecordsConvertor();
        this.getCardConvertor = new GetCardConvertor();

        this.logger = new FakeLogger();

        tetheringThread = new TetheringThread(this.logger);
        init();
    }

    public void init() {
        tetheringThread.start();
    }

    /**
     * Set logger
     * @param logger logger
     * @throws IllegalArgumentException if logger is empty
     */
    public void setLogger(IDejaPayLogger logger) throws IllegalArgumentException {
        if (logger == null) {
            throw new IllegalArgumentException();
        }
        this.logger = logger;

        if (transport != null) {
            transport.setLogger(logger);
        }

        tetheringThread.setLogger(logger);
    }


    /**
     * Connect to DejaPay terminal
     *
     * @return is connected
     */
    public boolean connect() {
        return transport.connect();
    }

    /**
     * Disconnect to DejaPay terminal
     */
    public void disconnect() {
        transport.stop();
        tetheringThread.finish();
    }

    public void cancel() {
        if (transport != null) {
            transport.cancel();
        }
    }

    /**
     * Send transaction request to DejaPay terminal
     *
     * @param request a transaction request
     * @param callback callback
     */
    public void sendTransactionRequest(final DejavooTransactionRequest request, final IRequestCallback<DejavooTransactionResponse> callback) {
        request.setRegisterId(transport.getRegisterId());
        request.setAuthenticationKey(transport.getAuthKey());
        request.setTpn(transport.getTpn());

        DejavooValidatorResult result = DejavooValidator.validator()
                .on(request, new TransactionRequestValidator())
                .doValidate();

        if (result.isValid()) {
            String packet = transactionConvertor.serialize(request);
            logger.d(TAG+"Send transaction: %s", packet);
            transport.sendPacket(IDejavooTransport.SPinType.Transaction, packet, new ITransportCallback() {
                @Override
                public void onResponse(String data) {
                    DejavooTerminal.this.logger.d(TAG+"Received response: %s", data);
                    final DejavooTransactionResponse response = transactionConvertor.deserialize(data);

                    if (response != null) {
                        if (response.isTethering()) {
                            DejavooTerminal.this.logger.d(TAG+"Processing tethering request");
                            if (!tetheringThread.isOpenedSocket()) {
                                DejavooTerminal.this.logger.d(TAG+"Socket closed, try to open socket");
                                tetheringThread.processConnect(response.getTetheringHost(),
                                        response.getTetheringPort(),
                                        response.getTetheringSecurity());
                            }


                            tetheringThread.process(
                                    response.getTetheringProtocol(), response.getTetheringMessage(),
                                    request.getPaymentType(),
                                    new TetheringThread.ISpinTetheringListener() {
                                        @Override
                                        public void onResponse(byte[] bytes) {
                                            DejavooTerminal.this.logger.d(TAG+"Processing tethering request completed");
                                            request.setTetheringMessage(bytes);
                                            sendTransactionRequest(request, callback);
                                        }

                                        @Override
                                        public void onError(DejavooThrowable throwable) {
                                            DejavooTerminal.this.logger.e(throwable,TAG+"Tethering request failed");
                                            callback.onError(throwable);
                                        }
                                    });

                        } else {
                            tetheringThread.processDisconnect();

                            if (isEmptyOrNull(response.getError())) {
                                // When we use PaymentType=Batch then Response doesn't contain PaymentType and TransType
                                if (response.getPaymentType() == null) {
                                    response.setPaymentType(request.getPaymentType());
                                }
                                if (response.getTransactionType() == null) {
                                    response.setTransactionType(request.getTransactionType());
                                }

                                callback.onResponse(response);
                            } else {
                                logger.e(new DejavooInternalTerminalException(
                                        response.getError(), response.getErrorCode()), TAG+"SPIN internal error");
                                callback.onError(new DejavooInternalTerminalException(
                                        response.getError(), response.getErrorCode())
                                );
                            }
                        }
                    } else {
                        logger.e(new DejavooInvalidResponseException(), TAG+"SPIN response is invalid");
                        callback.onError(new DejavooInvalidResponseException());
                    }
                }

                @Override
                public void onError(DejavooThrowable throwable) {
                    logger.e(throwable, TAG+"SPIN request failed");
                    callback.onError(throwable);
                }
            });
        } else {
            logger.e(new DejavooInvalidRequestException(result.getErrors()), TAG+"SPIN request is invalid");
            callback.onError(new DejavooInvalidRequestException(result.getErrors()));
        }
    }

    /**
     * Send PrintOut request to DejaPay terminal
     * @param request PrintOut request
     * @param callback callback
     */
    public void sendPrintoutRequest(final DejavooPrintoutRequest request, final IRequestCallback<DejavooPrintoutResponse> callback) {
        request.setRegisterId(transport.getRegisterId());
        request.setAuthenticationKey(transport.getAuthKey());
        request.setTpn(transport.getTpn());

        String packet = printoutConvertor.serialize(request);
        DejavooTerminal.this.logger.d("Send printout: %s", packet);
        transport.sendPacket(IDejavooTransport.SPinType.Printout, packet, new ITransportCallback() {
            @Override
            public void onResponse(String data) {
                DejavooTerminal.this.logger.d("Received response: %s", data);
                final DejavooPrintoutResponse response = printoutConvertor.deserialize(data);

                if (response != null) {
                    callback.onResponse(response);
                } else {
                    callback.onError(new DejavooInvalidResponseException());
                }
            }

            @Override
            public void onError(DejavooThrowable throwable) {
                callback.onError(throwable);
            }
        });
    }

    /**
     * Send User Choice request to DejaPay terminal
     * @param request User Choice request
     * @param callback callback
     */
    public void sendUserChoiceRequest(final DejavooUserChoiceRequest request, final IRequestCallback<DejavooUserChoiceResponse> callback) {
        request.setAuthKey(transport.getAuthKey());
        request.setTpn(transport.getTpn());

        String packet = userChoiceConvertor.serialize(request);
        DejavooTerminal.this.logger.d("Send user choice: %s", packet);
        transport.sendPacket(IDejavooTransport.SPinType.Transaction, packet, new ITransportCallback() {
            @Override
            public void onResponse(String data) {
                DejavooTerminal.this.logger.d("Received response: %s", data);
                final DejavooUserChoiceResponse response = userChoiceConvertor.deserialize(data);

                if (response != null) {
                    callback.onResponse(response);
                } else {
                    callback.onError(new DejavooInvalidResponseException());
                }
            }

            @Override
            public void onError(DejavooThrowable throwable) {
                callback.onError(throwable);
            }
        });
    }

    /**
     * Send User Input request to DejaPay terminal
     * @param request User Input request
     * @param callback callback
     */
    public void sendUserInputRequest(final DejavooUserInputRequest request, final IRequestCallback<DejavooUserInputResponse> callback) {
        String packet = userInputConvertor.serialize(request);
        DejavooTerminal.this.logger.d("Send user choice: %s", packet);
        transport.sendPacket(IDejavooTransport.SPinType.Transaction, packet, new ITransportCallback() {
            @Override
            public void onResponse(String data) {
                DejavooTerminal.this.logger.d("Received response: %s", data);
                final DejavooUserInputResponse response = userInputConvertor.deserialize(data);

                if (response != null) {
                    callback.onResponse(response);
                } else {
                    callback.onError(new DejavooInvalidResponseException());
                }
            }

            @Override
            public void onError(DejavooThrowable throwable) {
                callback.onError(throwable);
            }
        });
    }

    /**
     * Get connection state
     * @param tpn Terminal TPN
     * @param callback Callback with state
     */
    public void getState(String tpn, ICallback<IDejavooTransport.State> callback) {
        if (transport != null) {
            transport.getState(tpn, callback);
            return;
        }

        callback.call(IDejavooTransport.State.None);
    }

    public void getParam(String file, String params, final IRequestCallback<DejavooGetParamsResponse> callback) {
        DejavooGetParamsRequest request = new DejavooGetParamsRequest();

        request.setRegisterId(transport.getRegisterId());
        request.setAuthKey(transport.getAuthKey());
        request.setFile(file);
        request.setParams(params);

        String packet = getParamsConvertor.serialize(request);
        DejavooTerminal.this.logger.d("Send GetParams request: %s", packet);
        transport.sendPacket(IDejavooTransport.SPinType.Transaction, packet, new ITransportCallback() {
            @Override
            public void onResponse(String data) {
                DejavooTerminal.this.logger.d("Received response: %s", data);
                final DejavooGetParamsResponse response = getParamsConvertor.deserialize(data);

                if (response != null) {
                    callback.onResponse(response);
                } else {
                    callback.onError(new DejavooInvalidResponseException());
                }
            }

            @Override
            public void onError(DejavooThrowable throwable) {
                callback.onError(throwable);
            }
        });
    }

    /**
     * Get current TPN
     * @param callback calls onResponse with TPN on success response and onError on failed response
     * @see IRequestCallback
     * @see #getParam(String, String, IRequestCallback) using GetParam request to get TPN
     * Dangerous method, it can be reason of crash (Found crash for Z1)
     */
    public void getTpn(final IRequestCallback<String> callback) {
        getParam("DVCoreAdParm.xml", "MMS_TPN", new IRequestCallback<DejavooGetParamsResponse>() {
            @Override
            public void onResponse(DejavooGetParamsResponse response) {
                callback.onResponse(response.getValue("MMS_TPN"));
            }

            @Override
            public void onError(DejavooThrowable t) {
                callback.onError(t);
            }
        });
    }

    public void getCard(String prompt, int timeoutSeconds, final IRequestCallback<DejavooGetCardResponse> callback) {
        final DejavooGetCardRequest request = new DejavooGetCardRequest();

        request.setRegisterId(transport.getRegisterId());
        request.setAuthenticationKey(transport.getAuthKey());
        request.setTpn(transport.getTpn());
        request.setPrompt(prompt);
        request.setTimeout(timeoutSeconds);


        String packet = getCardConvertor.serialize(request);
        logger.d(TAG+"Send GetCard request: %s", packet);
        transport.sendPacket(IDejavooTransport.SPinType.Transaction, packet, new ITransportCallback() {
            @Override
            public void onResponse(String data) {
                DejavooTerminal.this.logger.d(TAG+"Received GetCard response: %s", data);
                final DejavooGetCardResponse response = getCardConvertor.deserialize(data);

                if (response != null) {
                    callback.onResponse(response);
                } else {
                    logger.e(new DejavooInvalidResponseException(), TAG+"SPIN response is invalid");
                    callback.onError(new DejavooInvalidResponseException());
                }
            }

            @Override
            public void onError(DejavooThrowable throwable) {
                logger.e(throwable, TAG+"SPIN request failed");
                callback.onError(throwable);
            }
        });

    }

    // Version 2.0
    public void processSaleRequest(final DejavooRequestSale sale, final IRequestCallback<DejavooResponseSale> callback) {
        final DejavooTransactionRequest request = new DejavooTransactionRequest();

        request.setPaymentType(sale.getPaymentType());
        request.setTransactionType(DejavooTransactionType.Sale);
        request.setReferenceId(sale.getRefId());
        request.setAmount(sale.getAmount() + sale.getTip());

        request.setRegisterId(transport.getRegisterId());
        request.setAuthenticationKey(transport.getAuthKey());
        request.setSignatureCapable(true);

        request.setInvoiceNumber(String.valueOf(sale.getInvoiceId()));

//        if (cart.getTableId() > 0) {
//            request.setTableNumber(cart.getTableId());
//        }

        request.setTip((float)sale.getTip());

        request.setReceiptType(DejavooTransactionRequest.ReceiptType.Both);
        request.setPrintReceipt(sale.getReceipt());

        if (sale.getSignature() != null) {
            request.setSignature(sale.getSignature());
        }

        if (sale.getCardData() != null) {
            request.setCardData(sale.getCardData());
        }

        sendTransactionRequest(request, new IRequestCallback<DejavooTransactionResponse>() {
            @Override
            public void onResponse(DejavooTransactionResponse response) {
                DejavooResponseSale saleResponse = DejavooResponseSale.builder()
                        .setAmount( response.getTotalAmount("") )
                        .setTip( response.getTip("") )
                        .setAcntLast4( response.getAcntLast4("") )
                        .setAuthenticationCode( response.getAuthenticationCode() )
                        .setType( sale.getPaymentType() )
                        .setReferenceId( response.getReferenceId() )
                        .setInvoiceId( response.getInvoiceNumber() )
                        .setReceipts(response.getRawReceipts())
                        .setTransactionResponse(response)
                        .setSuccess(response.getResultCode() == DejavooTransactionResponse.ResultCode.Succeded)
                        .setErrorMessage(response.getMessage())
                        .build();

                callback.onResponse(saleResponse);
            }

            @Override
            public void onError(DejavooThrowable throwable) {
                callback.onError(throwable);
            }
        });

    }

    public void processTipAdjustRequest(final DejavooResponseSale sale, double tip, final IRequestCallback<DejavooResponseSale> callback) {
        final DejavooTransactionRequest request = new DejavooTransactionRequest();

        request.setPaymentType(sale.getType());
        request.setTransactionType(DejavooTransactionType.TipAdjust);

        request.setAmount(sale.getAmount());
        request.setTip((float) tip);
        request.setReferenceId(sale.getReferenceId());
        request.setAcntLast4(sale.getAcntLast4());
        request.setTickerNumber(sale.getInvoiceId());


        sendTransactionRequest(request, new IRequestCallback<DejavooTransactionResponse>() {
            @Override
            public void onResponse(DejavooTransactionResponse response) {
                DejavooResponseSale tippedSale = DejavooResponseSale.builder()
                        .setAmount( response.getTotalAmount("") )
                        .setTip( response.getTip("") )
                        .setAcntLast4( sale.getAcntLast4() )
                        .setAuthenticationCode( sale.getAuthenticationCode() )
                        .setType( sale.getType() )
                        .setReferenceId( sale.getReferenceId() )
                        .setInvoiceId( sale.getInvoiceId() )
                        .setReceipts(response.getRawReceipts())
                        .setTransactionResponse(response)
                        .setSuccess(response.getResultCode() == DejavooTransactionResponse.ResultCode.Succeded)
                        .setErrorMessage(response.getMessage())
                        .build();

                callback.onResponse(tippedSale);
            }

            @Override
            public void onError(DejavooThrowable throwable) {
                callback.onError(throwable);
            }
        });
    }

    public void processVoidRequest(final DejavooResponseSale sale, final IRequestCallback<DejavooResponseVoid> callback) {
        DejavooTransactionRequest request = new DejavooTransactionRequest();

        request.setPaymentType(sale.getType());
        request.setTransactionType(DejavooTransactionType.Void);

        request.setAmount(sale.getAmount());
        request.setReferenceId(sale.getReferenceId());

        sendTransactionRequest(request, new IRequestCallback<DejavooTransactionResponse>() {
            @Override
            public void onResponse(DejavooTransactionResponse response) {
                callback.onResponse(
                        DejavooResponseVoid.builder()
                                .setPaymentType(sale.getType())
                                .setReceipts(response.getRawReceipts())
                                .setRefId(response.getReferenceId())
                                .setResponse(response)
                                .setSuccess(response.getResultCode() == DejavooTransactionResponse.ResultCode.Succeded)
                                .setErrorMessage(response.getMessage())
                                .build()
                );
            }

            @Override
            public void onError(DejavooThrowable throwable) {
                callback.onError(throwable);
            }
        });
    }

    public void processSettlementRequest(DejavooRequestSettlement settle, final IRequestCallback<DejavooResponseSettlement> callback) {
        final DejavooTransactionRequest request = new DejavooTransactionRequest();
        request.setPaymentType(DejavooPaymentType.Batch);
        request.setTransactionType(DejavooTransactionType.Settle);

        if (settle.isForce()) {
            request.setParameter("Force");
        } else {
            request.setParameter("Close");
        }

        request.setReceiptType(DejavooTransactionRequest.ReceiptType.Both);
        request.setPrintReceipt(settle.getReceipt());
        request.setReferenceId(settle.getRefId());

        sendTransactionRequest(request, new IRequestCallback<DejavooTransactionResponse>() {
            @Override
            public void onResponse(DejavooTransactionResponse response) {
                callback.onResponse(DejavooResponseSettlement.builder()
                        .setTransactionResponse(response)
                        .setSuccess(response.getResultCode() == DejavooTransactionResponse.ResultCode.Succeded)
                        .setErrorMessage(response.getMessage())
                        .setExtData(response.getExtDatas())
                        .build()
                );
            }

            @Override
            public void onError(DejavooThrowable throwable) {
                callback.onError(throwable);
            }
        });
    }

    public void processPreAuthRequest(final DejavooRequestPreAuth preAuth, final IRequestCallback<DejavooResponsePreAuth> callback) {
        final DejavooTransactionRequest request = new DejavooTransactionRequest();

        request.setPaymentType(preAuth.getType());
        request.setTransactionType(DejavooTransactionType.Auth);

        request.setAmount(preAuth.getAmount());
        request.setReferenceId(preAuth.getRefId());

        request.setSignatureCapable(true);

        request.setReceiptType(DejavooTransactionRequest.ReceiptType.Both);
        request.setPrintReceipt( preAuth.getReceipt() );

        request.setInvoiceNumber(String.valueOf(preAuth.getInvoiceId()));


        sendTransactionRequest(request, new IRequestCallback<DejavooTransactionResponse>() {
            @Override
            public void onResponse(DejavooTransactionResponse response) {
                DejavooResponsePreAuth responseSale = DejavooResponsePreAuth.builder()
                        .setAmount( response.getTotalAmount("") )
                        .setTip( response.getTip("") )
                        .setAcntLast4( response.getAcntLast4("") )
                        .setAuthenticationCode( response.getAuthenticationCode() )
                        .setType( preAuth.getType() )
                        .setReferenceId( response.getReferenceId() )
                        .setInvoiceId( Integer.valueOf(response.getInvoiceNumber()) )
                        .build();

                callback.onResponse(responseSale);
            }

            @Override
            public void onError(DejavooThrowable throwable) {
                callback.onError(throwable);
            }
        });
    }

    public void processCapturePreAuthRequest(
            final DejavooResponsePreAuth preAuth,
            double amount,
            double tip,
            DejavooTransactionRequest.ReceiptType receipt,
            final IRequestCallback<DejavooResponseSale> callback
    ) {
        final DejavooTransactionRequest request = new DejavooTransactionRequest();

        request.setPaymentType(preAuth.getType());
        request.setTransactionType(DejavooTransactionType.Sale);
        request.setTransactionType(DejavooTransactionType.Capture);
        request.setReferenceId(preAuth.getReferenceId());
        request.setAmount(amount + tip);

        request.setSignatureCapable(true);

        request.setInvoiceNumber(String.valueOf(preAuth.getInvoiceId()));


        request.setAuthenticationCode(preAuth.getAuthenticationCode());
        request.setAcntLast4(preAuth.getAcntLast4());

        request.setTip((float)tip);

        request.setReceiptType(DejavooTransactionRequest.ReceiptType.Both);
        request.setPrintReceipt(receipt);

//        if (preAuth.getSignature() != null) {
//            request.setSignature(sale.getSignature());
//        }

        sendTransactionRequest(request, new IRequestCallback<DejavooTransactionResponse>() {
            @Override
            public void onResponse(DejavooTransactionResponse response) {
                DejavooResponseSale responseSale = DejavooResponseSale.builder()
                        .setAmount( response.getTotalAmount("") )
                        .setTip( response.getTip("") )
                        .setAcntLast4( response.getAcntLast4("") )
                        .setAuthenticationCode( response.getAuthenticationCode() )
                        .setType( preAuth.getType() )
                        .setReferenceId( response.getReferenceId() )
                        .setInvoiceId( response.getInvoiceNumber() )
                        .build();

                callback.onResponse(responseSale);
            }

            @Override
            public void onError(DejavooThrowable throwable) {
                callback.onError(throwable);
            }
        });
    }

    // offset start from 1
    public void getTransactions(
            final DejavooPaymentType paymentType,
            final int offset,
            int limit,
            final IRequestCallback<DejavooResponseTransactionRecords> callback
    ) {
        String packet = getTransactionRecordsConvertor.serialize(
                transport.getTpn(),
                paymentType,
                transport.getRegisterId(),
                transport.getAuthKey(),
                offset,
                offset + limit - 1);


        logger.d(TAG+"Get transactions: %s", packet);
        transport.sendPacket(IDejavooTransport.SPinType.Transaction, packet, new ITransportCallback() {
            @Override
            public void onResponse(String data) {
                DejavooTerminal.this.logger.d(TAG+"Received response: %s", data);
                final DejavooResponseTransactionRecords response = getTransactionRecordsConvertor.deserialize(data);

                if (response != null) {
                    int start = offset;
                    for (DejavooTransactionRecord record : response.getRecords()) {
                        record.setTransNumber(start);
                        start++;
                    }

                    callback.onResponse(response);
                } else {
                    logger.e(new DejavooInvalidResponseException(), TAG+"SPIN response is invalid");
                    callback.onError(new DejavooInvalidResponseException());
                }
            }

            @Override
            public void onError(DejavooThrowable throwable) {
                logger.e(throwable, TAG+"SPIN request failed");
                callback.onError(throwable);
            }
        });
    }

    public void getTransaction(String tpn, DejavooRequestTransactionRecord record, final IRequestCallback<DejavooTransactionResponse> callback) {
        final DejavooTransactionRequest request = new DejavooTransactionRequest();

        request.setPaymentType(record.getPaymentType());
        request.setTransactionType(DejavooTransactionType.Status);
        request.setReferenceId(record.getRefId());

        request.setSignatureCapable(true);

        request.setInvoiceNumber(record.getInvoiceId());

        request.setReceiptType(record.getReturnReceipt());
        request.setPrintReceipt(record.getPrintReceipt());
        request.setOnBypassSignature();
        if (record.getTransNum() > 0) {
            request.setTransNum(record.getTransNum());
        }

        request.setTpn(tpn);

        sendTransactionRequest(request, callback);
    }

    // ---

    static boolean isEmptyOrNull(String text) {
        return (text == null || text.isEmpty());
    }


}
