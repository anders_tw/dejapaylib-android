package com.dvmms.dejapay.models;

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Transaction request
 */
public class DejavooTransactionRequest {

    public enum ReceiptType {
        No, Merchant, Customer, Both, Batch, Note
    }

    public enum FrequencyType {
        OneTime
    }

    private DejavooPaymentType paymentType;
    private DejavooTransactionType transactionType;
    private String authenticationKey;
    private Float amount;
    private Float tip;
    private Integer points;
    private String invoiceNumber;
    private String referenceId;
    private String authenticationCode;
    private Integer clerkId;
    private Integer tableNumber;
    private String tickerNumber;
    private String acntLast4;
    private String registerId;
    private String parameter;
    private String token;

    private FrequencyType frequency;

    private byte[] tetheringMessage;
    private byte[] signature;

    private boolean signatureCapable;
    private Boolean signatureCapture;

    private ReceiptType receiptType;
    private ReceiptType printReceipt;
    private String tpn;
    private Integer transNum;

    DejavooTransactionCardData cardData;

    private List<IDejavooPrintoutItem> extReceiptData;
    private List<IDejavooPrintoutItem> printout;

    private Map<String, String> customFields;
    private Map<String, String> customOptions;

    /**
     * Get payment type of request
     * @return DejaPay payment type of request
     * @see DejavooPaymentType
     */
    public DejavooPaymentType getPaymentType() {
        return paymentType;
    }

    /**
     * Set payment type for request
     * @param paymentType DejaPay payment type of request
     * @see DejavooPaymentType
     */
    public void setPaymentType(DejavooPaymentType paymentType) {
        this.paymentType = paymentType;
    }

    /**
     * Get transaction type of request
     * @return DejaPay transaction type
     * @see DejavooTransactionType
     */
    public DejavooTransactionType getTransactionType() {
        return transactionType;
    }

    /**
     * Set transaction type of request
     * @param transactionType transaction type
     * @see DejavooTransactionType
     */
    public void setTransactionType(DejavooTransactionType transactionType) {
        this.transactionType = transactionType;
    }

    /**
     * Get authentication key
     * @return authentication key
     */
    public String getAuthenticationKey() {
        return authenticationKey;
    }

    /**
     * Set authentication key
     * @param authenticationKey authentication key
     */
    public void setAuthenticationKey(String authenticationKey) {
        this.authenticationKey = authenticationKey;
    }

    /**
     * Get amount of the transaction
     * @return amount of the transaction
     */
    public Float getAmount() {
        return amount;
    }

    /**
     * Dollar amount of the transaction.
     * Note: This is the total amount. If tip is added, Amount will include base + tip.
     * Not used , if using Loyalty transactions in Points value.
     * @param amount amount of the transaction
     */
    public void setAmount(Float amount) {
        this.amount = amount;

        if (amount != null) {
            this.amount = round(amount, 2);
        }
    }

    public void setAmount(Double amount) {
        if (amount != null) {
            this.amount = round(amount.floatValue(), 2);
        } else {
            this.amount = null;
        }
    }

    /**
     * Dollar amount of the transaction as tip.
     * Note: Tip must be enabled on terminal
     *
     * @return tip
     */
    public Float getTip() {
        return tip;
    }

    /**
     * Dollar amount of the transaction.
     * Note: Tip must be enabled on terminal
     *
     * @param tip amount of tip
     */
    public void setTip(Float tip) {
        this.tip = tip;

        if (tip != null) {
            this.tip = round(tip, 2);
        }
    }

    /**
     * Populated for Loyalty transactions in Points value
     * @return points
     */
    public Integer getPoints() {
        return points;
    }

    /**
     * Populated for Loyalty transactions in Points value
     * @param points points
     */
    public void setPoints(Integer points) {
        this.points = points;
    }

    /**
     * Unique alpha numeric invoice number. Used if terminal Invoice parameter is disabled
     * @return Invoice Number
     */
    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    /**
     * Unique alpha numeric invoice number. Used if terminal Invoice parameter is disabled
     * @param invoiceNumber Invoice Number
     */
    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    /**
     * Alpha numeric reference field. Must be unique for the open batch scope.
     * Set by the host system and, Mandatory for all deployments.
     * Used for Tip or other transaction adjustment requests related to a given transaction,
     * for example, Tip Adjustment or Void Transaction request must have a RefId value of
     * an original transaction.
     * @return Reference Id
     */
    public String getReferenceId() {
        return referenceId;
    }

    /**
     * Alpha numeric reference field. Must be unique for the open batch scope.
     * Set by the host system and, Mandatory for all deployments.
     * Used for Tip or other transaction adjustment requests related to a given transaction,
     * for example, Tip Adjustment or Void Transaction request must have a RefId value of
     * an original transaction.
     * @param referenceId Reference Id
     */
    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }

    /**
     * Get authentication code
     * @return Authentication code
     */
    public String getAuthenticationCode() {
        return authenticationCode;
    }

    /**
     * Set authentication code
     * @param authenticationCode Authentication code
     */
    public void setAuthenticationCode(String authenticationCode) {
        this.authenticationCode = authenticationCode;
    }

    /**
     * Get clerk id
     * @return Clerk id
     */
    public Integer getClerkId() {
        return clerkId;
    }

    /**
     * Set clerk id
     * @param clerkId Clerk id
     */
    public void setClerkId(Integer clerkId) {
        this.clerkId = clerkId;
    }

    /**
     * Get table number
     * @return Table number
     */
    public Integer getTableNumber() {
        return tableNumber;
    }

    /**
     * Set table number
     * @param tableNumber Table number
     */
    public void setTableNumber(Integer tableNumber) {
        this.tableNumber = tableNumber;
    }

    /**
     * Get ticket number
     * @return Ticket number
     */
    public String getTickerNumber() {
        return tickerNumber;
    }

    /**
     * Set ticket number
     * @param tickerNumber Ticket number
     */
    public void setTickerNumber(String tickerNumber) {
        this.tickerNumber = tickerNumber;
    }

    /**
     * Get last 4 digit of account number
     * @return Last 4 digit of account number
     */
    public String getAcntLast4() {
        return acntLast4;
    }

    /**
     * Set last 4 digit of account number
     * @param acntLast4 Last 4 digit of account number
     */
    public void setAcntLast4(String acntLast4) {
        this.acntLast4 = acntLast4;
    }

    /**
     * Alpha numeric reference field.
     * @return Register Id
     */
    public String getRegisterId() {
        return registerId;
    }

    /**
     * Alpha numeric reference field.
     * @param registerId Register Id
     */
    public void setRegisterId(String registerId) {
        this.registerId = registerId;
    }

    /**
     * Type of Settlement request:
     * - Close: normal closing
     * - Clear: Delete local batch (Dangerous! Must be used only if normal settlement is not possible)
     * - Force: Force batch totals
     * @return Parameter
     */
    public String getParameter() {
        return parameter;
    }

    /**
     * Type of Settlement request:
     * - Close: normal closing
     * - Clear: Delete local batch (Dangerous! Must be used only if normal settlement is not possible)
     * - Force: Force batch totals
     * @param parameter Additional parameter
     */
    public void setParameter(String parameter) {
        this.parameter = parameter;
    }

    /**
     * Get tethering message
     * @return Tethering Message Bytes
     */
    public byte[] getTetheringMessage() {
        return tetheringMessage;
    }

    /**
     * Set result from tethering server
     * @param tetheringMessage Tethering Message bytes
     */
    public void setTetheringMessage(byte[] tetheringMessage) {
        this.tetheringMessage = tetheringMessage;
    }

    /**
     * Get capable signature
     * @return signature capable
     */
    public boolean isSignatureCapable() {
        return signatureCapable;
    }

    /**
     * Set capable signature
     * @param signatureCapable Support signature
     */
    public void setSignatureCapable(boolean signatureCapable) {
        this.signatureCapable = signatureCapable;
    }

    /**
     * Get Card Data
     * @return Card Data
     */
    public DejavooTransactionCardData getCardData() {
        return cardData;
    }

    /**
     * Set Card Data
     * @param cardData Card Data
     */
    public void setCardData(DejavooTransactionCardData cardData) {
        this.cardData = cardData;
    }

    /**
     * Get signature
     * @return Signature bytes
     */
    public byte[] getSignature() {
        return signature;
    }

    /**
     * Set signature
     * @param signature Signature bytes
     */
    public void setSignature(byte[] signature) {
        this.signature = signature;
    }

    /**
     * Get Receipt Type
     * @return Receipt Type
     * @see ReceiptType
     */
    public ReceiptType getReceiptType() {
        return receiptType;
    }

    /**
     * Set Receipt Type
     * @param receiptType Receipt Type
     */
    public void setReceiptType(ReceiptType receiptType) {
        this.receiptType = receiptType;
    }

    /**
     * Get receipts will be printed
     * @return Receipt Type
     */
    public ReceiptType getPrintReceipt() {
        return printReceipt;
    }

    /**
     * Set receipts will be printed
     * @param printReceipt Receipt Type
     */
    public void setPrintReceipt(ReceiptType printReceipt) {
        this.printReceipt = printReceipt;
    }

    /**
     * Get token
     * @return Token
     */
    public String getToken() {
        return token;
    }

    /**
     * Set token
     * @param token Token
     */
    public void setToken(String token) {
        this.token = token;
    }

    /**
     * Get custom fields
     * @return custom fields
     */
    public Map<String, String> getCustomFields() {
        return customFields;
    }

    /**
     * Add custom field
     * @param title custom field title
     * @param value custom field value
     */
    public void addCustomFields(String title, String value) {
        if (customFields == null) {
            customFields = new LinkedHashMap<>();
        }

        customFields.put(title, value);
    }

    public Map<String, String> getCustomOptions() {
        return customOptions;
    }

    public void setCustomOptions(Map<String, String> customOptions) {
        this.customOptions = customOptions;
    }

    public void addCustomOption(String name, String value) {
        if (customOptions == null) {
            customOptions = new LinkedHashMap<>();
        }

        customFields.put(name, value);
    }

    public FrequencyType getFrequency() {
        return frequency;
    }

    public void setFrequency(FrequencyType frequency) {
        this.frequency = frequency;
    }

    public List<IDejavooPrintoutItem> getExtReceiptData() {
        return extReceiptData;
    }

    public void setExtReceiptData(List<IDejavooPrintoutItem> extReceiptData) {
        this.extReceiptData = extReceiptData;
    }

    public List<IDejavooPrintoutItem> getPrintout() {
        return printout;
    }

    public void setPrintout(List<IDejavooPrintoutItem> printout) {
        this.printout = printout;
    }

    public void setOnBypassSignature() {
        this.signatureCapture = false;
    }

    public void setOffBypassSignature() {
        this.signatureCapture = null;
    }

    public boolean isBypassSignature() {
        return (signatureCapture != null);
    }

    public String getTpn() {
        return tpn;
    }

    public void setTpn(String tpn) {
        this.tpn = tpn;
    }

    public Integer getTransNum() {
        return transNum;
    }

    public void setTransNum(Integer transNum) {
        this.transNum = transNum;
    }

    static Float round(float d, int decimalPlace) {
        BigDecimal bd = new BigDecimal(Float.toString(d));
        bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
        return bd.floatValue();
    }

    @Override
    public String toString() {
        return "DejavooTransactionRequest{" +
                "paymentType=" + paymentType +
                ", transactionType=" + transactionType +
                ", authenticationKey='" + authenticationKey + '\'' +
                ", amount=" + amount +
                ", tip=" + tip +
                ", points=" + points +
                ", invoiceNumber='" + invoiceNumber + '\'' +
                ", referenceId='" + referenceId + '\'' +
                ", authenticationCode='" + authenticationCode + '\'' +
                ", acntLast4='" + acntLast4 + '\'' +
                ", registerId='" + registerId + '\'' +
                ", receiptType=" + receiptType +
                ", printReceipt=" + printReceipt +
                ", cardData=" + cardData +
                ", customOptions=" + customOptions +
                '}';
    }
}
