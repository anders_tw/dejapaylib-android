package com.dvmms.dejapay.models;

import java.util.ArrayList;
import java.util.List;

/**
 * User Choice request
 */
public class DejavooUserChoiceRequest {
    private String tpn;
    private String authKey;
    private String title;
    private Integer timeout;
    private List<String> choices;

    public DejavooUserChoiceRequest() {
        choices = new ArrayList<>();
    }

    public String getTpn() {
        return tpn;
    }

    public void setTpn(String tpn) {
        this.tpn = tpn;
    }

    /**
     * Get authentication key
     * @return authentication key
     */
    public String getAuthKey() {
        return authKey;
    }

    /**
     * Set authentication key
     * @param authKey authentication key
     */
    public void setAuthKey(String authKey) {
        this.authKey = authKey;
    }

    /**
     * Get title prompt
     * @return title prompt
     */
    public String getTitle() {
        return title;
    }

    /**
     * Set title prompt
     * @param title title prompt
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Get timeout prompt
     * @return timeout prompt
     */
    public Integer getTimeout() {
        return timeout;
    }

    /**
     * Set timeout
     * @param timeout timeout prompt
     */
    public void setTimeout(Integer timeout) {
        this.timeout = timeout;
    }

    /**
     * Add choice
     * @param choice choice title
     */
    public void addChoice(String choice) {
        choices.add(choice);
    }

    /**
     * Get choices
     * @return choices
     */
    public List<String> getChoices() {
        return choices;
    }
}
