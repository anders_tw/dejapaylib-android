package com.dvmms.dejapay.models;

/**
 * Created by pmalyugin on 10/04/2017.
 */

public class DejavooPrintoutItemItem implements IDejavooPrintoutItem {
    private String name;
    private Double price;
    private float quantity;
    private Boolean separator;

    private DejavooPrintoutItemItem(DejavooPrintoutItemItem.Builder builder) {
        name = builder.name;
        price = builder.price;
        quantity = builder.quantity;
        separator = builder.separator;
    }

    @Override
    public String data() {
        String data = "<item";


        if (name != null) {
            data += String.format(" name=\"%s\"", name);
        }

        if (price != null) {
            int priceVal = (int)(price * 100.0 + 0.5);
            data += String.format(" price=\"%d\"", priceVal);
        }

        if (quantity > 0) {
            data += String.format(" quantity=\"%.3f\"", quantity);
        }

        if (separator != null) {
            data += " separate=\"true\"";
        }

        data += "/>";

        return data;
    }

    public static class Builder {
        private String name = "";
        private Double price;
        private float quantity = 0.f;
        private Boolean separator;

        public Builder() {

        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder price(Double price) {
            this.price = price;
            return this;
        }

        public Builder quantity(float quantity) {
            this.quantity = quantity;
            return this;
        }

        public Builder separator(boolean separator) {
            this.separator = separator;
            return this;
        }

        public DejavooPrintoutItemItem build() {
            return new DejavooPrintoutItemItem(this);
        }
    }
}
