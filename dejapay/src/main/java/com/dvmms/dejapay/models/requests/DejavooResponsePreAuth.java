package com.dvmms.dejapay.models.requests;

import com.dvmms.dejapay.models.DejavooPaymentType;

public class DejavooResponsePreAuth {
    private final DejavooPaymentType type;
    private final double amount;
    private final double tip;
    private final String referenceId;
    private final String authenticationCode;

    private final String acntLast4;
    private final int invoiceId;

    private DejavooResponsePreAuth(Builder builder) {
        this.type = builder.type;
        this.amount = builder.amount;
        this.tip = builder.tip;
        this.referenceId = builder.referenceId;
        this.authenticationCode = builder.authenticationCode;
        this.acntLast4 = builder.acntLast4;
        this.invoiceId = builder.invoiceId;
    }

    public DejavooPaymentType getType() {
        return type;
    }

    public double getAmount() {
        return amount;
    }

    public double getTip() {
        return tip;
    }

    /**
     * Total amount involves Amount + Tip
     * @return
     */
    public double getTotal() {
        return amount + tip;
    }

    public String getReferenceId() {
        return referenceId;
    }

    public String getAuthenticationCode() {
        return authenticationCode;
    }

    public String getAcntLast4() {
        return acntLast4;
    }

    public int getInvoiceId() {
        return invoiceId;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private DejavooPaymentType type;

        private double amount;
        private double tip;
        private String referenceId;
        private String authenticationCode;

        private String acntLast4;
        private int invoiceId;

        private Builder() { }

        public Builder setType(DejavooPaymentType type) {
            this.type = type;
            return this;
        }

        public Builder setAmount(double amount) {
            this.amount = amount;
            return this;
        }

        public Builder setTip(double tip) {
            this.tip = tip;
            return this;
        }

        public Builder setReferenceId(String referenceId) {
            this.referenceId = referenceId;
            return this;
        }

        public Builder setAuthenticationCode(String authenticationCode) {
            this.authenticationCode = authenticationCode;
            return this;
        }

        public Builder setAcntLast4(String acntLast4) {
            this.acntLast4 = acntLast4;
            return this;
        }

        public Builder setInvoiceId(int invoiceId) {
            this.invoiceId = invoiceId;
            return this;
        }

        public DejavooResponsePreAuth build() {
            return new DejavooResponsePreAuth(this);
        }
    }
}
