package com.dvmms.dejapay.models;

/**
 * Created by pmalyugin on 10/04/2017.
 */

public class DejavooPrintoutSeparatorItem implements IDejavooPrintoutItem {
    public DejavooPrintoutSeparatorItem() { }

    @Override
    public String data() {
        return "<SEP/>";
    }
}
