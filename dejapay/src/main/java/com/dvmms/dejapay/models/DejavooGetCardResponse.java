package com.dvmms.dejapay.models;

import java.util.List;

public class DejavooGetCardResponse {
    private final String registerId;
    private final String message;
    private final String track1;
    private final String track2;
    private final String account;
    private final String responseMessage;

    protected DejavooGetCardResponse(Builder builder) {
        registerId = builder.registerId;
        message = builder.message;
        track1 = builder.track1;
        track2 = builder.track2;
        account = builder.account;
        responseMessage = builder.responseMessage;
    }

    public String getTrack1() {
        return track1;
    }

    public String getTrack2() {
        return track2;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public String getAccount() {
        return account;
    }

    public boolean isSuccess() {
        return message != null && message.equalsIgnoreCase("Success");
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private String registerId = "";
        private String message = "";
        private String track1 = "";
        private String track2 = "";
        private String account = "";
        private String responseMessage = "";

        private Builder() { }

        public Builder setRegisterId(String registerId) {
            this.registerId = registerId;
            return this;
        }

        public Builder setMessage(String message) {
            this.message = message;
            return this;
        }

        public Builder setTrack1(String track1) {
            this.track1 = (track1 != null) ? track1 : "";
            return this;
        }

        public Builder setTrack2(String track2) {
            this.track2 =  (track2 != null) ? track2 : "";
            return this;
        }

        public Builder setAccount(String account) {
            this.account = account;
            return this;
        }

        public Builder setResponseMessage(String responseMessage) {
            this.responseMessage = (responseMessage != null) ? responseMessage : "";
            return this;
        }

        public DejavooGetCardResponse build() {
            return new DejavooGetCardResponse(this);
        }
    }
}
