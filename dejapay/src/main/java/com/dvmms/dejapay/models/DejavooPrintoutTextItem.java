package com.dvmms.dejapay.models;

/**
 * Text item for PrintOut request
 */
@Deprecated
public class DejavooPrintoutTextItem implements IDejavooPrintoutItem {
    private final boolean centered;
    private final boolean large;
    private final boolean inverted;
    private final boolean condensed;
    private final boolean bold;

    private final String text;

    private DejavooPrintoutTextItem(DejavooPrintoutTextItem.Builder builder) {
        centered = builder.centered;
        large = builder.large;
        inverted = builder.inverted;
        condensed = builder.condensed;
        bold = builder.bold;
        text = builder.text;
    }

    @Override
    public String data() {
        String data = this.text;

        if (bold) {
            data = "<b>"+data+"</b>";
        }

        if (centered) {
            data = "<c>"+data+"</c>";
        }

        if (large) {
            data = "<lg>"+data+"</lg>";
        }

        if (inverted) {
            data = "<inv>"+data+"</inv>";
        }

        if (condensed) {
            data = "<cd>"+data+"</cd>";
        }

        data = "<t>" + data + "</t>";

        return data;
    }

    public static class Builder {
        private boolean centered = false;
        private boolean large = false;
        private boolean inverted = false;
        private boolean condensed = false;
        private boolean bold = false;
        private String text;

        public Builder() {

        }

        public Builder isCentered(boolean centered) {
            this.centered = centered;
            return this;
        }

        public Builder isBold(boolean bold) {
            this.bold = bold;
            return this;
        }

        public Builder isLarge(boolean large) {
            this.large = large;
            return this;
        }

        public Builder isInverted(boolean inverted) {
            this.inverted = inverted;
            return this;
        }

        public Builder isCondensed(boolean condensed) {
            this.condensed = condensed;
            return this;
        }

        public Builder text(String text) {
            this.text = text;
            return this;
        }

        public DejavooPrintoutTextItem build() {
            return new DejavooPrintoutTextItem(this);
        }
    }



}