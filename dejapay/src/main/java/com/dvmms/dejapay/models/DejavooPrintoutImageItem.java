package com.dvmms.dejapay.models;

import android.util.Base64;

/**
 * Image item for Printout request
 */
public class DejavooPrintoutImageItem implements IDejavooPrintoutItem {
//    private final Bitmap image;
    private final byte[] bytes;

    protected DejavooPrintoutImageItem(Builder builder) {
//        this.image = builder.image;
        this.bytes = builder.bytes;
    }

    @Override
    public String data() {
        if (bytes != null) {
            String data = Base64.encodeToString(bytes, 0);
            data = "<IMG>" + data + "</IMG>";
            return data;
        }
        return "";
    }

    public static class Builder {
//        private Bitmap image;
        private byte[] bytes;

        public Builder() {  }

// FIXME: Image bitmap is not support.
//        public Builder image(Bitmap image) {
//            this.image = image;
//            return this;
//        }

        public Builder image(byte[] bytes) {
//            this.image = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);

            this.bytes = bytes;

            return this;
        }

        public DejavooPrintoutImageItem build() {
            return new DejavooPrintoutImageItem(this);
        }
    }


}
