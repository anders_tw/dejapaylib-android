package com.dvmms.dejapay.models;

/**
 * Supported DejaPay transaction types.
 */
public enum DejavooTransactionType {
    Sale,
    Return,
    Void,
    Auth,
    Capture,
    Verification,
    Conversion,
    Settle,
    Redeem,
    Refund,
    Reload,
    Activate,
    Deactivate,
    Reissue,
    AddPoints,
    Inquire,
    TipAdjust,
    Ticket,
    Forward,
    Get,
    Status,
    EBTVoucher,
    EBTVoucherReturn,
    EBTVoucherVoid,
    Balance
    ;


    static public DejavooTransactionType fromString(String type) {
        for (DejavooTransactionType transactionType : DejavooTransactionType.values()) {
            if (transactionType.name().equalsIgnoreCase(type)) {
                return transactionType;
            }
        }
        return Sale;
    }
}
