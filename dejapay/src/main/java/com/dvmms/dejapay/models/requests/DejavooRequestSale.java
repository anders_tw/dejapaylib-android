package com.dvmms.dejapay.models.requests;

import com.dvmms.dejapay.DejavooUtils;
import com.dvmms.dejapay.models.DejavooPaymentType;
import com.dvmms.dejapay.models.DejavooTransactionCardData;
import com.dvmms.dejapay.models.DejavooTransactionRequest;
import com.dvmms.dejapay.models.IDejavooPrintoutItem;

import java.util.ArrayList;
import java.util.List;

public class DejavooRequestSale {
    private final DejavooPaymentType paymentType;
    private final double amount;
    private final double tip;
    private final DejavooTransactionRequest.ReceiptType receipt;
    private final List<IDejavooPrintoutItem> printoutItems;
    private final String refId;
    private final int invoiceId;
    private final byte[] signature;
    private final DejavooTransactionCardData cardData;

    private DejavooRequestSale(Builder builder) {
        this.paymentType = builder.paymentType;
        this.amount = builder.amount;
        this.tip = builder.tip;
        this.receipt = builder.receipt;
        this.printoutItems = builder.printoutItems;
        this.refId = builder.refId;
        this.invoiceId = builder.invoiceId;
        this.signature = builder.signature;
        this.cardData = builder.cardData;
    }

    public DejavooPaymentType getPaymentType() {
        return paymentType;
    }

    public double getAmount() {
        return amount;
    }

    public double getTip() {
        return tip;
    }

    public DejavooTransactionRequest.ReceiptType getReceipt() {
        return receipt;
    }

    public List<IDejavooPrintoutItem> getPrintoutItems() {
        return printoutItems;
    }

    public String getRefId() {
        return refId;
    }

    public int getInvoiceId() {
        return invoiceId;
    }

    public byte[] getSignature() {
        return signature;
    }

    public DejavooTransactionCardData getCardData() {
        return cardData;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private DejavooPaymentType paymentType = DejavooPaymentType.Credit;
        private double amount = 0.0;
        private double tip = 0.0;
        private DejavooTransactionRequest.ReceiptType receipt = DejavooTransactionRequest.ReceiptType.Both;
        private List<IDejavooPrintoutItem> printoutItems = new ArrayList<>();
        private String refId = DejavooUtils.generateRandomRefId();
        private int invoiceId = DejavooUtils.generateRandomInvoiceId();
        private byte[] signature = null;
        private DejavooTransactionCardData cardData;

        private Builder() { }

        /**
         * Set Payment type
         * @param paymentType Dejavoo Payment type
         * @return
         */
        public Builder setPaymentType(DejavooPaymentType paymentType) {
            this.paymentType = paymentType;
            return this;
        }

        /**
         * Set Amount
         * @param amount Sale amount
         * @return
         */
        public Builder setAmount(double amount) {
            this.amount = amount;
            return this;
        }

        /**
         * Set tip
         * @param tip tip
         * @return
         */
        public Builder setTip(double tip) {
            this.tip = tip;
            return this;
        }

        /**
         * Set Receipt type
         * @param receipt receipt type, support only No, Merchant, Customer, Both
         * @return
         */
        public Builder setReceipt(DejavooTransactionRequest.ReceiptType receipt) {
            this.receipt = receipt;
            return this;
        }

        /**
         * Set Printout Items
         * @param printoutItems Printout Items
         * @return
         */
        public Builder setPrintoutItems(List<IDejavooPrintoutItem> printoutItems) {
            this.printoutItems = printoutItems;
            return this;
        }

        /**
         * Set Reference Id (optional)
         * @param refId - Reference ID
         * @return
         */
        public Builder setRefId(String refId) {
            this.refId = refId;
            return this;
        }

        /**
         * Set Invoice ID (optional)
         * @param invoiceId
         * @return
         */
        public Builder setInvoiceId(int invoiceId) {
            this.invoiceId = invoiceId;
            return this;
        }

        /**
         * Set Signature (optional)
         * @param signature - signature should be in 1bpp-monochrome
         * @return Builder
         */
        public Builder setSignature(byte[] signature) {
            this.signature = signature;
            return this;
        }

        public Builder setCardData(DejavooTransactionCardData cardData) {
            this.cardData = cardData;
            return this;
        }

        public DejavooRequestSale build() {
            return new DejavooRequestSale(this);
        }

    }


}
