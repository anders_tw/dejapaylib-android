package com.dvmms.dejapay.models.requests;

import com.dvmms.dejapay.models.DejavooTransactionRecord;

import java.util.ArrayList;
import java.util.List;

public class DejavooResponseTransactionRecords {
    private final boolean success;
    private final int numRecords;
    private final List<DejavooTransactionRecord> records;
    private final String message;
    private final String responseMessage;

    public DejavooResponseTransactionRecords(Builder builder) {
        this.numRecords = builder.records.size();
        this.records = builder.records;
        this.success = builder.success;
        this.message = builder.message;
        this.responseMessage = builder.responseMessage;
    }

    public int getNumRecords() {
        return numRecords;
    }

    public List<DejavooTransactionRecord> getRecords() {
        return records;
    }

    public boolean isSuccess() {
        return success;
    }

    public String getMessage() {
        return message;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private List<DejavooTransactionRecord> records = new ArrayList<>();
        public boolean success = true;
        public String message = "";
        public String responseMessage = "";

        private Builder() { }

        public Builder setSuccess(boolean success) {
            this.success = success;
            return this;
        }

        public Builder setMessage(String message) {
            this.message = message;
            return this;
        }

        public Builder setResponseMessage(String responseMessage) {
            this.responseMessage = responseMessage;
            return this;
        }

        public Builder addRecord(DejavooTransactionRecord record) {
            this.records.add(record);
            return this;
        }

        public DejavooResponseTransactionRecords build() {
            return new DejavooResponseTransactionRecords(this);
        }
    }
}
