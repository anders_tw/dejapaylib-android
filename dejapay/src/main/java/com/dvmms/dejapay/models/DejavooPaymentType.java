package com.dvmms.dejapay.models;

/**
 * Supported DejaPay payment types.
 */
public enum DejavooPaymentType {
    Credit("DvCredit"),
    Debit("DvDebit"),
    Cash("DvCash"),
    Check("DvCheck"),
    Gift("DvGift"),
    Loaylty("DvLoyalty"),
    Batch(""),
    Report(""),
    Invoice("DvInvoice"),
    EBT_Food("DvEBTFood"),
    EBT_Cash("DvEBTCash"),
    Unknown("")
    ;

    private final String app;

    DejavooPaymentType(String app) {
        this.app = app;
    }

    static public DejavooPaymentType fromApp(String app) {
        if (app == null || app.isEmpty()) {
            return Unknown;
        }

        for (DejavooPaymentType paymentType : DejavooPaymentType.values()) {
            if (app.equalsIgnoreCase(paymentType.app)) {
                return paymentType;
            }
        }
        return Unknown;
    }

    static public DejavooPaymentType fromString(String type) {
        for (DejavooPaymentType paymentType : DejavooPaymentType.values()) {
            if (paymentType.name().equalsIgnoreCase(type)) {
                return paymentType;
            }
        }
        return Unknown;
    }
}
