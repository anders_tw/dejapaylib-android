package com.dvmms.dejapay.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Pair;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by pmalyugin on 30/08/2017.
 */

public class DejavooResponseExtData implements Serializable, Parcelable {
    private final String app;
    private final Map<String, String> data;

    public DejavooResponseExtData(String app, Map<String, String> data) {
        this.app = app;
        this.data = data;
    }

    public String getApp() {
        return app;
    }

    public Map<String, String> getData() {
        return data;
    }


    public DejavooCardType getCardType() {
        String cardTypeString = getCardTypeString();

        if (cardTypeString != null && cardTypeString.length() > 0) {
            for (DejavooCardType type : DejavooCardType.values()) {
                if (type.name().equalsIgnoreCase(cardTypeString)) {
                    return type;
                }
            }
        }

        return DejavooCardType.UNKNOWN;
    }

    public String getCardTypeString() {
        if (data != null && data.containsKey("CardType")) {
            return data.get("CardType");
        }
        return "";
    }

    public int getBatchNumber() {
        if (data != null && data.containsKey("BatchNum")) {
            return parseInteger(data.get("BatchNum"), -1);
        }

        return -1;
    }

    public double getTip() {
        if (data != null && data.containsKey("Tip")) {
            return parseDouble(data.get("Tip"), 0);
        }

        return 0;
    }

    public double getAmount() {
        return getTotalAmount() - getTip() - getFee();
    }

    public double getFee() {
        return getMerchantFee() + getServiceFee();
    }

    public double getTotalAmount() {
        if (data != null && data.containsKey("TotalAmt")) {
            return parseDouble(data.get("TotalAmt"), 0);
        }

        return 0;
    }

    public String getCardHolder() {
        if (data != null && data.containsKey("Name")) {
            String name = data.get("Name");
            if (name != null) {
                try {
                    return URLDecoder.decode(name, "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    return "";
                }
            }

            return "";
        }
        return "";
    }

    public String getAcntLast4() {
        if (data != null && data.containsKey("AcntLast4")) {
            return data.get("AcntLast4");
        }
        return "";
    }

    public String getAcntFirst4() {
        if (data != null && data.containsKey("AcntFirst4")) {
            return data.get("AcntFirst4");
        }
        return "";
    }

    public double getMerchantFee() {
        if (data != null && data.containsKey("Fee")) {
            return parseDouble(data.get("Fee"), 0);
        }

        return 0;
    }

    // Service and Handling Fee, if enabled
    public double getServiceFee() {
        if (data != null && data.containsKey("SHFee")) {
            return parseDouble(data.get("SHFee"), 0);
        }

        return 0;
    }

    public double getTaxAmount() {
        if (data != null && data.containsKey("TaxAmount")) {
            return parseDouble(data.get("TaxAmount"), 0);
        }

        return 0;
    }

    public double getTaxCity() {
        if (data != null && data.containsKey("TaxCity")) {
            return parseDouble(data.get("TaxCity"), 0);
        }

        return 0;
    }

    public double getTaxState() {
        if (data != null && data.containsKey("TaxState")) {
            return parseDouble(data.get("TaxState"), 0);
        }

        return 0;
    }

    public double getCashBack() {
        if (data != null && data.containsKey("CashBack")) {
            return parseDouble(data.get("CashBack"), 0);
        }

        return 0;
    }

    // Entry method used to provide account data. Supported Entry Types - Swipe, Manual, CHIP, Contactless and CHIP Contactless
    public String getEntryTypeString() {
        if (data != null && data.containsKey("EntryType")) {
            return data.get("EntryType");
        }
        return "";
    }

    public List<Pair<String, String>> getPromptValues() {
        List<Pair<String, String>> values = new ArrayList<>();

        if (data != null && data.containsKey("Cust1") && data.containsKey("Cust1Value")) {
            values.add(new Pair<>(data.get("Cust1"), data.get("Cust1Value")));
        }

        if (data != null && data.containsKey("Cust2") && data.containsKey("Cust2Value")) {
            values.add(new Pair<>(data.get("Cust2"), data.get("Cust2Value")));
        }

        if (data != null && data.containsKey("Cust3") && data.containsKey("Cust3Value")) {
            values.add(new Pair<>(data.get("Cust3"), data.get("Cust3Value")));
        }

        return values;
    }

    //region Parcelable
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.app);
        dest.writeInt(this.data.size());
        for (Map.Entry<String, String> entry : this.data.entrySet()) {
            dest.writeString(entry.getKey());
            dest.writeString(entry.getValue());
        }
    }

    protected DejavooResponseExtData(Parcel in) {
        this.app = in.readString();
        int dataSize = in.readInt();
        this.data = new HashMap<String, String>(dataSize);
        for (int i = 0; i < dataSize; i++) {
            String key = in.readString();
            String value = in.readString();
            this.data.put(key, value);
        }
    }

    public static final Parcelable.Creator<DejavooResponseExtData> CREATOR = new Parcelable.Creator<DejavooResponseExtData>() {
        @Override
        public DejavooResponseExtData createFromParcel(Parcel source) {
            return new DejavooResponseExtData(source);
        }

        @Override
        public DejavooResponseExtData[] newArray(int size) {
            return new DejavooResponseExtData[size];
        }
    };
    //endregion


    //region Private methods
    private int parseInteger(String value, int defaultValue) {
        if (value == null || value.isEmpty()) {
            return defaultValue;
        }

        try {
            return Integer.valueOf(value);
        } catch (NumberFormatException e) {
            return defaultValue;
        }
    }

    private double parseDouble(String value, double defaultValue) {
        if (value == null || value.isEmpty()) {
            return defaultValue;
        }

        try {
            return Double.valueOf(value);
        } catch (NumberFormatException e) {
            return defaultValue;
        }
    }
    //endregion
}
