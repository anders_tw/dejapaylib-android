package com.dvmms.dejapay.models;
import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Transaction response
 */
public class DejavooTransactionResponse implements Serializable, Parcelable {

    private Map<ReceiptType, String> rawReceipts;

    public enum ResultCode {
        Succeded, Failed
    }

    public enum ReceiptType {
        Merchant(0), Customer(1);

        final int key;

        ReceiptType(int key) {
            this.key = key;
        }

        public int getKey() {
            return key;
        }

        public static ReceiptType fromNumber(int key) {
            for (ReceiptType type : ReceiptType.values()) {
                if (type.getKey() == key) {
                    return type;
                }
            }

            // FIXME
            return Merchant;
        }
    }

    public enum TetheringProtocol {
        Default, DataWire, VisaI
    }

    private String referenceId;
    private String registerId;
    private String invoiceNumber;
    private ResultCode resultCode;
    private String responseMessage;
    private String message;
    private String authenticationCode;
    private String pnReference;
    private DejavooPaymentType paymentType;
    private DejavooTransactionType transactionType;

    private byte[] tetheringMessage;
    private String tetheringHost;
    private Integer tetheringPort;
    private String tetheringSecurity;
    private TetheringProtocol tetheringProtocol = TetheringProtocol.Default;

    private List<DejavooResponseExtData> extDatas = new ArrayList<>();
    private List<DejavooResponseTransReport> transReports = new ArrayList<>();

    private String emvData;
    private byte[] sign;

    private String error;
    private String errorCode;

    private boolean signatureRequired;
    private double signatureTimeout;

    private String packet;
    private boolean voided = false;
    private boolean voucher = false;

    /**
     * Alpha numeric reference field. Must be unique for the open batch scope.
     * Set by the host system and, Mandatory for all deployments.
     * Used for Tip or other transaction adjustment requests related to a given transaction,
     * for example, Tip Adjustment or Void Transaction request must have a RefId value of
     * an original transaction.
     * @return Reference Id
     */
    public String getReferenceId() {
        return referenceId;
    }

    /**
     * Alpha numeric reference field. Must be unique for the open batch scope.
     * Set by the host system and, Mandatory for all deployments.
     * Used for Tip or other transaction adjustment requests related to a given transaction,
     * for example, Tip Adjustment or Void Transaction request must have a RefId value of
     * an original transaction.
     * @param referenceId Reference Id
     */
    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }

    /**
     * Alpha numeric reference field.
     * @return Register Id
     */
    public String getRegisterId() {
        return registerId;
    }

    /**
     * Alpha numeric reference field.
     * @param registerId Register Id
     */
    public void setRegisterId(String registerId) {
        this.registerId = registerId;
    }

    /**
     * Unique alpha numeric invoice number. Used if terminal Invoice parameter is disabled
     * @return Invoice Number
     */
    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    /**
     * Unique alpha numeric invoice number. Used if terminal Invoice parameter is disabled
     * @param invoiceNumber Invoice Number
     */
    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    /**
     * Get result code
     * @return Result code
     * @see ResultCode
     */
    public ResultCode getResultCode() {
        return resultCode;
    }

    /**
     * Set result code
     * @param resultCode Result code
     * @see ResultCode
     */
    public void setResultCode(ResultCode resultCode) {
        this.resultCode = resultCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    /**
     * Get message from terminal
     * @return response message
     */
    public String getMessage() {
        return message;
    }

    /**
     * Set message
     * @param message response message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * Get authentication code
     * @return Authentication code
     */
    public String getAuthenticationCode() {
        return authenticationCode;
    }

    /**
     * Set authentication code
     * @param authenticationCode Authentication code
     */
    public void setAuthenticationCode(String authenticationCode) {
        this.authenticationCode = authenticationCode;
    }

    /**
     * Get PN Reference
     * @return PN Reference
     */
    public String getPnReference() {
        return pnReference;
    }

    /**
     * Set PN Reference
     * @param pnReference PN Reference
     */
    public void setPnReference(String pnReference) {
        this.pnReference = pnReference;
    }

    /**
     * Get payment type
     * @return Payment type
     * @see DejavooPaymentType
     */
    public DejavooPaymentType getPaymentType() {
        return paymentType;
    }

    /**
     * Set payment type
     * @param paymentType Payment type
     * @see DejavooPaymentType
     */
    public void setPaymentType(DejavooPaymentType paymentType) {
        this.paymentType = paymentType;
    }

    /**
     * Get EMV data
     * Additional transaction data specific to EMV transactions required to be present on receipt:
     •	AID: Application ID from ICC
     •	AppName: Application Name from ICC
     •	TVR: Terminal verification results
     •	TSI: Transaction status information
     * @return EMV data
     */
    public String getEmvData() {
        return emvData;
    }

    /**
     * Set EMV data
     * @param emvData EMV data
     */
    public void setEmvData(String emvData) {
        this.emvData = emvData;
    }

    /**
     * Get signature (BMP)
     * @return Signature (bmp) in bytes
     */
    public byte[] getSign() {
        return sign;
    }


    /**
     * Set signature (BMP)
     * @param sign Signature in bytes
     */
    public void setSign(byte[] sign) {
        this.sign = sign;
    }

    /**
     * Get error message from terminal
     * @return Error message
     */
    public String getError() {
        return error;
    }

    /**
     * Set error message
     * @param error Error message
     */
    public void setError(String error) {
        this.error = error;
    }

    /**
     * Get error code
     * @return Error code
     */
    public String getErrorCode() {
        return errorCode;
    }

    /**
     * Get error code
     * @param errorCode error code
     */
    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    /**
     * Is signature required
     * @return required
     */
    public boolean isSignatureRequired() {
        return signatureRequired;
    }

    /**
     * Set signature required
     * @param signatureRequired signature required
     */
    public void setSignatureRequired(boolean signatureRequired) {
        this.signatureRequired = signatureRequired;
    }

    /**
     * Get signature timeout
     * @return timeout in seconds
     */
    public double getSignatureTimeout() {
        return signatureTimeout;
    }

    /**
     * Set signature timeout
     * @param signatureTimeout Timeout in seconds
     */
    public void setSignatureTimeout(double signatureTimeout) {
        this.signatureTimeout = signatureTimeout;
    }

    /**
     * Is tethering response
     * @return true if tethering response, else if not tethering
     */
    public boolean isTethering() {
        return (!isEmptyOrNull(tetheringHost) && tetheringPort != null && tetheringPort > 0 && tetheringMessage != null);
    }

    /**
     * Get tethering host
     * @return Host
     */
    public String getTetheringHost() {
        return tetheringHost;
    }

    /**
     * Set tethering host
     * @param tetheringHost Host
     */
    public void setTetheringHost(String tetheringHost) {
        this.tetheringHost = tetheringHost;
    }

    /**
     * Get tethering port
     * @return Port
     */
    public Integer getTetheringPort() {
        return tetheringPort;
    }

    /**
     * Set tethering port
     * @param tetheringPort Port
     */
    public void setTetheringPort(Integer tetheringPort) {
        this.tetheringPort = tetheringPort;
    }

    /**
     * Get tethering message
     * @return message
     */
    public byte[] getTetheringMessage() {
        return tetheringMessage;
    }

    /**
     * Set tethering message
     * @param tetheringMessage Tethering message
     */
    public void setTetheringMessage(byte[] tetheringMessage) {
        this.tetheringMessage = tetheringMessage;
    }

    public TetheringProtocol getTetheringProtocol() {
        return tetheringProtocol;
    }

    public void setTetheringProtocol(String tetheringProtocol) {
        if (tetheringProtocol.equalsIgnoreCase("Visa I")) {
            this.tetheringProtocol = TetheringProtocol.VisaI;
        } else if (tetheringProtocol.equalsIgnoreCase("Data Wire")) {
            this.tetheringProtocol = TetheringProtocol.DataWire;
        } else {
            this.tetheringProtocol = TetheringProtocol.Default;
        }
    }

    /**
     * Get additional transaction data
     * List and explanation of data
     •	InvNum: invoice number
     •	CardType: VISA, MASTERCARD, AMEX, DISCOVER, DINERSCLUB, JCB, ENROUTE, DEBIT, EBT
     •	BatchNum: batch number
     •	Tip: tip amount
     •	CashBack: cashback amount
     •	Fee: merchant fee
     •	AcntLast: last 4 digit of account number
     •	Name: cardholder name (if card swipe)
     •	SVC: debit surcharge amount
     •	TotalAmt: total amount
     •	DISC: discount amount
     •	SHFee: Service and Handling Fee, if enabled
     •	RwdPoints: Points earned (if Internal Loyalty or Rewardy is enabled
     •	RwdBalance: Points Balance for transaction processed.
     •	RwdIssued: Text reward issued if reward threshold was exceeded
     •	EBTFSLedgerBalance, EBTFSAvailBalance, EBTFSBeginBalance: EBT Food Stamp Balance, if provided by Host
     •	EBTCashLedgerBalance, EBTCashAvailBalance, EBTCashBeginBalance: EBT Cash Balance, if provided by Host
     •	AcqReference: Acquirer Reference Data
     •	ProcessData: Processing routing data returned in the original transaction response
     •	RefNo: Mercury processor reference number
     •	RewardQR: Loyalty QR code
     •	RewardCode: Loyalty promo code
     * Check transaction does not have this field
     * @param appName App name for several apps operation by default "" (empty string)
     * @return additional transaction data
     */
    public Map<String,String> getExtData(String appName) {
        DejavooResponseExtData extData = getExtDataByAppName(appName);

        if (extData != null) {
            return extData.getData();
        }

        return new LinkedHashMap<>();
    }

    /**
     * Get additional transaction data for single app operation
     * @return additional transaction data
     */
    public Map<String,String> getExtData() {
        return getExtData("");
    }

    public void setExtData(String app, Map<String, String> data) {
        extDatas.add(new DejavooResponseExtData(app, data));
    }

    public void setTransReport(String app, Map<String, String> data) {
        transReports.add(new DejavooResponseTransReport(app, data));
    }

    public List<DejavooResponseTransReport> getTransReports() {
        return transReports;
    }

    public Map<ReceiptType, String> getRawReceipts() {
        return rawReceipts;
    }

    public void setRawReceipts(Map<ReceiptType, String> rawReceipts) {
        this.rawReceipts = rawReceipts;
    }

    public boolean hasReceipt(ReceiptType type) {
        return rawReceipts != null && rawReceipts.containsKey(type);
    }

    public String getReceipt(ReceiptType receiptType) {
        if (hasReceipt(receiptType)) {
            return rawReceipts.get(receiptType);
        }
        return "";
    }

    /**
     * Get transaction type.
     * @return Transaction type
     */
    public DejavooTransactionType getTransactionType() {
        return transactionType;
    }

    /**
     * Set transaction type
     * @param transactionType Transaction type
     */
    public void setTransactionType(DejavooTransactionType transactionType) {
        this.transactionType = transactionType;
    }

    /**
     * Get tethering security
     * @return tethering security (TLS/SSL)
     */
    public String getTetheringSecurity() {
        return tetheringSecurity;
    }

    /**
     * Set tethering security
     * @param tetheringSecurity Security key for tethering
     */
    public void setTetheringSecurity(String tetheringSecurity) {
        this.tetheringSecurity = tetheringSecurity;
    }

    public String getPacketRaw() {
        return packet;
    }

    public void setPacketRaw(String packet) {
        this.packet = packet;
    }

    static boolean isEmptyOrNull(String text) {
        return (text == null || text.isEmpty());
    }

    public double getTotalAmount(String appName) {
        DejavooResponseExtData extData = getExtDataByAppName(appName);

        if (extData != null) {
            return roundDouble(extData.getTotalAmount(), 2);
        }

        return 0.f;
    }

    public double getTip(String appName) {
        DejavooResponseExtData extData = getExtDataByAppName(appName);

        if (extData != null) {
            return roundDouble(extData.getTip(), 2);
        }

        return 0.f;
    }

    public double getMerchantFee(String appName) {
        DejavooResponseExtData extData = getExtDataByAppName(appName);

        if (extData != null) {
            return roundDouble(extData.getMerchantFee(), 2);
        }

        return 0.f;
    }

    // Service and Handling Fee, if enabled
    public double getServiceFee(String appName) {
        DejavooResponseExtData extData = getExtDataByAppName(appName);

        if (extData != null) {
            return roundDouble(extData.getServiceFee(), 2);
        }

        return 0.f;
    }

    public double getTaxAmount(String appName) {
        DejavooResponseExtData extData = getExtDataByAppName(appName);

        if (extData != null) {
            return roundDouble(extData.getTaxAmount(), 2);
        }

        return 0.f;
    }

    public double getTaxCity(String appName) {
        DejavooResponseExtData extData = getExtDataByAppName(appName);

        if (extData != null) {
            return roundDouble(extData.getTaxCity(), 2);
        }

        return 0.f;
    }

    public double getTaxState(String appName) {
        DejavooResponseExtData extData = getExtDataByAppName(appName);

        if (extData != null) {
            return roundDouble(extData.getTaxState(), 2);
        }

        return 0.f;
    }

    public String getAcntLast4(String appName) {
        DejavooResponseExtData extData = getExtDataByAppName(appName);

        if (extData != null) {
            return extData.getAcntLast4();
        }

        return "";
    }

    public String getCardHolder(String appName) {
        DejavooResponseExtData extData = getExtDataByAppName(appName);

        if (extData != null) {
            return extData.getCardHolder();
        }

        return "";
    }



    public int getBatchNumber(String appName) {
        DejavooResponseExtData extData = getExtDataByAppName(appName);

        if (extData != null) {
            return extData.getBatchNumber();
        }

        return -1;
    }

    public List<DejavooResponseExtData> getExtDatas() {
        return extDatas;
    }

    private DejavooResponseExtData getExtDataByAppName(String appName) {
        if (extDatas == null) {
            return null;
        }

        for (DejavooResponseExtData extData : extDatas) {
            if (extData.getApp().equalsIgnoreCase(appName)) {
                return extData;
            }
        }

        if (isEmptyOrNull(appName)) {
            if (extDatas.size() > 0) {
                return extDatas.get(0);
            }
        }

        return null;
    }

    public boolean isVoided() {
        return voided;
    }

    public void setVoided(boolean voided) {
        this.voided = voided;
    }

    public boolean isVoucher() {
        return voucher;
    }

    public void setVoucher(boolean voucher) {
        this.voucher = voucher;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (this.rawReceipts != null) {
            dest.writeInt(this.rawReceipts.size());
            for (Map.Entry<ReceiptType, String> entry : this.rawReceipts.entrySet()) {
                dest.writeInt(entry.getKey() == null ? -1 : entry.getKey().ordinal());
                dest.writeString(entry.getValue());
            }
        } else {
            dest.writeInt(0);
        }
        dest.writeString(this.referenceId);
        dest.writeString(this.registerId);
        dest.writeString(this.invoiceNumber);
        dest.writeInt(this.resultCode == null ? -1 : this.resultCode.ordinal());
        dest.writeString(this.responseMessage);
        dest.writeString(this.message);
        dest.writeString(this.authenticationCode);
        dest.writeString(this.pnReference);
        dest.writeInt(this.paymentType == null ? -1 : this.paymentType.ordinal());
        dest.writeInt(this.transactionType == null ? -1 : this.transactionType.ordinal());
        dest.writeByteArray(this.tetheringMessage);
        dest.writeString(this.tetheringHost);
        dest.writeValue(this.tetheringPort);
        dest.writeString(this.tetheringSecurity);
        dest.writeInt(this.tetheringProtocol == null ? -1 : this.tetheringProtocol.ordinal());

        if (extDatas != null) {
            dest.writeList(this.extDatas);
        } else {
            dest.writeList(new ArrayList());
        }

        dest.writeString(this.emvData);
        dest.writeByteArray(this.sign);
        dest.writeString(this.error);
        dest.writeString(this.errorCode);
        dest.writeByte(this.signatureRequired ? (byte) 1 : (byte) 0);
        dest.writeDouble(this.signatureTimeout);
        dest.writeString(this.packet);
        dest.writeByte(this.voided ? (byte) 1: (byte) 0);
        dest.writeByte(this.voucher ? (byte) 1: (byte) 0);


        if (transReports != null) {
            dest.writeList(this.transReports);
        } else {
            dest.writeList(new ArrayList());
        }
    }

    public DejavooTransactionResponse() {
    }

    protected DejavooTransactionResponse(Parcel in) {
        int rawReceiptsSize = in.readInt();
        this.rawReceipts = new HashMap<ReceiptType, String>(rawReceiptsSize);
        for (int i = 0; i < rawReceiptsSize; i++) {
            int tmpKey = in.readInt();
            ReceiptType key = tmpKey == -1 ? null : ReceiptType.values()[tmpKey];
            String value = in.readString();
            this.rawReceipts.put(key, value);
        }
        this.referenceId = in.readString();
        this.registerId = in.readString();
        this.invoiceNumber = in.readString();
        int tmpResultCode = in.readInt();
        this.resultCode = tmpResultCode == -1 ? null : ResultCode.values()[tmpResultCode];
        this.responseMessage = in.readString();
        this.message = in.readString();
        this.authenticationCode = in.readString();
        this.pnReference = in.readString();
        int tmpPaymentType = in.readInt();
        this.paymentType = tmpPaymentType == -1 ? null : DejavooPaymentType.values()[tmpPaymentType];
        int tmpTransactionType = in.readInt();
        this.transactionType = tmpTransactionType == -1 ? null : DejavooTransactionType.values()[tmpTransactionType];
        this.tetheringMessage = in.createByteArray();
        this.tetheringHost = in.readString();
        this.tetheringPort = (Integer) in.readValue(Integer.class.getClassLoader());
        this.tetheringSecurity = in.readString();
        int tmpTetheringProtocol = in.readInt();
        this.tetheringProtocol = tmpTetheringProtocol == -1 ? null : TetheringProtocol.values()[tmpTetheringProtocol];
        this.extDatas = new ArrayList<DejavooResponseExtData>();
        in.readList(this.extDatas, DejavooResponseExtData.class.getClassLoader());
        this.emvData = in.readString();
        this.sign = in.createByteArray();
        this.error = in.readString();
        this.errorCode = in.readString();
        this.signatureRequired = in.readByte() != 0;
        this.signatureTimeout = in.readDouble();
        this.packet = in.readString();
        this.voided = in.readByte() != 0;
        this.voucher = in.readByte() != 0;

        this.transReports = new ArrayList<DejavooResponseTransReport>();
        in.readList(this.transReports, DejavooResponseTransReport.class.getClassLoader());
    }

    public static final Parcelable.Creator<DejavooTransactionResponse> CREATOR = new Parcelable.Creator<DejavooTransactionResponse>() {
        @Override
        public DejavooTransactionResponse createFromParcel(Parcel source) {
            return new DejavooTransactionResponse(source);
        }

        @Override
        public DejavooTransactionResponse[] newArray(int size) {
            return new DejavooTransactionResponse[size];
        }
    };

    private float doubleToFloat(double value, int fraction) {
        float a = (float)value;
        double temp = Math.pow(10.0, fraction);
        a *= temp;
        a = Math.round(a);
        return (a / (float)temp);
    }

    private double roundDouble(double value, int fraction) {
        if (fraction < 0) return 0;

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(fraction, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }


}
