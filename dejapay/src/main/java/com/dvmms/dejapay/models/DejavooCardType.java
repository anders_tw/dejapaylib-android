package com.dvmms.dejapay.models;

public enum DejavooCardType {
    UNKNOWN, VISA, MASTERCARD, AMEX, DISCOVER, DINERSCLUB, JCB, ENROUTE, DEBIT, EBT
}
