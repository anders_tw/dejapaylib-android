package com.dvmms.dejapay.models;

/**
 * Created by pmalyugin on 10/04/2017.
 */

public class DejavooPrintoutBreakLeftItem implements IDejavooPrintoutItem {
    public DejavooPrintoutBreakLeftItem() { }

    @Override
    public String data() {
        return "<L></L>";
    }
}
