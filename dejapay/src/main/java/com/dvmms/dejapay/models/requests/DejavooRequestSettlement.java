package com.dvmms.dejapay.models.requests;

import android.os.Parcel;
import android.os.Parcelable;

import com.dvmms.dejapay.DejavooUtils;
import com.dvmms.dejapay.models.DejavooTransactionRequest;

public class DejavooRequestSettlement implements Parcelable {
    private final boolean force;
    private final DejavooTransactionRequest.ReceiptType receipt;
    private final String refId;

    public DejavooRequestSettlement(Builder builder) {
        this.force = builder.force;
        this.receipt = builder.receipt;
        this.refId = builder.refId;
    }

    public boolean isForce() {
        return force;
    }

    public DejavooTransactionRequest.ReceiptType getReceipt() {
        return receipt;
    }

    public String getRefId() {
        return refId;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private boolean force = false;
        private DejavooTransactionRequest.ReceiptType receipt = DejavooTransactionRequest.ReceiptType.Both;
        private String refId = DejavooUtils.generateRandomRefId();

        private Builder() { }

        public Builder setForce(boolean force) {
            this.force = force;
            return this;
        }

        public Builder setReceipt(DejavooTransactionRequest.ReceiptType receipt) {
            this.receipt = receipt;
            return this;
        }

        public Builder setRefId(String refId) {
            this.refId = refId;
            return this;
        }

        public DejavooRequestSettlement build() {
            return new DejavooRequestSettlement(this);
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte(this.force ? (byte) 1 : (byte) 0);
        dest.writeInt(this.receipt == null ? -1 : this.receipt.ordinal());
        dest.writeString(this.refId);
    }

    protected DejavooRequestSettlement(Parcel in) {
        this.force = in.readByte() != 0;
        int tmpReceipt = in.readInt();
        this.receipt = tmpReceipt == -1 ? null : DejavooTransactionRequest.ReceiptType.values()[tmpReceipt];
        this.refId = in.readString();
    }

    public static final Parcelable.Creator<DejavooRequestSettlement> CREATOR = new Parcelable.Creator<DejavooRequestSettlement>() {
        @Override
        public DejavooRequestSettlement createFromParcel(Parcel source) {
            return new DejavooRequestSettlement(source);
        }

        @Override
        public DejavooRequestSettlement[] newArray(int size) {
            return new DejavooRequestSettlement[size];
        }
    };
}
