package com.dvmms.dejapay.models;

/**
 * Interface for PrintOut items
 */
public interface IDejavooPrintoutItem {
    String data();
}
