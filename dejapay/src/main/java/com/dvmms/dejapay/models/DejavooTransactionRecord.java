package com.dvmms.dejapay.models;

import android.os.Parcel;
import android.os.Parcelable;

public class DejavooTransactionRecord implements Parcelable {

    private final boolean success;
    private final String refId;
    private final String invoiceId;
    private final String registerId;
    private final String responseMessage;
    private final String message;
    private final String approvalCode;
    private final String pnRef;
    private final DejavooPaymentType paymentType;
    private final DejavooTransactionType transactionType;
    private final String sn;
    private final DejavooCardHolderVerificationMethod cvmResult;
    private final DejavooResponseExtData extData;
    private final boolean voided;
    private final boolean voucher;

    private int transNumber = -1;

    private DejavooTransactionRecord(Builder builder) {
        this.success = builder.success;
        this.refId = builder.refId;
        this.invoiceId = builder.invoiceId;
        this.registerId = builder.registerId;
        this.responseMessage = builder.responseMessage;
        this.message = builder.message;
        this.approvalCode = builder.approvalCode;
        this.pnRef = builder.pnRef;
        this.paymentType = builder.paymentType;
        this.transactionType = builder.transactionType;
        this.sn = builder.sn;
        this.cvmResult = builder.cvmResult;
        this.extData = builder.extData;
        this.voided = builder.voided;
        this.voucher = builder.voucher;
    }

    public int getTransNumber() {
        return transNumber;
    }

    public DejavooTransactionRecord setTransNumber(int transNumber) {
        this.transNumber = transNumber;
        return this;
    }

    public boolean isSuccess() {
        return success;
    }

    public String getRefId() {
        return refId;
    }

    public String getInvoiceId() {
        return invoiceId;
    }

    public String getRegisterId() {
        return registerId;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public String getMessage() {
        return message;
    }

    public String getApprovalCode() {
        return approvalCode;
    }

    public String getPnRef() {
        return pnRef;
    }

    public DejavooPaymentType getPaymentType() {
        return paymentType;
    }

    public DejavooTransactionType getTransactionType() {
        return transactionType;
    }

    public String getSn() {
        return sn;
    }

    public DejavooCardHolderVerificationMethod getCvmResult() {
        return cvmResult;
    }

    public DejavooResponseExtData getExtData() {
        return extData;
    }

    public double getTip() {
        if (extData != null) {
            return extData.getTip();
        }
        return 0;
    }

    public double getAmount() {
        if (extData != null) {
            return extData.getAmount();
        }
        return 0;
    }

    public double getTotalAmount() {
        if (extData != null) {
            return extData.getTotalAmount();
        }
        return 0;
    }

    public double getFee() {
        if (extData != null) {
            return extData.getFee();
        }
        return 0;
    }

    public DejavooCardType getCardType() {
        if (extData != null) {
            return extData.getCardType();
        }

        return DejavooCardType.UNKNOWN;
    }

    public boolean isVoided() {
        return voided;
    }

    public boolean isVoucher() {
        return voucher;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private boolean success = false;
        private DejavooCardHolderVerificationMethod cvmResult = DejavooCardHolderVerificationMethod.Unknown;
        private DejavooPaymentType paymentType = DejavooPaymentType.Unknown;
        private DejavooTransactionType transactionType = DejavooTransactionType.Sale;

        private String refId = "";
        private String invoiceId = "";
        private String registerId = "";
        private String responseMessage = "";
        private String message = "";
        private String approvalCode = "";
        private String pnRef = "";
        private String sn = "";
        private DejavooResponseExtData extData;
        private boolean voided = false;
        private boolean voucher = false;

        private Builder() {  }

        public Builder setSuccess(boolean success) {
            this.success = success;
            return this;
        }

        public Builder setCvmResult(DejavooCardHolderVerificationMethod cvmResult) {
            this.cvmResult = cvmResult;
            return this;
        }

        public Builder setPaymentType(DejavooPaymentType paymentType) {
            this.paymentType = paymentType;
            return this;
        }

        public Builder setTransactionType(DejavooTransactionType transactionType) {
            this.transactionType = transactionType;
            return this;
        }

        public Builder setRefId(String refId) {
            this.refId = refId;
            return this;
        }

        public Builder setInvoiceId(String invoiceId) {
            this.invoiceId = invoiceId;
            return this;
        }

        public Builder setRegisterId(String registerId) {
            this.registerId = registerId;
            return this;
        }

        public Builder setResponseMessage(String responseMessage) {
            this.responseMessage = responseMessage;
            return this;
        }

        public Builder setMessage(String message) {
            this.message = message;
            return this;
        }

        public Builder setApprovalCode(String approvalCode) {
            this.approvalCode = approvalCode;
            return this;
        }

        public Builder setPnRef(String pnRef) {
            this.pnRef = pnRef;
            return this;
        }

        public Builder setSn(String sn) {
            this.sn = sn;
            return this;
        }

        public Builder setExtData(DejavooResponseExtData extData) {
            this.extData = extData;
            return this;
        }

        public Builder setVoided(boolean voided) {
            this.voided = voided;
            return this;
        }

        public Builder setVoucher(boolean voucher) {
            this.voucher = voucher;
            return this;
        }

        public DejavooTransactionRecord build() {
            return new DejavooTransactionRecord(this);
        }

    }

    //endregion

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte(this.success ? (byte) 1 : (byte) 0);
        dest.writeString(this.refId);
        dest.writeString(this.invoiceId);
        dest.writeString(this.registerId);
        dest.writeString(this.responseMessage);
        dest.writeString(this.message);
        dest.writeString(this.approvalCode);
        dest.writeString(this.pnRef);
        dest.writeInt(this.paymentType == null ? -1 : this.paymentType.ordinal());
        dest.writeInt(this.transactionType == null ? -1 : this.transactionType.ordinal());
        dest.writeString(this.sn);
        dest.writeInt(this.cvmResult == null ? -1 : this.cvmResult.ordinal());
        dest.writeParcelable(this.extData, flags);
        dest.writeByte(this.voided ? (byte) 1 : (byte) 0);
        dest.writeByte(this.voucher ? (byte) 1 : (byte) 0);
        dest.writeInt(this.transNumber);
    }

    protected DejavooTransactionRecord(Parcel in) {
        this.success = in.readByte() != 0;
        this.refId = in.readString();
        this.invoiceId = in.readString();
        this.registerId = in.readString();
        this.responseMessage = in.readString();
        this.message = in.readString();
        this.approvalCode = in.readString();
        this.pnRef = in.readString();
        int tmpPaymentType = in.readInt();
        this.paymentType = tmpPaymentType == -1 ? null : DejavooPaymentType.values()[tmpPaymentType];
        int tmpTransactionType = in.readInt();
        this.transactionType = tmpTransactionType == -1 ? null : DejavooTransactionType.values()[tmpTransactionType];
        this.sn = in.readString();
        int tmpCvmResult = in.readInt();
        this.cvmResult = tmpCvmResult == -1 ? null : DejavooCardHolderVerificationMethod.values()[tmpCvmResult];
        this.extData = in.readParcelable(DejavooResponseExtData.class.getClassLoader());
        this.voided = in.readByte() != 0;
        this.voucher = in.readByte() != 0;
        this.transNumber = in.readInt();
    }

    public static final Creator<DejavooTransactionRecord> CREATOR = new Creator<DejavooTransactionRecord>() {
        @Override
        public DejavooTransactionRecord createFromParcel(Parcel source) {
            return new DejavooTransactionRecord(source);
        }

        @Override
        public DejavooTransactionRecord[] newArray(int size) {
            return new DejavooTransactionRecord[size];
        }
    };
}