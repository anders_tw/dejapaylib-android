package com.dvmms.dejapay.models;

/**
 * Created by pmalyugin on 10/04/2017.
 */

public class DejavooPrintoutLineItem implements IDejavooPrintoutItem {
    final String left;
    final String right;
    final String center;

    private final boolean large;
    private final boolean inverted;
    private final boolean condensed;
    private final boolean bold;

    private DejavooPrintoutLineItem(String left, String right, String center, boolean large, boolean inverted, boolean condensed, boolean bold) {
        this.left = left;
        this.right = right;
        this.center = center;

        this.bold = bold;
        this.large = large;
        this.condensed = condensed;
        this.inverted = inverted;
    }

    @Override
    public String data() {
        String data = "";
        if (!isEmptyOrNull(center)) {
            data += "<C>" + escapeText(center) + "</C>";
        }

        if (!isEmptyOrNull(left)) {
            data += "<L>" + escapeText(left) + "</L>";
        }

        if (!isEmptyOrNull(right)) {
            data += "<R>" + escapeText(right) + "</R>";
        }

        if (bold) {
            data = "<B>"+data+"</B>";
        }

        if (large) {
            data = "<LG>"+data+"</LG>";
        }

        if (inverted) {
            data = "<INV>"+data+"</INV>";
        }

        if (condensed) {
            data = "<CD>"+data+"</CD>";
        }

        return data;
    }

    // #, % - symbols break SPIN/WiFi request,
    // Prevent parsing failed
    private String escapeText(String text) {
        return text.replace("#", "&#35;").replace("%", "&#37;");
    }

    public static DejavooPrintoutLineItem center(String line, boolean large, boolean inverted, boolean condensed, boolean bold) {
        return new DejavooPrintoutLineItem("", "", line, large, inverted, condensed, bold);
    }

    public static DejavooPrintoutLineItem leftRight(String left, String right, boolean large, boolean inverted, boolean condensed, boolean bold) {
        return new DejavooPrintoutLineItem(left, right, "", large, inverted, condensed, bold);
    }

    public static DejavooPrintoutLineItem left(String left, boolean large, boolean inverted, boolean condensed, boolean bold) {
        return new DejavooPrintoutLineItem(left, "", "", large, inverted, condensed, bold);
    }

    public static DejavooPrintoutLineItem right(String right, boolean large, boolean inverted, boolean condensed, boolean bold) {
        return new DejavooPrintoutLineItem("", right, "", large, inverted, condensed, bold);
    }

    private boolean isEmptyOrNull(String text) {
        return (text != null && text.length() == 0);
    }
}
