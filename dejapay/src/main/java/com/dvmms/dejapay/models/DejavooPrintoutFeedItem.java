package com.dvmms.dejapay.models;

/**
 * Feed item for PrintOut request
 */
public class DejavooPrintoutFeedItem implements IDejavooPrintoutItem {
    public DejavooPrintoutFeedItem() {
    }

    @Override
    public String data() {
        return "<BR/><BR/><BR/><BR/><BR/><BR/><BR/><BR/><BR/>";
    }
}
