package com.dvmms.dejapay.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashMap;
import java.util.Map;

public class DejavooResponseTransReport implements Parcelable {
    private final String app;
    private final Map<String, String> data;

    public DejavooResponseTransReport(String app, Map<String, String> data) {
        this.app = app;
        this.data = data;
    }

    public String getApp() {
        return app;
    }

    public Map<String, String> getData() {
        return data;
    }

    public double getTotalAmount() {
        if (data != null && data.containsKey("TotalAmt")) {
            return parseDouble(data.get("TotalAmt"), 0);
        }

        return 0;
    }

    public double getSaleAmount() {
        if (data != null && data.containsKey("SaleAmt")) {
            return parseDouble(data.get("SaleAmt"), 0);
        }

        return 0;
    }
    public double getReturnAmount() {
        if (data != null && data.containsKey("ReturnAmt")) {
            return parseDouble(data.get("ReturnAmt"), 0);
        }

        return 0;
    }

    public double getVoidAmount() {
        if (data != null && data.containsKey("VoidAmt")) {
            return parseDouble(data.get("VoidAmt"), 0);
        }

        return 0;
    }

    public double getAuthAmount() {
        if (data != null && data.containsKey("AuthAmt")) {
            return parseDouble(data.get("AuthAmt"), 0);
        }

        return 0;
    }

    //region Private methods
    private int parseInteger(String value, int defaultValue) {
        if (value == null || value.isEmpty()) {
            return defaultValue;
        }

        try {
            return Integer.valueOf(value);
        } catch (NumberFormatException e) {
            return defaultValue;
        }
    }

    private double parseDouble(String value, double defaultValue) {
        if (value == null || value.isEmpty()) {
            return defaultValue;
        }

        try {
            return Double.valueOf(value);
        } catch (NumberFormatException e) {
            return defaultValue;
        }
    }
    //endregion

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.app);
        dest.writeInt(this.data.size());
        for (Map.Entry<String, String> entry : this.data.entrySet()) {
            dest.writeString(entry.getKey());
            dest.writeString(entry.getValue());
        }
    }

    protected DejavooResponseTransReport(Parcel in) {
        this.app = in.readString();
        int dataSize = in.readInt();
        this.data = new HashMap<String, String>(dataSize);
        for (int i = 0; i < dataSize; i++) {
            String key = in.readString();
            String value = in.readString();
            this.data.put(key, value);
        }
    }

    public static final Parcelable.Creator<DejavooResponseTransReport> CREATOR = new Parcelable.Creator<DejavooResponseTransReport>() {
        @Override
        public DejavooResponseTransReport createFromParcel(Parcel source) {
            return new DejavooResponseTransReport(source);
        }

        @Override
        public DejavooResponseTransReport[] newArray(int size) {
            return new DejavooResponseTransReport[size];
        }
    };
}
