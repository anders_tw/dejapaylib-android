package com.dvmms.dejapay.models;

/**
 * User Choice response
 */
public class DejavooUserChoiceResponse {
    private String registerId;
    private String userChoice;
    private String message;

    /**
     * Alpha numeric reference field.
     * @return Register Id
     */
    public String getRegisterId() {
        return registerId;
    }

    /**
     * Alpha numeric reference field.
     * @param registerId Register Id
     */
    public void setRegisterId(String registerId) {
        this.registerId = registerId;
    }

    /**
     * Get response to Request for a user choice
     * @return user choice
     */
    public String getUserChoice() {
        return userChoice;
    }

    /**
     * Set response to Request for a user choice
     * @param userChoice user choice
     */
    public void setUserChoice(String userChoice) {
        this.userChoice = userChoice;
    }

    /**
     * Get alpha numeric field providing detailed result message for each result code:
     * Success, Failure, Cancel
     * @return message
     */
    public String getMessage() {
        return message;
    }

    /**
     * Set alpha numeric field providing detailed result message for each result code:
     * Success, Failure, Cancel
     * @param message message
     */
    public void setMessage(String message) {
        this.message = message;
    }
}
