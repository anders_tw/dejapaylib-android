package com.dvmms.dejapay.models;

/**
 * Transaction Card Data
 */
public class DejavooTransactionCardData {

    /**
     * Card Data type
     */
    public enum Type {
        Manual("1"), Swipe("2"), Contactless("3");

        String value;

        public String getValue() {
            return value;
        }

        Type(String value) {
            this.value = value;
        }
    }

    Type entType;
    String PAN;
    String track1;
    String track2;
    Integer expiryDate;
    String AVS;
    String ZIP;
    String CVV;
    Boolean cardPresent;

    /**
     * Get entity type
     * @return entity type
     * @see Type
     */
    public Type getEntType() {
        return entType;
    }

    /**
     * Set entity type
     * @param entType Entity type
     * @see Type
     */
    public void setEntType(Type entType) {
        this.entType = entType;
    }

    /**
     * Get PAN of account card
     * @return PAN of account card
     */
    public String getPAN() {
        return PAN;
    }

    /**
     * Set PAN of account card
     * @param pan PAN of account card
     */
    public void setPAN(String pan) {
        this.PAN = pan;
    }

    /**
     * Get Track1
     * @return Track1 string
     */
    public String getTrack1() {
        return track1;
    }

    /**
     * Set Track1
     * @param track1 Track1 string
     */
    public void setTrack1(String track1) {
        this.track1 = track1;
    }

    /**
     * Get Track2
     * @return Track2 string
     */
    public String getTrack2() {
        return track2;
    }

    /**
     * Set Track2
     * @param track2 Track2 string
     */
    public void setTrack2(String track2) {
        this.track2 = track2;
    }

    /**
     * Get expiry date (format: MMYY)
     * @return expiry date
     */
    public Integer getExpiryDate() {
        return expiryDate;
    }

    /**
     * Set expiry date (format: MMYY)
     * @param expiryDate expiry date
     */
    public void setExpiryDate(Integer expiryDate) {
        this.expiryDate = expiryDate;
    }

    /**
     * Get street data
     * @return Street data
     */
    public String getAVS() {
        return AVS;
    }

    /**
     * Set street data
     * @param AVS street data
     */
    public void setAVS(String AVS) {
        this.AVS = AVS;
    }

    /**
     * Get ZIP code
     * @return ZIP code
     */
    public String getZIP() {
        return ZIP;
    }

    /**
     * Set ZIP code
     * @param ZIP ZIP code
     */
    public void setZIP(String ZIP) {
        this.ZIP = ZIP;
    }

    /**
     * Get CVV
     * @return CVV
     */
    public String getCVV() {
        return CVV;
    }

    /**
     * Set CVV
     * @param CVV CVV code
     */
    public void setCVV(String CVV) {
        this.CVV = CVV;
    }

    /**
     * Get card present
     * @return is card present
     */
    public Boolean getCardPresent() {
        return cardPresent;
    }

    /**
     * Set card present
     * @param cardPresent is card present
     */
    public void setCardPresent(Boolean cardPresent) {
        this.cardPresent = cardPresent;
    }
}
