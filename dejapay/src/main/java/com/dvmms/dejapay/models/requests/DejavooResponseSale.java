package com.dvmms.dejapay.models.requests;

import android.os.Parcel;
import android.os.Parcelable;

import com.dvmms.dejapay.models.DejavooPaymentType;
import com.dvmms.dejapay.models.DejavooTransactionResponse;

import java.util.HashMap;
import java.util.Map;

public class DejavooResponseSale implements Parcelable {
    private final boolean success;
    private final String errorMessage;
    private final DejavooPaymentType type;

    private final double amount;
    private final double tip;
    private final String referenceId;
    private final String authenticationCode;

    private final String acntLast4;
    private final String invoiceId;

    private final Map<DejavooTransactionResponse.ReceiptType, String> receipts;
    private final DejavooTransactionResponse response;

    private DejavooResponseSale(Builder builder) {
        this.success = builder.success;
        this.errorMessage = builder.errorMessage;
        this.type = builder.type;
        this.amount = builder.amount;
        this.tip = builder.tip;
        this.referenceId = builder.referenceId;
        this.authenticationCode = builder.authenticationCode;
        this.acntLast4 = builder.acntLast4;
        this.invoiceId = builder.invoiceId;
        this.receipts = builder.receipts;
        this.response = builder.response;
    }

    /**
     * Get Sale Payment type
     * @return Sale Payment type
     */
    public DejavooPaymentType getType() {
        return type;
    }

    /**
     * Get Sale amount
     * @return Sale amount
     */
    public double getAmount() {
        return amount;
    }

    /**
     * Get Sale tip
     * @return Sale tip
     */
    public double getTip() {
        return tip;
    }

    /**
     * Total amount involves Amount + Tip
     * @return
     */
    public double getTotal() {
        return amount + tip;
    }


    /**
     * Get Sale Reference ID
     * @return Reference ID
     */
    public String getReferenceId() {
        return referenceId;
    }

    /**
     * Get Sale Auth Code
     * @return Auth Code
     */
    public String getAuthenticationCode() {
        return authenticationCode;
    }

    /**
     * Get last 4 numbers of an account
     * @return Last 4 numbers
     */
    public String getAcntLast4() {
        return acntLast4;
    }

    /**
     * Get Sale Invoice ID
     * @return Invoice ID
     */
    public String getInvoiceId() {
        return invoiceId;
    }

    /**
     * Get Sale transaction result
     * @return Result
     */
    public boolean isSuccess() {
        return success;
    }

    /**
     * Get error message if a sale is failed
     * @return Error message
     */
    public String getErrorMessage() {
        return errorMessage;
    }

    /**
     * Get Sale receipts
     * @return Sale receipts
     */
    public Map<DejavooTransactionResponse.ReceiptType, String> getReceipts() {
        return receipts;
    }

    /**
     * Get Transaction response
     * @return Transaction response
     */
    public DejavooTransactionResponse getResponse() {
        return response;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private boolean success = false;
        private String errorMessage = "";
        private DejavooPaymentType type;

        private double amount;
        private double tip;
        private String referenceId;
        private String authenticationCode;

        private String acntLast4;
        private String invoiceId;
        private Map<DejavooTransactionResponse.ReceiptType, String> receipts;
        private DejavooTransactionResponse response;

        private Builder() { }

        public Builder setSuccess(boolean success) {
            this.success = success;
            return this;
        }

        public Builder setErrorMessage(String message) {
            this.errorMessage = message != null ? message : "";
            return this;
        }

        public Builder setType(DejavooPaymentType type) {
            this.type = type;
            return this;
        }

        public Builder setAmount(double amount) {
            this.amount = amount;
            return this;
        }

        public Builder setTip(double tip) {
            this.tip = tip;
            return this;
        }

        public Builder setReferenceId(String referenceId) {
            this.referenceId = referenceId;
            return this;
        }

        public Builder setAuthenticationCode(String authenticationCode) {
            this.authenticationCode = authenticationCode;
            return this;
        }

        public Builder setAcntLast4(String acntLast4) {
            this.acntLast4 = acntLast4;
            return this;
        }

        public Builder setInvoiceId(String invoiceId) {
            this.invoiceId = invoiceId;
            return this;
        }

        public Builder setReceipts(Map<DejavooTransactionResponse.ReceiptType, String> receipts) {
            this.receipts = receipts;
            return this;
        }

        public Builder setTransactionResponse(DejavooTransactionResponse response) {
            this.response = response;
            return this;
        }

        public DejavooResponseSale build() {
            return new DejavooResponseSale(this);
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte(this.success ? (byte) 1 : (byte) 0);
        dest.writeString(this.errorMessage);
        dest.writeInt(this.type == null ? -1 : this.type.ordinal());
        dest.writeDouble(this.amount);
        dest.writeDouble(this.tip);
        dest.writeString(this.referenceId);
        dest.writeString(this.authenticationCode);
        dest.writeString(this.acntLast4);
        dest.writeString(this.invoiceId);
        dest.writeInt(this.receipts.size());
        for (Map.Entry<DejavooTransactionResponse.ReceiptType, String> entry : this.receipts.entrySet()) {
            dest.writeInt(entry.getKey() == null ? -1 : entry.getKey().ordinal());
            dest.writeString(entry.getValue());
        }
        dest.writeParcelable(this.response, flags);
    }

    protected DejavooResponseSale(Parcel in) {
        this.success = in.readByte() != 0;
        this.errorMessage = in.readString();
        int tmpType = in.readInt();
        this.type = tmpType == -1 ? null : DejavooPaymentType.values()[tmpType];
        this.amount = in.readDouble();
        this.tip = in.readDouble();
        this.referenceId = in.readString();
        this.authenticationCode = in.readString();
        this.acntLast4 = in.readString();
        this.invoiceId = in.readString();
        int receiptsSize = in.readInt();
        this.receipts = new HashMap<DejavooTransactionResponse.ReceiptType, String>(receiptsSize);
        for (int i = 0; i < receiptsSize; i++) {
            int tmpKey = in.readInt();
            DejavooTransactionResponse.ReceiptType key = tmpKey == -1 ? null : DejavooTransactionResponse.ReceiptType.values()[tmpKey];
            String value = in.readString();
            this.receipts.put(key, value);
        }
        this.response = in.readParcelable(DejavooTransactionResponse.class.getClassLoader());
    }

    public static final Parcelable.Creator<DejavooResponseSale> CREATOR = new Parcelable.Creator<DejavooResponseSale>() {
        @Override
        public DejavooResponseSale createFromParcel(Parcel source) {
            return new DejavooResponseSale(source);
        }

        @Override
        public DejavooResponseSale[] newArray(int size) {
            return new DejavooResponseSale[size];
        }
    };
}
