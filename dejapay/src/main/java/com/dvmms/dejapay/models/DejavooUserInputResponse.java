package com.dvmms.dejapay.models;

/**
 * User Input response
 */
public class DejavooUserInputResponse {
    private String registerId;
    private String userInput;
    private String message;

    /**
     * Alpha numeric reference field.
     * @return Register Id
     */
    public String getRegisterId() {
        return registerId;
    }

    /**
     * Alpha numeric reference field.
     * @param registerId Register Id
     */
    public void setRegisterId(String registerId) {
        this.registerId = registerId;
    }

    /**
     * Get response to Request for a user input
     * @return user input
     */
    public String getUserInput() {
        return userInput;
    }

    /**
     * Set response to Request for a user input
     * @param userInput user input
     */
    public void setUserInput(String userInput) {
        this.userInput = userInput;
    }

    /**
     * Get alpha numeric field providing detailed result message for each result code:
     * Success, Failure, Cancel, Wrong input type
     * @return message
     */
    public String getMessage() {
        return message;
    }

    /**
     * Set alpha numeric field providing detailed result message for each result code:
     * Success, Failure, Cancel, Wrong input type
     * @param message message
     */
    public void setMessage(String message) {
        this.message = message;
    }
}
