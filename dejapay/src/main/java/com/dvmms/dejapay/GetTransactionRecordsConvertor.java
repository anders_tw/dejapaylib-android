package com.dvmms.dejapay;

import com.dvmms.dejapay.models.DejavooCardHolderVerificationMethod;
import com.dvmms.dejapay.models.DejavooPaymentType;
import com.dvmms.dejapay.models.DejavooResponseExtData;
import com.dvmms.dejapay.models.DejavooTransactionRecord;
import com.dvmms.dejapay.models.DejavooTransactionType;
import com.dvmms.dejapay.models.requests.DejavooResponseTransactionRecords;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class GetTransactionRecordsConvertor implements IDejavooDeserializer<DejavooResponseTransactionRecords> {
    @Override
    public DejavooResponseTransactionRecords deserialize(String data) {
        DejavooResponseTransactionRecords.Builder records = DejavooResponseTransactionRecords.builder();

        XmlPullParserFactory xmlFactoryObject = null;
        Map<String, String> tagsText = new HashMap<>();

        String endTag = "</xmp>";
        int endIndex = data.lastIndexOf(endTag);
        if (endIndex != -1) {
            data = data.substring(0, endIndex + endTag.length());
        }

        data = data.replaceAll(">\\s*<", "><");

        try {
            String valueText = "";
            xmlFactoryObject = XmlPullParserFactory.newInstance();
            XmlPullParser parser = xmlFactoryObject.newPullParser();
            InputStream is = new ByteArrayInputStream(data.getBytes());
            parser.setInput(is, null);

            int event = parser.getEventType();

            DejavooTransactionRecord.Builder record = null;

            String startTag = "";
            while (event != XmlPullParser.END_DOCUMENT) {
                String name = parser.getName();
                switch (event) {
                    case XmlPullParser.START_TAG:
                        startTag = name;

                        if (startTag.equalsIgnoreCase("Record")) {
                            record = DejavooTransactionRecord.builder();
                        }

                        break;
                    case XmlPullParser.TEXT:
                        valueText = parser.getText();
                        tagsText.put(startTag, valueText);
                        break;
                    case XmlPullParser.END_TAG: {
                        if (record != null) {
                            if (name.equalsIgnoreCase("Record")) {
                                records.addRecord(record.build());
                            } else if (name.equals("ResultCode")) {
                                if (valueText.equalsIgnoreCase("0")) {
                                    record.setSuccess(true);
                                } else {
                                    record.setSuccess(false);
                                }

                            } else if (name.equals("Message")) {
                                record.setMessage(valueText);
                            } else if (name.equals("InvNum")) {
                                record.setInvoiceId(valueText);
                            } else if (name.equals("ExtData")) {
                                setResponseExtData(record, valueText);
                            } else if (name.equals("Receipt")) {
//                                setResponseReceipts(response, valueText);
                            } else if (name.equals("PNRef")) {
                                record.setPnRef(valueText);
                            } else if (name.equals("SN")) {
                                record.setSn(valueText);
                            } else if (name.equals("ApprovalCode")) {
                                record.setApprovalCode(valueText);
                            } else if (name.equals("RefId")) {
                                record.setRefId(valueText);
                            } else if (name.equals("RegisterId")) {
                                record.setRegisterId(valueText);
                            } else if (name.equals("RespMSG")) {
                                record.setResponseMessage(URLDecoder.decode(valueText, "UTF-8"));
                            } else if (name.equals("Sign")) {
//                                response.setSign( base64Coder.decode(valueText) );
                            } else if (name.equals("CVMResult")) {
                                record.setCvmResult(DejavooCardHolderVerificationMethod.fromNumber(
                                        parseInteger(valueText, DejavooCardHolderVerificationMethod.Unknown.getNumber())
                                ));
                            } else if (name.equals("Voided")) {
                                if (valueText.equalsIgnoreCase("true")) {
                                    record.setVoided(true);
                                } else {
                                    record.setVoided(false);
                                }
                            } else if (name.equals("PaymentType")) {
                                for (DejavooPaymentType paymentType : DejavooPaymentType.values()) {
                                    if (paymentType.name().equalsIgnoreCase(valueText)) {
                                        record.setPaymentType(paymentType);
                                        break;
                                    }
                                }
                            } else if (name.equals("TransType")) {
                                if (StringUtils.containsIgnoreCase(valueText, "Voucher")) {
                                    record.setVoucher(true);
                                }

                                if (valueText.equalsIgnoreCase("Purchase Correction") || valueText.startsWith("Void")) {
                                    record.setTransactionType(DejavooTransactionType.Void);
                                } else {
                                    for (DejavooTransactionType transactionType : DejavooTransactionType.values()) {
                                        if (transactionType.name().equalsIgnoreCase(valueText)) {
                                            record.setTransactionType(transactionType);
                                            break;
                                        }
                                    }
                                }


                            }
                        } else {
                            if (name.equals("ResultCode")) {
                                if (!valueText.equalsIgnoreCase("0")) {
                                    records.setSuccess(false);
                                }
                            } else if (name.equals("Message")) {
                                records.setMessage(valueText);
                            } else if (name.equals("RespMSG")) {
                                records.setResponseMessage(URLDecoder.decode(valueText, "UTF-8"));
                            }
                        }
                    }
                    break;
                }
                event = parser.next();
            }

            return records.build();
        } catch (
                XmlPullParserException ex)

        {
            return null;
        } catch (
                IOException ex)

        {
            return null;
        }

    }

    private void setResponseExtData(DejavooTransactionRecord.Builder record, String extDataText) {
        try {
            Map<String, String> data = new LinkedHashMap<>();

            List<String> values = new ArrayList<>(Arrays.asList(extDataText.split(",")));
            for (String value : values) {
                String[] items = value.split("=");

                String title = items[0].trim();
                if (items.length == 2) {
                    data.put(title, items[1].trim());
                } else {
                    data.put(title, "");
                }
            }


            if (data.containsKey("App")) {
                record.setExtData(new DejavooResponseExtData(data.get("App"), data));
            } else {
                record.setExtData(new DejavooResponseExtData("", data));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private int parseInteger(String value, int defaultValue) {
        if (value == null || value.isEmpty()) {
            return defaultValue;
        }

        try {
            return Integer.valueOf(value);
        } catch (NumberFormatException e) {
            return defaultValue;
        }
    }

    public String serialize(String tpn, DejavooPaymentType paymentType, String registerId, String authKey, int from, int to) {
        return String.format("<request>\n" +
                        "<PaymentType>%s</PaymentType>\n" +
                        "<TransType>Get</TransType>\n" +
                        "<RegisterId>%s</RegisterId>\n" +
                        "<AuthKey>%s</AuthKey>\n" +
                        "<FromTransNo>%d</FromTransNo>\n" +
                        "<ToTransNo>%d</ToTransNo>\n" +
                        "<TPN>%s</TPN>\n" +
                        "</request>",
                paymentType.name(),
                registerId,
                authKey,
                from,
                to,
                tpn
        );
    }
}
