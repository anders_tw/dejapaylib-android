package com.dvmms.dejapay;

/**
 * Created by Platon on 30.06.2016.
 */
class ValidatorItem {
    private Object target;
    private IValidator validator;

    public ValidatorItem(Object target, IValidator validator) {
        this.target = target;
        this.validator = validator;
    }

    public Object getTarget() {
        return target;
    }

    public IValidator getValidator() {
        return validator;
    }
}
