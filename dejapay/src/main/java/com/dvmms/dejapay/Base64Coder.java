package com.dvmms.dejapay;

import android.util.Base64;

/**
 * Created by Platon on 30.06.2016.
 */
public class Base64Coder {
    public String encode(byte[] data) {
        return Base64.encodeToString(data, 0);
    }

    public byte[] decode(String text) {
        return Base64.decode(text, 0);
    }

//    public String decodeToString(String text) {
//        try {
//            byte[] bytes = Base64.decode(text, 0);
//            return new String(bytes, "UTF-8");
//        } catch (IOException ex) {
//            return "";
//        }
//    }


}
